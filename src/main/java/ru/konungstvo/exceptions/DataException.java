package ru.konungstvo.exceptions;

public class DataException extends GenericException {
    public DataException(String errorMessage) {this("DataException", errorMessage);}
    public DataException(String errorName, String errorMessage) {
        super(errorName, errorMessage);
    }
}
