package ru.konungstvo.combat.equipment;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;

public abstract class Equipment {
    private String name;

    public Equipment(String name) {
        this.name = name;
    }


    //public abstract void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException;
    @Deprecated
    public abstract void fillFromJson(String jsonStr) throws ParseException;
    public abstract int getDamage();
    public abstract double getReach();

    public abstract ArrayList<String> getProficiencies();

    @Override
    public String toString() {
        return "Equipment{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
     public boolean isRanged() { return false; }

}
