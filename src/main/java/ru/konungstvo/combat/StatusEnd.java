package ru.konungstvo.combat;

public enum StatusEnd {
    ROUND_START("р."),
    TURN_START("н.х."),
    TURN_END("х.");

    private String desc;
    StatusEnd(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
