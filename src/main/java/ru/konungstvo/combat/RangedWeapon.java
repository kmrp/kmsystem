package ru.konungstvo.combat;

import org.omg.PortableInterceptor.INACTIVE;
import ru.konungstvo.control.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

public enum RangedWeapon {

    SHORT_BOW("короткий лук", "кл", 2, 0, 0, new HashMap<>()),
    LONG_BOW("длинный лук", "дл", 3, 1, 1, new HashMap<>()),
    CROSSBOW("арбалет", "", 3, 1, 1, new HashMap<>()),
    PISTOLE("пистоль", "", 3, 2, 2, new HashMap<>()),
    OUTMODE_SMOOTHBORE("устаревшее гладкоствольное оружие", "уго", 4, 2, 3, new HashMap<>()),
    REVOLVER("револьвер", "", 3, 1, 0, new HashMap<>()),
    MODERN_SMOOTHBORE("современное гладкоствольное оружие", "сго", 5, 2, 2, new HashMap<>()),
    EARLY_RIFLE("раннее нарезное оружие", "рно", 5, 2, 4, new HashMap<>()),
    SHOTGUN("дробовик", "", 5, 2, 3, new HashMap<>()),
    MAGIC("способность", "", 0, 0, 0, new HashMap<>()),
    BIG_THROWING("габаритное метательное оружие", "гмо", 4, -7, 0, new HashMap<>()),
    THROWING("метательное оружие", "мо", 2, -7, 0, new HashMap<>()),
    GARBAGE("подручные предметы", "мусор", 1, -7, 0, new HashMap<>());

    private String name;
    private String alias;
    private int damage;
    private int difficulty;
    private int reload;
    private HashMap<Integer, Integer> distance;
    private Logger logger = new Logger(this.getClass().getSimpleName());

    RangedWeapon(String name, String alias, int damage, int difficulty, int reload, HashMap<Integer, Integer> distance) {
        this.name = name;
        this.alias = alias;
        this.damage = damage;
        this.difficulty = difficulty;
        this.reload = reload;
        this.distance = distance;
        fillTables();
    }

    private void fillTables() {
        switch (this.name) {
            case "короткий лук":
                distance.put(0, 8);
                distance.put(1, 16);
                distance.put(2, 24);
                distance.put(3, 32);
                distance.put(4, 40);
                distance.put(5, 48);
                distance.put(6, 56);
                break;
            case "длинный лук":
                distance.put(0, 10);
                distance.put(1, 20);
                distance.put(2, 30);
                distance.put(3, 40);
                distance.put(4, 50);
                distance.put(5, 60);
                distance.put(6, 70);
                break;
            case "арбалет":
                distance.put(0, 12);
                distance.put(1, 24);
                distance.put(2, 36);
                distance.put(3, 48);
                distance.put(4, 60);
                distance.put(5, 72);
                distance.put(6, 84);
                break;
            case "пистоль":
                distance.put(0, 4);
                distance.put(1, 8);
                distance.put(2, 12);
                distance.put(3, 16);
                distance.put(4, 20);
                distance.put(5, 24);
                distance.put(6, 28);
                break;
            case "устаревшее гладкоствольное оружие":
                distance.put(0, 8);
                distance.put(1, 16);
                distance.put(2, 24);
                distance.put(3, 32);
                distance.put(4, 40);
                distance.put(5, 48);
                distance.put(6, 56);
                break;
            case "револьвер":
                distance.put(0, 5);
                distance.put(1, 10);
                distance.put(2, 15);
                distance.put(3, 20);
                distance.put(4, 25);
                distance.put(5, 30);
                distance.put(6, 35);
                break;
            case "современное гладкоствольное оружие":
                distance.put(0, 10);
                distance.put(1, 20);
                distance.put(2, 30);
                distance.put(3, 40);
                distance.put(4, 50);
                distance.put(5, 60);
                distance.put(6, 70);
                break;
            case "раннее нарезное оружие":
                distance.put(0, 3);
                distance.put(1, 20);
                distance.put(2, 40);
                distance.put(3, 60);
                distance.put(4, 80);
                distance.put(5, 90);
                distance.put(6, 100);
                break;
            case "дробовик":
                distance.put(0, 5);
                distance.put(1, 10);
                distance.put(2, 15);
                distance.put(3, 20);
                break;
            case "способность":
                distance.put(0, 10);
                distance.put(1, 20);
                distance.put(2, 30);
                distance.put(3, 40);
                distance.put(4, 50);
                distance.put(5, 60);
                distance.put(6, 70);
                break;
            case "габаритное метательное оружие":
                distance.put(0, 5);
                distance.put(1, 10);
                distance.put(2, 15);
                distance.put(3, 20);
                distance.put(4, 25);
                distance.put(5, 30);
                distance.put(6, 35);
                break;
            case "метательное оружие":
                distance.put(0, 5);
                distance.put(1, 10);
                distance.put(2, 15);
                distance.put(3, 20);
                distance.put(4, 25);
                distance.put(5, 30);
                distance.put(6, 35);
                break;
            case "подручные предметы":
                distance.put(0, 5);
                distance.put(1, 10);
                distance.put(2, 15);
                distance.put(3, 20);
                distance.put(4, 25);
                distance.put(5, 30);
                distance.put(6, 35);
                break;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getAttackDifficulty(double realDistance) {
//        logger.debug("RangedWeapon: realDistance is " + realDistance);
        for (Map.Entry<Integer, Integer> entry : distance.entrySet()) {
//            logger.debug("Max distance for this tier (" + entry.getKey() +") is " + entry.getValue() + "=" + entry.getValue());
            if (entry.getValue() >= realDistance) {
                return entry.getKey();
            }


            /*
            logger.debug("Max distance for this tier (" + entry.getKey() +") is " + entry.getValue() + "^2=" + Math.pow(entry.getValue(), 2));
            if (Math.pow(entry.getValue(), 2) >= realDistance) {
                return entry.getKey();
            }

             */
        }
        return 666;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getReload() {
        return reload;
    }

    public void setReload(int reload) {
        this.reload = reload;
    }

    public HashMap<Integer, Integer> getDistance() {
        return distance;
    }

    public void setDistance(HashMap<Integer, Integer> distance) {
        this.distance = distance;
    }
}
