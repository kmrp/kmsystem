package ru.konungstvo.commands.executor;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.exceptions.GenericException;
import ru.konungstvo.player.Player;

import java.util.Map;

@Deprecated
public class AliasExecutor {
    public static String execute(Player executor, String... args) {
        if (args.length == 0) {
            StringBuilder result = new StringBuilder(ChatColor.GRAY + "Алиасы:\n");
            for (Map.Entry<String,String> entry: executor.getAliases().entrySet()) {
                result.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
            }
            return result.toString();
        }

        if (args[0].equals("help")) {
            return ChatColor.COMBAT + "Тут будет хелп"; //TODO
        }

        String joinedArgs = String.join(" ", args);

        if (args[0].equals("remove") || args[0].equals("delete")) {
            String alias = joinedArgs.replace("delete", "").trim();
            if (executor.getAliases().containsKey(alias)) {
                executor.removeAlias(alias);
                return ChatColor.COMBAT + "Алиас \"" + alias + " удалён!";
            }
            return ChatColor.COMBAT + "Алиас \"" + alias + " не был найден!";
        }

        String skillName = executor.getSkillNameFromContext(joinedArgs);
        if (skillName == null) {
            throw new GenericException("", "В контексте " + joinedArgs + " не найден навык!");
        }
        String newAlias = joinedArgs.replace(skillName, "").trim();
        executor.addAlias(newAlias, skillName);
        return ChatColor.COMBAT + "Алиас \"" + newAlias + ": " + skillName + "\" добавлен!";
    }
}
