package ru.konungstvo.player;

public enum Permission {
    GM("km.gm"),
    CM("km.craft"),
    BD("km.builder"),
    TELL("km.tell"),
    HINTE("km.hinte"),
    RADIO("km.radio");

    Permission(String str) {
        this.str = str;
    }
    private String str;
    public String get() {
        return str;
    }

    @Override
    public String toString() {
        return str;
    }
}
