package ru.konungstvo.combat.reactions;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;

import java.io.Serializable;
import java.util.Observable;

/**
 * Stores information about player's or npc's reaction skill. Does not trow the die.
 */
public class Reaction extends Observable implements Serializable {

    private String playerName;
    private String skillLevel;
    private String mod;

    public Reaction(String playerName) {
        this(playerName, null, "");
    }

    public Reaction(String playerName, String skillLevel) {
        this(playerName, skillLevel, "");
    }

    public Reaction(String playerName, String skillLevel, String mod) {
        this.playerName = playerName;
        this.skillLevel = skillLevel;
        if (skillLevel == null) {
            DataHolder.inst().updatePlayerNewSkills(playerName); //if no reaction lvl is known, update it from database
            this.skillLevel = DataHolder.inst().getPlayer(playerName).getSkillLevel("реакция");
            if (this.skillLevel == null) {
                throw new DataException("1", "Уровень реакции игрока не найден!");
            }
        }
        this.mod = mod;
        if (mod == null) {
            this.mod = "0";
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getSkillLevel() {
        return skillLevel;
    }

    public String getMod() {
        return mod;
    }

    public Message toMessage() {
//        String result = ChatColor.NICK.get() + playerName + " " +  ChatColor.DICE.get() + skillLevel;
        Message result = new Message(playerName + " ", ChatColor.NICK);
        result.addComponent(new MessageComponent(skillLevel, ChatColor.DICE));

        if (mod != null) result.addComponent(new MessageComponent(" " + mod));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Reaction)) return false;
        Reaction other = (Reaction) obj;
        return this.playerName.equals(other.getPlayerName());
    }
}
