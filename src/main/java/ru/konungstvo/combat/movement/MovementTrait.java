package ru.konungstvo.combat.movement;

public enum MovementTrait {
    NORMAL, FAST, SUPERFAST;
}
