package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToolPanel;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class AttackCommand extends CommandBase {
    public static final String NAME = "makeattack";
    public static String START_MESSAGE = "Выберите, какой рукой вы атакуете: \n";
    public static String END_MESSAGE = "Вы выбрали %s.";


    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        DataHolder dh = DataHolder.inst();
        Player player = dh.getPlayer(sender.getName());
        Player attacker = player;
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (player.hasPermission(Permission.GM)) {
            if (attacker.getSubordinate() != null) {
                attacker = attacker.getSubordinate();
                if (!DataHolder.inst().isNpc(attacker.getName())) {
                    forgePlayer = ServerProxy.getForgePlayer(attacker.getName());
                } else {
                    forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(attacker.getName());
                }
            }
        }

        ClickContainer.deleteContainer("PerformAction", getCommandSenderAsPlayer(sender));



        // CREATE DUEL
        Duel duel;
        {
            try {
                duel = DuelMaster.createDuel(attacker);
            } catch (DataException e) {
//                player.sendError(e);
                return;
            }
        }

        String type = args[0];

        // CHOOSE HAND
        WeaponTagsHandler left = new WeaponTagsHandler(forgePlayer.getHeldItemOffhand());
        WeaponTagsHandler right = new WeaponTagsHandler(forgePlayer.getHeldItemMainhand());

        // ОРУЖИЕ
        if (type.equals("armed")) {

            // В ОБЕИХ РУКАХ ОРУЖИЯ
            if (left.isWeapon() && right.isWeapon()) {
                // DETERMINE WHICH ONE IS ACTIVE
                // GENERATE CLICK PANEL
                Message message = new Message(START_MESSAGE, ChatColor.COMBAT);

                MessageComponent mes = new MessageComponent("[Атака левой] ", ChatColor.BLUE);
                mes.setClickCommand(String.format("/%s 0 %s", ActiveHandAttack.NAME, type));
                message.addComponent(mes);

                mes = new MessageComponent("[Атака правой] ", ChatColor.BLUE);
                mes.setClickCommand(String.format("/%s 1 %s", ActiveHandAttack.NAME, type));
                message.addComponent(mes);

                mes = new MessageComponent("[Парная атака] ", ChatColor.BLUE);
                mes.setClickCommand(String.format("/%s 2 %s", ActiveHandAttack.NAME, type));
                message.addComponent(mes);

                MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
                //toRoot.setBold(true);
                toRoot.setClickCommand("/toroot ActiveHand " + attacker.getName());
                message.addComponent(toRoot);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "ActiveHand", getCommandSenderAsPlayer(sender), attacker);


            } else if (left.isWeapon()) {
                player.performCommand(String.format("/%s 0 %s", ActiveHandAttack.NAME, type));

            } else if (right.isWeapon()) {
                player.performCommand(String.format("/%s 1 %s", ActiveHandAttack.NAME, type));

            } else {
                Message error = new Message(TextFormatting.RED + "Ошибка! У вас в руках нет оружия.");
                player.sendMessage(error);
                DuelMaster.end(duel);
                player.performCommand("/" + ToolPanel.NAME);
            }


        }

        // РУКОПАШКА
        else if (type.equals("unarmed")) {
            if (left.isUnarmed() && right.isUnarmed()) {
                // DETERMINE WHICH ONE IS ACTIVE
                // GENERATE CLICK PANEL
                Message message = new Message(START_MESSAGE, ChatColor.COMBAT);

                MessageComponent mes = new MessageComponent("[Атака левой] ", ChatColor.BLUE);
                mes.setClickCommand("/" + ActiveHandAttack.NAME + " 0 " + type);
                message.addComponent(mes);

                mes = new MessageComponent("[Атака правой] ", ChatColor.BLUE);
                mes.setClickCommand("/" + ActiveHandAttack.NAME + " 1 " + type);
                message.addComponent(mes);

                MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
                //toRoot.setBold(true);
                toRoot.setClickCommand("/toroot ActiveHand " + attacker.getName());
                message.addComponent(toRoot);


                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "ActiveHand", getCommandSenderAsPlayer(sender), attacker);
            } else if (left.isUnarmed()) {
                player.performCommand("/" + ActiveHandAttack.NAME + " 0 " + type);
            } else {
                player.performCommand("/" + ActiveHandAttack.NAME + " 1 " + type);
            }
        }

        // БОРЬБА
        else {}
    }
}
