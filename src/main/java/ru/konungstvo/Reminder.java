package ru.konungstvo;

import java.io.Serializable;

public class Reminder implements Serializable {
    private String playerName;
    private String content;

    public Reminder(String playerName, String content) {
        this.playerName = playerName;
        this.content = content;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
