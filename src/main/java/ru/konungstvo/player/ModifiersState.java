package ru.konungstvo.player;

import java.util.HashMap;

public class ModifiersState {
    private HashMap<String, Integer> modifiers;
    public static final String bodypart = "bodypart";
    public static final String damage = "damage";
    public static final String dice = "dice";
    public static final String serial = "serial";
    public static final String percent = "percent";
    public static final String stationary = "stationary";
    public static final String cover = "cover";
    public static final String optic = "optic";
    public static final String bipod = "bipod";
    public static final String stunning = "stunning";
    public static final String capture = "capture";
    public static final String noriposte = "noriposte";
    public static final String shieldwall = "shieldwall";
    public static final String suppressing = "suppressing";

    public ModifiersState() {
        this.modifiers = new HashMap<>();
    }

    public void setModifier(String type, int mod) {
//        if (type.equals(serial)) {
//            this.modifiers.remove(serial, 1);
//        }
//        System.out.println(this.modifiers);
        if (mod == -666) {
            this.modifiers.remove(type);
//            System.out.println(this.modifiers);
            return;
        }
        this.modifiers.put(type, new Integer(mod));
        System.out.println(this.modifiers);
//        System.out.println("Put " + mod + " for " + type);
    }

    public Integer getModifier(String type) {
//        System.out.println("Requested " + type + ", found " + this.modifiers.get(type));
        return this.modifiers.get(type);
    }

    public void purge() {
        this.modifiers = new HashMap<>();
    }

    @Override
    public String toString() {
        return "ModifiersState{" +
                "modifiers=" + modifiers +
                '}';
    }

    public boolean isNoModifier(String name) {
        if (getModifier(name) == null) return true;
        if (getModifier(name) == -666) return true;
        if (getModifier(name) == 0) return true;
        return false;
    }

    public boolean isModifier(String name) {
        if (getModifier(name) == null) return false;
        if (getModifier(name) != 1) return false;
        return true;
    }
}