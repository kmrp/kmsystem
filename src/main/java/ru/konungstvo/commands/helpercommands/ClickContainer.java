package ru.konungstvo.commands.helpercommands;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.ITextComponent;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;

public class ClickContainer {
    private int id;
    private String name;
    private ITextComponent message;
    private String start_hook;
    private String end_hook;

    public static void deleteContainer(String name, EntityPlayerMP sender) {
        // REMOVE PREVIOUS PANEL
//        System.out.println("Deleting container " + name);
        ClickContainerMessage remove = new ClickContainerMessage(name, true);
        KMPacketHandler.INSTANCE.sendTo(remove, sender);
        System.out.println("Deleting container " + name + " " + sender.getName());
    }

    public ClickContainer(int id, String name) {
        this(id, name, "", "");
    }

    public ClickContainer(int id, String name, String start_hook, String end_hook) {
        this.id = id;
        this.name = name;
        this.start_hook = start_hook;
        this.end_hook = end_hook;
        this.message = null;

    }

    public ITextComponent getMessage() {
        return message;
    }

    public void setMessage(ITextComponent message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStart_hook() {
        return start_hook;
    }

    public void setStart_hook(String start_hook) {
        this.start_hook = start_hook;
    }

    public String getEnd_hook() {
        return end_hook;
    }

    public void setEnd_hook(String end_hook) {
        this.end_hook = end_hook;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
