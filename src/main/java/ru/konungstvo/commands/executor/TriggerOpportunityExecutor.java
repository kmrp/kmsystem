package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TriggerOpportunityExecutor extends CommandBase {

    public static final String NAME = "triggeropportunity";

    @Override
    public String getName() {
        return "triggeropportunity";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        if(args.length > 0) oldContainer = args[0];
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();

        boolean riders = false;
        if (oldContainer.equals("stop")) {
            subordinate.clearOpportunities();
            return;
        }

        // REMOVE PREVIOUS PANEL
        if (!oldContainer.isEmpty()) {
            ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        }

        if (args.length > 1 && args[1].equals("riders")) {
            subordinate.triggerOpportunityForRiders();
            riders = true;
        }

        boolean proced = false;
        if (subordinate.getEngagedBy().size() > 0) {
            ArrayList<Player> opp = new ArrayList<Player>(subordinate.getEngagedBy());
            for (Player opponent : opp) {
                proced = opponent.executeOpportunity();
                if (proced) break;
            }
        }
        if (!proced && !riders) player.performCommand("/toolpanel");


    }
}
