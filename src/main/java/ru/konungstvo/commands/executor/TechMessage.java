package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

import java.util.Arrays;
import java.util.List;

public class TechMessage extends CommandBase {


    @Override
    public String getName() {
        return "techmessage";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
//
//        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 2) {
//            player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
            return;
        }

        Player player = DataHolder.inst().getPlayer(args[0]);

        String name = sender.getName();
        System.out.println(name);
        if (!name.contains("CustomNPC")) return;

        String msg = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        msg = msg.replaceAll("&([a-z0-9])", "§$1");
        Message message = new Message(msg, ChatColor.GMCHAT);
        Range range = player.getDefaultRange();
        if (range.equals(Range.NORMAL)) range = Range.QUIET;
        //message.addComponent(new MessageComponent(" (" + range.getSign() + ")", ChatColor.DARK_GRAY));
        ServerProxy.sendMessageFromAndInformMasters(player, message, range);
    }

}

