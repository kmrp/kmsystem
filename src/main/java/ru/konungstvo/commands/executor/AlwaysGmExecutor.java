package ru.konungstvo.commands.executor;

import ru.konungstvo.chat.range.Range;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

@Deprecated
public class AlwaysGmExecutor {
    public static String execute(Player executor, String[] args) {
        if (!executor.hasPermission(Permission.GM)) {
            throw new PermissionException("При выполнении alwaysgm: ", "Недостаточно прав!");
        }
        if (args.length == 0) {
            if (executor.getDefaultRange().equals(Range.GM)) {
                return "§7Автоматический ГМ-чат §aвключён!§f";
            }
            return "§7Автоматический ГМ-чат §4выключен!§f";
        }

        switch (args[0]) {
            case "help":
                return "§6Usage:\n" +
                        "§f/alwaysgm - check condition\n" +
                        "/alwaysgm on - turn on\n" +
                        "/alwaysgm off - turn off\n" +
                        ":message - regular chat (if alwaysgm is on)";
            case "on":
                executor.setDefaultRange(Range.GM);
                return "§7Автоматический ГМ-чат теперь §aвключён!§f";
            case "off":
                executor.setDefaultRange(Range.NORMAL);
                return "§7Автоматический ГМ-чат теперь §4выключен!§f";

        }
        return null;
    }
}
