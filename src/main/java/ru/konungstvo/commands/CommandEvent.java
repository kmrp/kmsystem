package ru.konungstvo.commands;

import ru.konungstvo.player.Player;

public class CommandEvent {
    private Player executor;
    private String command;
    private String[] args;
    private String response;

    public CommandEvent(Player player, String command, String[] args) {
        this.executor = player;
        this.command = command;
        this.args = args;
        this.response = null;
        process();
    }

    public void process() {
        this.response = CommandFactory.execute(executor, command, args);
    }

    public String getResponse() {
        return response;
    }
}
