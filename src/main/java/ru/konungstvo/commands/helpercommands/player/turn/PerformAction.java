package ru.konungstvo.commands.helpercommands.player.turn;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.HelpingActionExecutor;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.attack.AttackCommand;
import ru.konungstvo.commands.helpercommands.player.movement.MoveExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

public class PerformAction  extends CommandBase {
    public static final String NAME = "performaction";
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = player;
        if (player.getSubordinate() != null) {
            player = player.getSubordinate();
            subordinate = player;
        }
        // REMOVE PREVIOUS PANEL
        ClickContainer.deleteContainer("ToolPanel", getCommandSenderAsPlayer(sender));

        if (args.length > 0 && args[0].equals("from_movement")) {
            System.out.println("Завершаем движение при выборе действия");
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
        } else if (args.length > 0 && args[0].equals("stopprep")) {
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
            player.removeStatusEffect(StatusType.PREPARING);
        } else if (args.length > 0 && args[0].equals("stopcpr")) {
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, subordinate.getCprTo(), subordinate.getDefaultRange(), new Message(subordinate.getName() + " прекращает зажимать раны " + subordinate.getCprTo().getName() + ".", ChatColor.RED));
            subordinate.removeCprTo();
        }else if (args.length > 0 && args[0].equals("stopdef")) {
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
            player.setDefensiveStance(false);
        }

        if (args.length > 1 && args[1].equals("cancel_charge")) {
            subordinate.setChargeHistory(ChargeHistory.NONE);
        }

        // ASSEMBLE PANEL
        Message panel = new Message("Выберите действие!\n", ChatColor.COMBAT);

        if (subordinate.getCprTo() != null) {
            MessageComponent action = new MessageComponent("[Прервать зажимание] ", ChatColor.RED);
            action.setClickCommand("/performaction stopcpr");

            MessageComponent pass = new MessageComponent("[Продолжать зажимание] ", ChatColor.BLUE);
            pass.setClickCommand("/" + NextExecutor.NAME + " cpraidtarget " + subordinate.getCprTo().getName());

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            panel.addComponent(action);
            panel.addComponent(pass);
            panel.addComponent(modifiers);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        } else if (player.hasStatusEffect(StatusType.PREPARING)) {
            MessageComponent action = new MessageComponent("[Прервать подготовку] ", ChatColor.RED);
            action.setClickCommand("/performaction stopprep");

            MessageComponent pass = new MessageComponent("[Продолжать подготовку] ", ChatColor.BLUE);
            pass.setClickCommand("/" + NextExecutor.NAME + " magic");

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            panel.addComponent(action);
            panel.addComponent(pass);
            panel.addComponent(modifiers);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        } else if (player.hasDefensiveStance()) {
            MessageComponent action = new MessageComponent("[Прервать глухую оборону] ", ChatColor.RED);
            action.setClickCommand("/performaction stopdef");

            MessageComponent pass = new MessageComponent("[Продолжать глухую оборону] ", ChatColor.BLUE);
            pass.setClickCommand("/defensivestance continue");

            MessageComponent def = new MessageComponent("[Защита союзника] ", ChatColor.BLUE);
            def.setClickCommand("/helpingaction allydefense performaction");

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            panel.addComponent(action);
            panel.addComponent(pass);
            panel.addComponent(def);
            panel.addComponent(modifiers);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        } else {
            MessageComponent attack = new MessageComponent("[Атака оружием] ", ChatColor.BLUE);
            attack.setClickCommand("/" + AttackCommand.NAME + " armed");
            panel.addComponent(attack);

            MessageComponent unarmed = new MessageComponent("[Без оружия] ", ChatColor.BLUE);
            unarmed.setClickCommand("/" + AttackCommand.NAME + " unarmed");
            panel.addComponent(unarmed);

            MessageComponent helping = new MessageComponent("[Оказать помощь] ", ChatColor.BLUE);
            helping.setClickCommand("/" + HelpingActionExecutor.NAME + " PerformAction");
            panel.addComponent(helping);

            if (player.hasStatusEffect(StatusType.BURNING)) {
                MessageComponent extinguish = new MessageComponent("[Потушиться] ", ChatColor.RED);
                extinguish.setClickCommand("/" + NextExecutor.NAME + " extinguish");
                extinguish.setHoverText("Упасть и кататься!", TextFormatting.BLUE);
                panel.addComponent(extinguish);
            }

            MessageComponent customAction = new MessageComponent("[Конец движения] ", ChatColor.BLUE);
            customAction.setClickCommand("/" + NextExecutor.NAME + " resetmovement");
            panel.addComponent(customAction);

//        MessageComponent pass = new MessageComponent("[Закончить ход] ", ChatColor.BLUE);
//        pass.setClickCommand("/" + NextExecutor.NAME);

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton PerformAction");
            panel.addComponent(modifiers);

            if (player.getMovementHistory().equals(MovementHistory.NONE) || player.getMovementHistory().equals(MovementHistory.LAST_TIME)) {
                MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
                //toRoot.setBold(true);
                toRoot.setClickCommand("/" + ToRoot.NAME + " PerformAction" + " ToolPanel");
                panel.addComponent(toRoot);
            }


            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        }

    }
}
