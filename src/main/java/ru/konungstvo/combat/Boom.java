package ru.konungstvo.combat;

import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.equipment.Armor;
import ru.konungstvo.combat.equipment.ArmorPiece;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.movement.MovementTracker;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.ArmorDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Boom implements Serializable, Cloneable {
    private int combatid;
    private ArrayList<Player> targetsList;
    private HashMap<Player, String> defenseMap;
    private HashMap<Player, Integer> coverMap;
    private BlockPos blockPos;
    private int damage;
    private int dimension;
    private boolean defendable;
    private boolean ignorearmor;
    private boolean nextWhenDone = true;
    private int toBeBoomIn = -666;
    private boolean toBeRevealed = false;
    private boolean npcSpawned = false;
    private boolean inactive = false;
    private int despawnMeAt = -666;
    boolean keepExploding = false;
    private ArrayList<BlockPos> blockPosesList;
    private int uuid;


    private BoomType boomType;



    public Boom(BlockPos blockPos, BoomType boomType, int damage, int id, int dimension, boolean defendable, boolean ignorearmor) {
        this.targetsList = new ArrayList<>();
        this.defenseMap = new HashMap<Player, String>();
        this.coverMap = new HashMap<Player, Integer>();
        this.blockPosesList = new ArrayList<BlockPos>();
        this.boomType = boomType;
        this.blockPos = blockPos;
        this.damage = damage;
        this.combatid = id;
        this.dimension = dimension;
        this.defendable = defendable;
        this.ignorearmor = ignorearmor;
        this.uuid = Sequence.nextValue();
    }

    public void addTarget(Player player) {
        if (!targetsList.contains(player)) targetsList.add(player);
    }

    public void addTargets(ArrayList<Player> targets) {
        targetsList = targets;
    }

    public void removeTarget(Player player) {
        targetsList.remove(player);
    }

    public ArrayList<Player> getTargetsList() {
        return targetsList;
    }

    public boolean hasTarget(Player player) {
        return targetsList.contains(player);
    }

    public void addDefense(Player player, String defense) {
        removeTarget(player);
        defenseMap.put(player, defense);
    }

    public void addCover(Player player, Integer cover) {
        //removeTarget(player);
        coverMap.put(player, cover);
    }

    public void removeCover(Player player) {
        //removeTarget(player);
        coverMap.remove(player);
    }

    public Integer getCover(Player player) {
        //removeTarget(player);
        if (!coverMap.containsKey(player)) return 0;
        return coverMap.get(player);
    }

    public String getTargetsString() {
        StringBuilder targetsName = new StringBuilder();
        for (Player target : targetsList) {
            targetsName.append(target.getName()).append(" ");
//            if (DataHolder.inst().isNpc(target.getName())) continue;
//            target.performCommand("/boomd");
        }

        return targetsName.toString().trim();
    }


    public void doBoom() {
        for (Map.Entry<Player, String> defense : defenseMap.entrySet()) {
            int armor = 0;
            int shield = 0;
            int magicShield = 0;
            if (defense.getValue().equals("safe")) {
                continue;
            } else {
                Player player = defense.getKey();
                System.out.println(player.getName());
                int distance = (int) Math.floor(ServerProxy.getDistanceBetween(blockPos, player));
                if (damage < distance) {
                    Message wound = new Message("", ChatColor.RED);
                    MessageComponent wound111 = new MessageComponent("Игрок " + player.getName() + " успешно защищается от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                    MessageComponent wound222 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + "§6=§4" + (damage-distance) + "§c)");
                    wound.addComponent(wound111);
                    wound.addComponent(wound222);
                    ServerProxy.sendMessageFromAndInformMasters(player, wound);
                    continue;
                }
                boolean succblock = false;
                if (defense.getValue().contains("blocking")) {
                    Vec3d nvector = new Vec3d(blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5);
                    EntityLivingBase forgeplayer = player.getEntity();
                    Vec3d vector1 = nvector.subtract(forgeplayer.getPositionVector());
                    Vec3d playerDirection = forgeplayer.getLookVec();
                    double angleDir1 = (Math.atan2(vector1.z, vector1.x) / 2 / Math.PI * 360 + 360) % 360;
                    double angleLook1 = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                    double angle1 = (angleDir1 - angleLook1 + 360) % 360;

                    System.out.println("angle1: " + angle1);
                    if (angle1 <= 67.5 || angle1 >= 292.5) {
                        succblock = true;
                    }
                }

                if (boomType != BoomType.GAS && boomType != BoomType.WEAK_GAS) {
                    if (defense.getValue().contains("blocking")) {
                        int hand = Integer.parseInt(defense.getValue().split(":")[1]);
                        ItemStack shieldIs = player.getItemForHand(hand);
                        if (WeaponTagsHandler.hasWeaponTags(shieldIs)) {
                            WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(player.getItemForHand(hand));
                            if (weaponTagsHandler.isShield()) {
                                Shield shieldS = new Shield(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon());
                                if (!shieldS.getType().equals("малый")) {
                                    SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% блокирование");
                                    sdm.build();
                                    ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                                    //ServerProxy.informDistantMasters(player, sdm, Range.NORMAL.getDistance());
                                    if ((sdm.getDice().getResult() >= 1 || succblock) && (succblock || boomType != BoomType.FIRE || sdm.getDice().getResult() >= 2)) {
                                        shield = shieldS.getDamage();
                                        WeaponDurabilityHandler defenderDurHandler = player.getDurHandlerForHand(hand);

                                        if (defenderDurHandler != null && defenderDurHandler.hasDurabilityDict()) {
                                            if (defenderDurHandler.getPercentageRatio() <= 25) {
                                                int ratio = defenderDurHandler.getPercentageRatio();
                                                if (ratio < 0) {
                                                    shield = 0;
                                                } else if (ratio == 0) {
                                                    shield = shield / 2;
                                                } else {
                                                    Random random = new Random();
                                                    int randomResult = random.nextInt(100) + 1;
                                                    System.out.println(randomResult);
                                                    if (randomResult <= (26 - ratio)) {
                                                        shield -= 1;
                                                    }
                                                }
                                            }
                                            if (damage - distance > shield) {
                                                defenderDurHandler.takeAwayDurability(Math.max(1, Math.min(damage - distance, shield)));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Armor arm = player.getArmor();
                    ArmorPiece armorPiece = null;
                    if (arm != null) {
                        armor = arm.getDefense();
                        armorPiece = arm.getArmorPiece(BodyPart.THORAX);
                    }

                    if (armorPiece != null) {

                        ArmorDurabilityHandler armorDurHandler = player.getDurHandlerForArmorPiece(armorPiece.getPart().getName());
                        if (armorDurHandler != null && armorDurHandler.hasArmorDict()) {
                            if (armorDurHandler.getPercentageRatioForSegment(BodyPart.THORAX.getName()) <= 25) {
                                int ratio = armorDurHandler.getPercentageRatioForSegment(BodyPart.THORAX.getName());
                                if (ratio < 0) {
                                    armor = 0;
                                } else if (ratio == 0) {
                                    armor = (int) Math.floor((double) armor / 2);
                                } else {
                                    Random random = new Random();
                                    int randomResult = random.nextInt(100) + 1;
                                    System.out.println(randomResult);
                                    if (randomResult <= (50 - ratio)) {
                                        if (armor > 0) armor = -1;
                                    }
                                }
                            }
                            if (damage - (distance + shield) > armor) {
                                armorDurHandler.takeAwayDurabilityForAllSegments(Math.max((damage - (distance + shield + armor)) /2, 1));
                            }
                        }
                    }

                    armor += player.getHardened();
                }

                if (boomType != BoomType.GAS && boomType != BoomType.WEAK_GAS) {
                    magicShield = player.getMagicShield();
                    armor += magicShield;
                }

                Message wound = new Message("", ChatColor.RED);
                int cover = getCover(player);
                String type = "";
                if (ignorearmor) {
                    magicShield = 0;
                    armor = 0;
                    shield = 0;
                }
                switch (boomType) {
                    case DEFAULT:
                    case STUNNING:
                        int realdamage = damage - (distance + armor + shield + cover);
                        String addDesc = "";
                        System.out.println("testTESTTESTESTTESTEST" + realdamage + " " + distance + " " + armor);
                        if (magicShield != 0 && (realdamage + magicShield) > 0) {
                            player.adjustMagicShield(realdamage + magicShield);
                        }
                        if (realdamage < 0) {
                            MessageComponent wound1 = new MessageComponent("Игрок " + player.getName() + " успешно защищается от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                            MessageComponent wound2 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + realdamage + "§c)");
                            wound.addComponent(wound1);
                            wound.addComponent(wound2);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        if (boomType == BoomType.DEFAULT) {
                            type = player.inflictDamage(realdamage, "взрыв");
                        } else {
                            if (realdamage > 9) {
                                addDesc = "[нокаут]";
                            } else if (realdamage > 6) {
                                addDesc = "[оглушение:станет переходной через 5 ходов]";
                            } else if (realdamage > 3) {
                                addDesc = "[оглушение:станет переходной через 3 хода]";
                            } else if (realdamage >= 0) {
                                addDesc = "[оглушение:станет переходной через ход]";
                            }

                            type = player.inflictDamage(realdamage, "взрыв " + addDesc);

                            if (realdamage > 6) {
                                if (!player.cantBeStunned()) {
                                    player.addStatusEffect("оглушение", StatusEnd.TURN_END, 1,  StatusType.STUNNED);
                                    addDesc += "\n§3<пропуск хода>";
                                }
                            }
                        }
                        boolean fallen = false;
                        if (realdamage > 6 && !player.isLying() && !player.cantBeCharged()) {
                            SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% сила");
                            sdm.build();
                            ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                            //ServerProxy.informDistantMasters(player, sdm, Range.NORMAL.getDistance());
                            if (sdm.getDice().getResult() < 4) {
                                fallen = true;
                                player.makeFall();
                            }
                        }

                        MessageComponent wound1 = new MessageComponent("Игроку " + player.getName() + " взрывом (" + boomType.color() + boomType.toString() + "§c) нанесена ", ChatColor.RED);
                        wound.addComponent(wound1);

                        MessageComponent typeC = new MessageComponent(type, ChatColor.RED);

                        if (fallen) {
                            addDesc += "\n§3<сбитие с ног>";
                        }

                        if (!addDesc.isEmpty()) {
                            typeC.setHoverText(new TextComponentString("§3" + addDesc.trim()));
                            typeC.setUnderlined(true);
                        }
                        wound.addComponent(typeC);

                        MessageComponent wound2 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + realdamage + "§c)");
                        wound.addComponent(wound2);
                        break;

                    case FIRE:
                        if (magicShield != 0 && (distance - magicShield) <= damage) {
                            player.adjustMagicShield(damage - distance);
                        }
                        if (distance + magicShield > damage) {
                            MessageComponent wound11 = new MessageComponent("Игрок " + player.getName() + " успешно защищается от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                            MessageComponent wound22 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + (damage-(distance+magicShield)) + "§c)");
                            wound.addComponent(wound11);
                            wound.addComponent(wound22);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        if (player.cantBurn()) continue;

                        player.addStatusEffect("горение", StatusEnd.TURN_END, 5,  StatusType.BURNING);
                        if (shield > 0) {
                            MessageComponent woundb = new MessageComponent("Игрок " + player.getName() + " §cзакрывается от зажигательной гранаты щитом и избежагает ранений, но всё равно §6загорается§c!", ChatColor.RED);
                            wound.addComponent(woundb);
                            break;
                        }
                        type = player.inflictDamage(5, "взрыв зажигательной");
                        MessageComponent woundb = new MessageComponent("Игроку " + player.getName() + " §cзажигательной гранатой нанесена " + type + ", игрок §6загорается§c!", ChatColor.RED);
                        wound.addComponent(woundb);
                        break;
                    case GAS:
                    case WEAK_GAS:
                        if (distance > damage) {
                            MessageComponent wound111 = new MessageComponent("Игрок " + player.getName() + " успешно защищается от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                            MessageComponent wound222 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + (damage-distance) + "§c)");
                            wound.addComponent(wound111);
                            wound.addComponent(wound222);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        if (boomType != BoomType.WEAK_GAS) {
                            SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% выносливость");
                            sdm.build();
                            ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                            if (sdm.getDice().getResult() < 2) {
                                type = player.inflictDamage(7, "газ");
                            } else {
                                type = player.inflictDamage(4, "газ");
                            }
                        } else {
                            type = player.inflictDamage(4, "газ");
                        }

                        MessageComponent woundg = new MessageComponent("Игроку " + player.getName() + " " + boomType.color() + "газом§c нанесена ", ChatColor.RED);
                        wound.addComponent(woundg);
                        wound.addComponent(new MessageComponent(type, ChatColor.RED));
                }


                ServerProxy.sendMessageFromAndInformMasters(player, wound);
                if (type.equals(WoundType.CRITICAL.getDesc()) || type.equals(WoundType.DEADLY.getDesc())) {
                    try {
                        if (DataHolder.inst().isNpc(player.getName())) {
                            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(player.getName());
                            npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                            String desc = npc.wrappedNPC.getDisplay().getTitle();
                            npc.wrappedNPC.getDisplay().setTitle((desc + " [крит]").trim());
                            npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //actualDefender.makeFall();
                    try {
                        Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                        gm.performCommand("/queue remove " + player.getName());
                        //gm.performCommand("/combat remove " + player.getName());
                        Combat combat = DataHolder.inst().getCombatForPlayer(player.getName());
                        if ((!DataHolder.inst().isNpc(player.getName()) || player.isPersistent()) && player.getWoundPyramid().getNumberOfWounds(WoundType.DEADLY) == 0) {
                            if (boomType != BoomType.STUNNING && combat != null) combat.addLethalCompany(player);
                        } else {
                            gm.performCommand("/combat remove " + player.getName());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if (DataHolder.inst().isNpc(player.getName())) {
                        NPC npc = (NPC) DataHolder.inst().getPlayer(player.getName());
                        npc.setWoundsInDescription();
                    }
                }
            }
        }
        //DataHolder.inst().getCombat(combatid).removeBoom(this);
        System.out.println("DESPAWN DESPAWN DESPAWN");
        System.out.println("ВЗРЫВ-" + uuid);

        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom("ВЗРЫВ-" + uuid, dimension);
        if (boomType == BoomType.DEFAULT || boomType == BoomType.STUNNING) NpcAPI.Instance().getIWorld(dimension).explode(blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5, 0, false, false);
        else if (boomType == BoomType.GAS || boomType == BoomType.FIRE || boomType == BoomType.WEAK_GAS || boomType == BoomType.SMOKE) NpcAPI.Instance().getIWorld(dimension).playSoundAt(npc.wrappedNPC.getPos(), "minecraft:entity.creeper.primed",1, 1);
        //NpcAPI.Instance().getIWorld(dimension).playSoundAt(npc.wrappedNPC.getPos(), "customnpcs:misc.old_explode", 1 , 1);


        if (boomType.equals(BoomType.FIRE)) {
            IBlockState block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "fake_fire"))).getDefaultState();
            for (int x = blockPos.getX() - damage; x <= blockPos.getX() + damage; x++) {
                for (int y = blockPos.getY() - damage/2; y <= blockPos.getY() + damage/2; y++) {
                    for (int z = blockPos.getZ() - damage; z <= blockPos.getZ() + damage; z++) {
                        if (!npc.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getMaterial().isReplaceable())
                            continue;
                        BlockPos bpos = new BlockPos(x, y, z);
                        if (npc.getEntityWorld().getBlockState(bpos.offset(EnumFacing.DOWN)).getMaterial().isReplaceable())
                            continue;
                        if (npc.getEntityWorld().getBlockState(bpos.offset(EnumFacing.DOWN)).equals(block))
                            continue;
                        double c = MovementTracker.getDistanceBetween(
                                blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5,
                                x + 0.5, y, z + 0.5
                        );
                        if (c > damage) continue;
                        npc.getEntityWorld().setBlockState(bpos, block);
                        blockPosesList.add(bpos);
                    }
                }
            }

            updateDesc("Горение до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + 3));

            despawnMeAt = (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + 3);


        }
        inactive = true;
        if (boomType == BoomType.GAS) {
            Random ran = new Random();
            Player master = DataHolder.inst().getMasterForCombat(combatid);
            int randomResult = ran.nextInt(3) + 1 + 2;
            Message message1 = new Message("§4Газовый снаряд начинает заволакивать всё ядовитым дымом, газ продлится ещё (d3+2) " + randomResult + " раундов.");
            ServerProxy.sendMessageFromAndInformMasters(blockPos, master, message1);
            IBlockState block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "gas"))).getDefaultState();
            for (int x = blockPos.getX() - damage; x <= blockPos.getX() + damage; x++) {
                for (int y = blockPos.getY() - damage; y <= blockPos.getY() + damage; y++) {
                    for (int z = blockPos.getZ() - damage; z <= blockPos.getZ() + damage; z++) {
                        BlockPos bpos = new BlockPos(x, y, z);
                        if(!npc.getEntityWorld().isAirBlock(bpos) && !npc.getEntityWorld().getBlockState(bpos).equals(block))
                            continue;

                        double c = MovementTracker.getDistanceBetween(
                                blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5,
                                x + 0.5, y, z + 0.5
                        );
                        if (c > damage) continue;
                        npc.getEntityWorld().setBlockState(bpos, block);
                        blockPosesList.add(bpos);
                    }
                }
            }
            if (!npcSpawned) {
                if (master.getEntity().getEntityWorld().getBlockState(blockPos.down()).getMaterial().isReplaceable()) {
                    if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("minecraft", "glass" )) == null) return;
                    IBlockState block1 = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "glass"))).getDefaultState();
                    master.getEntity().world.setBlockState(blockPos.down(), block1);
                }
                spawnNpc("Эпицентр газа, до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult));
            } else {
                updateDesc("Эпицентр газа, до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult));
            }
            despawnMeAt = DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult;
            boomType = BoomType.WEAK_GAS;
            defendable = false;
            keepExploding = true;
        } else if (npc != null && boomType != BoomType.WEAK_GAS && boomType != BoomType.FIRE) {
            System.out.println("despawn");
            npc.wrappedNPC.despawn();
        }

        if (boomType == BoomType.WEAK_GAS) {
            updateDesc("Эпицентр газа, до раунда номер " + (despawnMeAt));
        }

        if (nextWhenDone && !DataHolder.inst().getCombat(combatid).processExplosions2()) DataHolder.inst().getMasterForCombat(combatid).performCommand("/next");
    }

    public int getUuid() {
        return uuid;
    }

    public BoomType getBoomType() {
        return boomType;
    }

    public boolean isDefendable() {
        return defendable;
    }

    public void setNextWhenDone(boolean nextWhenDone) {
        this.nextWhenDone = nextWhenDone;
    }

    public void setToBeBoomIn(int toBeBoomIn) {
        this.toBeBoomIn = toBeBoomIn;
    }

    public int getToBeBoomIn() {
        return toBeBoomIn;
    }

    public void setToBeRevealed(boolean toBeRevealed) {
        this.toBeRevealed = toBeRevealed;
    }

    public boolean getToBeRevealed() {
        return toBeRevealed;
    }

    public int getDespawnMeAt() {
        return despawnMeAt;
    }

    public void wipeDespawnMeAt() {
        despawnMeAt = -666;
    }

    public void findTargetsAndStartExplosion () {
        Player master = DataHolder.inst().getMasterForCombat(combatid);
        EntityLivingBase entity = master.getEntity();
        if (toBeRevealed) {
            revealAndPrepare();
        } else if (npcSpawned) {
            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom("ВЗРЫВ-" + uuid, dimension);
            toBeBoomIn = -666;
            if (npc != null) {
                npc.wrappedNPC.getDisplay().setName("ВЗРЫВ-" + uuid);
                npc.wrappedNPC.getDisplay().setTitle(toBeBoomIn == -666 ? (boomType.toString() + " [" + String.valueOf(damage) + "]") : ("БУДЕТ ВЗРЫВ, взорвётся через " + toBeBoomIn));
                BlockPos npcPos = npc.getPosition();
                blockPos = new BlockPos(npcPos.getX(), npcPos.getY(), npcPos.getZ());
            }
        } else {
            spawnNpc();
        }

        ArrayList<Player> targets = (ArrayList<Player>) ServerProxy.getAllFightersInRangeForCombat(blockPos, master, damage);

        if (boomType.equals(BoomType.SMOKE)) {
            inactive = true;
            Random ran = new Random();
            int randomResult = ran.nextInt(3) + 1 + 2;
            Message message = new Message("§4Дымовой снаряд начинает заволакивать всё дымом, дым продлится ещё (d3+2) " + randomResult + " раундов.");
            ServerProxy.sendMessageFromAndInformMasters(blockPos, master, message);
            IBlockState block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "smoke"))).getDefaultState();
            for (int x = blockPos.getX() - damage; x <= blockPos.getX() + damage; x++) {
                for (int y = blockPos.getY() - damage; y <= blockPos.getY() + damage; y++) {
                    for (int z = blockPos.getZ() - damage; z <= blockPos.getZ() + damage; z++) {
                        BlockPos bpos = new BlockPos(x, y, z);
                        if(!entity.getEntityWorld().isAirBlock(bpos) && !entity.getEntityWorld().getBlockState(bpos).equals(block))
                            continue;

                        double c = MovementTracker.getDistanceBetween(
                                blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5,
                                x + 0.5, y, z + 0.5
                        );
                        if (c > damage) continue;
                        entity.getEntityWorld().setBlockState(bpos, block);
                        blockPosesList.add(bpos);
                    }
                }
            }
            if (!npcSpawned) {
                if (entity.getEntityWorld().getBlockState(blockPos.down()).getMaterial().isReplaceable()) {
                    if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("minecraft", "glass" )) == null) return;
                    IBlockState block1 = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "glass"))).getDefaultState();
                    entity.world.setBlockState(blockPos.down(), block1);
                }
                spawnNpc("Эпицентр дыма, до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult));
            } else {
                updateDesc("Эпицентр дыма, до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult));
            }
            despawnMeAt = DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult;
            inactive = true;
            if (nextWhenDone && !DataHolder.inst().getCombat(combatid).processExplosions2()) master.performCommand("/next");
            return;
        }


        if (targets.isEmpty()) {
            Message message = new Message("§4Происходит взрыв (" + boomType.color() + boomType.toString() + " §8[§c" + damage + "§8]§4)! В зоне взрыва нет целей.");
            ServerProxy.sendMessageFromAndInformMasters(blockPos, master, message);
            if (boomType == BoomType.DEFAULT || boomType == BoomType.STUNNING) NpcAPI.Instance().getIWorld(dimension).explode((double) blockPos.getX() + 0.5, blockPos.getY(), (double) blockPos.getZ() + 0.5, 0, false, false);
            else if (boomType == BoomType.GAS || boomType == BoomType.FIRE || boomType == BoomType.WEAK_GAS) NpcAPI.Instance().getIWorld(dimension).playSoundAt(NpcAPI.Instance().getIPos((double) blockPos.getX() + 0.5, blockPos.getY(), (double) blockPos.getZ() + 0.5), "minecraft:entity.creeper.primed",1, 1);

            if (boomType.equals(BoomType.FIRE)) {
                IBlockState block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "fake_fire"))).getDefaultState();
                for (int x = blockPos.getX() - damage; x <= blockPos.getX() + damage; x++) {
                    for (int y = blockPos.getY() - damage/2; y <= blockPos.getY() + damage/2; y++) {
                        for (int z = blockPos.getZ() - damage; z <= blockPos.getZ() + damage; z++) {
                            if(!entity.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getMaterial().isReplaceable())
                                continue;
                            BlockPos bpos = new BlockPos(x, y, z);
                            if (entity.getEntityWorld().getBlockState(bpos.offset(EnumFacing.DOWN)).getMaterial().isReplaceable())
                                continue;
                            if (entity.getEntityWorld().getBlockState(bpos.offset(EnumFacing.DOWN)).equals(block))
                                continue;
                            double c = MovementTracker.getDistanceBetween(
                                    blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5,
                                    x + 0.5, y, z + 0.5
                            );
                            if (c > damage) continue;
                            entity.getEntityWorld().setBlockState(bpos, block);
                            blockPosesList.add(bpos);
                        }
                    }
                }
                if (!npcSpawned) {
                    if (entity.getEntityWorld().getBlockState(blockPos.down()).getMaterial().isReplaceable()) {
                        if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("minecraft", "glass" )) == null) return;
                        IBlockState block1 = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "glass"))).getDefaultState();
                        entity.world.setBlockState(blockPos.down(), block1);
                    }
                    spawnNpc("Горение до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + 3));
                } else {
                    updateDesc("Горение до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + 3));
                }
                despawnMeAt = (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + 3);
                inactive = true;
                if (nextWhenDone && !DataHolder.inst().getCombat(combatid).processExplosions2()) master.performCommand("/next");
                return;
            }
            if (boomType == BoomType.GAS) {
                inactive = true;
                Random ran = new Random();
                int randomResult = ran.nextInt(3) + 1 + 2;
                Message message1 = new Message("§4Газовый снаряд начинает заволакивать всё ядовитым дымом, газ продлится ещё (d3+2) " + randomResult + " раундов.");
                ServerProxy.sendMessageFromAndInformMasters(blockPos, master, message1);
                IBlockState block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "gas"))).getDefaultState();
                for (int x = blockPos.getX() - damage; x <= blockPos.getX() + damage; x++) {
                    for (int y = blockPos.getY() - damage; y <= blockPos.getY() + damage; y++) {
                        for (int z = blockPos.getZ() - damage; z <= blockPos.getZ() + damage; z++) {
                            BlockPos bpos = new BlockPos(x, y, z);
                            if(!entity.getEntityWorld().isAirBlock(bpos) && !entity.getEntityWorld().getBlockState(bpos).equals(block))
                                continue;
                            double c = MovementTracker.getDistanceBetween(
                                    blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5,
                                    x + 0.5, y, z + 0.5
                            );
                            if (c > damage) continue;
                            entity.getEntityWorld().setBlockState(bpos, block);
                            blockPosesList.add(bpos);
                        }
                    }
                }
                if (!npcSpawned) {
                    if (entity.getEntityWorld().getBlockState(blockPos.down()).getMaterial().isReplaceable()) {
                        if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("minecraft", "glass" )) == null) return;
                        IBlockState block1 = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "glass"))).getDefaultState();
                        entity.world.setBlockState(blockPos.down(), block1);
                    }
                    spawnNpc("Эпицентр газа, до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult));
                } else {
                    updateDesc("Эпицентр газа, до раунда номер " + (DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult));
                }
                inactive = true;
                boomType = BoomType.WEAK_GAS;
                defendable = false;
                keepExploding = true;
                despawnMeAt = DataHolder.inst().getCombat(combatid).getReactionList().getRoundNumber() + randomResult;
                if (nextWhenDone && !DataHolder.inst().getCombat(combatid).processExplosions2()) master.performCommand("/next");
                return;
            }

            if (boomType == BoomType.WEAK_GAS && keepExploding) {
                updateDesc("Эпицентр газа, до раунда номер " + (despawnMeAt));
                if (nextWhenDone && !DataHolder.inst().getCombat(combatid).processExplosions2()) master.performCommand("/next");
                return;
            }
            if (npcSpawned) {
                EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom(  "ВЗРЫВ-" + uuid, dimension);
                if (npc != null) {
                    npc.wrappedNPC.despawn();
                }
            }
            inactive = true;
            if (nextWhenDone && !DataHolder.inst().getCombat(combatid).processExplosions2()) master.performCommand("/next");
            return;

        }

        if (entity.getEntityWorld().getBlockState(blockPos.down()).getMaterial().isReplaceable()) {
            if (ForgeRegistries.BLOCKS.getValue( new ResourceLocation("minecraft", "glass" )) == null) return;
            IBlockState block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("minecraft", "glass"))).getDefaultState();
            entity.world.setBlockState(blockPos.down(), block);
        }

        if (!npcSpawned) {
            spawnNpc();
        }

        addTargets(targets);

        StringBuilder targetsName = new StringBuilder();
        for (Player target : targets) {
            targetsName.append(target.getName()).append(" ");
            if (DataHolder.inst().isNpc(target.getName())) continue;
            target.performCommand("/boomd");
        }

        Message message = new Message("§4Происходит взрыв (" + boomType.color() + boomType.toString() + " §8[§c" + damage + "§8]§4)! §4Затронуты взрывом:\n§с[§a" + targetsName.toString().trim() + "§с]" +
                (defendable ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"));
        ServerProxy.sendMessageFromAndInformMasters(blockPos, master, message);
        for (Player target : targets) {
            if (DataHolder.inst().isNpc(target.getName())) {
                Message test = new Message("", ChatColor.BLUE);
                MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                testing.setUnderlined(true);
                testing.setClickCommand("/sub " + target.getName());
                test.addComponent(testing);
                ServerProxy.sendMessage(master, test);
                break;
            };
        }
    }

    private void spawnNpc(String desc) {
        DataHolder.inst().spawnGasNpc("ВЗРЫВ-" + getUuid(), ServerProxy.getForgePlayer(DataHolder.inst().getMasterForCombat(combatid).getName()), blockPos, desc);
        npcSpawned = true;
    }

    public void setBlockPos(BlockPos blockPos) {
        this.blockPos = blockPos;
    }

    public void spawnNpc() {
        DataHolder.inst().spawnBoomNpc("ВЗРЫВ-" + getUuid(), ServerProxy.getForgePlayer(DataHolder.inst().getMasterForCombat(combatid).getName()), blockPos, damage, boomType.toString(), toBeRevealed, toBeBoomIn);
        npcSpawned = true;
    }


    public void reveal() {
        toBeRevealed = false;
        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom(  "ВЗРЫВ-" + uuid, dimension);
        if (npc == null) {
            spawnNpc();
        } else {
            npc.wrappedNPC.getDisplay().setVisible(0);
            npc.wrappedNPC.getDisplay().setTitle(toBeBoomIn == -666 ? (boomType.toString() + " [" + String.valueOf(damage) + "]") : ("БУДЕТ ВЗРЫВ, взорвётся через " + toBeBoomIn));
        }
    }

    public void revealAndPrepare() {
        toBeRevealed = false;
        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom(  "ВЗРЫВ-" + uuid, dimension);
        toBeBoomIn = -666;
        if (npc == null) {
            spawnNpc();
        } else {
            npc.wrappedNPC.getDisplay().setName("ВЗРЫВ-" + uuid);
            npc.wrappedNPC.getDisplay().setVisible(0);
            npc.wrappedNPC.getDisplay().setTitle(toBeBoomIn == -666 ? (boomType.toString() + " [" + String.valueOf(damage) + "]") : ("БУДЕТ ВЗРЫВ, взорвётся через " + toBeBoomIn));
            BlockPos npcPos = npc.getPosition();
            blockPos = new BlockPos(npcPos.getX(), npcPos.getY(), npcPos.getZ());
        }
    }

    public void updateTimer() {
        if (!npcSpawned) return;
        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom(  "ВЗРЫВ-" + uuid, dimension);
        if (npc == null) return;
        npc.wrappedNPC.getDisplay().setTitle(toBeBoomIn == -666 ? (boomType.toString() + " [" + String.valueOf(damage) + "]") : ("БУДЕТ ВЗРЫВ, взорвётся через " + toBeBoomIn));
    }

    public void updateDesc(String desc) {
        if (!npcSpawned) return;
        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom(  "ВЗРЫВ-" + uuid, dimension);
        if (npc == null) return;
        npc.wrappedNPC.getDisplay().setTitle(desc);
    }

    public boolean isInactive() {
        return inactive;
    }

    public void activate() {
        inactive = false;
    }

    public void deactivate() {
        inactive = true;
    }

    public boolean keepExploding() {
        return keepExploding;
    }

    public void despawnNpc() {
        npcSpawned = false;
        inactive = true;
        keepExploding = false;
        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom(  "ВЗРЫВ-" + uuid, dimension);

        if (npc != null) {
            IBlockState block = null;
            switch (boomType) {
                case FIRE:
                    block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "fake_fire"))).getDefaultState();
                    break;
                case GAS:
                case WEAK_GAS:
                    block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "gas"))).getDefaultState();
                    break;
                case SMOKE:
                    block = Objects.requireNonNull(ForgeRegistries.BLOCKS.getValue(new ResourceLocation("pog", "smoke"))).getDefaultState();
                    break;
            }
            if (block != null) {
                Combat combat = DataHolder.inst().getCombat(combatid);
                if (combat != null) {
                    List<Boom> booms = combat.getBooms();
                    for (Boom boom : booms) {
                        if (boom.getDespawnMeAt() == -666) continue;
                        if (boom.getUuid() == getUuid()) continue;
                        if (boom.getDespawnMeAt() == getDespawnMeAt()) continue;
                        System.out.println("boom " + boom.getUuid() + " " + boom.blockPosesList.size());
                        System.out.println("before " + blockPosesList.size());
                        blockPosesList.removeAll(boom.blockPosesList);
                        System.out.println("after " + blockPosesList.size());
                    }
                    for (BlockPos bp : blockPosesList) {
                        if (!npc.getEntityWorld().getBlockState(bp).equals(block)) continue;
//                        boolean ignoreBp = false;
//                        for (Boom boom : booms) {
//                            if (boom.getDespawnMeAt() == -666) continue;
//                            if (boom.getUuid() == getUuid()) continue;
//                            if (boom.getDespawnMeAt() == getDespawnMeAt()) continue;
//                            System.out.println(boom.getUuid());
//                            for (BlockPos bpp : boom.blockPosesList) {
//                                if (bp.equals(bpp)) {
//                                    System.out.println("found");
//                                    System.out.println("x" + bp.getX() + " x" + bpp.getX() + " y" + bp.getY() + " y" + bpp.getY() + " z" + bp.getZ() + " z" + bpp.getZ());
//                                    ignoreBp = true;
//                                    break;
//                                }
//                            }
//                            if (ignoreBp) break;
//                        }
//                        if (ignoreBp) continue;
                        npc.getEntityWorld().setBlockToAir(bp);
                    }
                }
            }
            npc.wrappedNPC.despawn();
        }
        blockPosesList = new ArrayList<>();
    }

    boolean isGas() {
        return boomType.equals(BoomType.GAS) || boomType.equals(BoomType.WEAK_GAS);
    }
}
