package ru.konungstvo.combat.movement;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import noppes.mpm.MorePlayerModels;
import noppes.npcs.CustomNpcs;
import noppes.npcs.NoppesUtilPlayer;
import noppes.npcs.entity.EntityCustomNpc;
import noppes.npcs.entity.EntityNPCInterface;
import org.lwjgl.Sys;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.moves.DeprecatedAttack;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.gm.npcadder.NpcCommand;
import ru.konungstvo.commands.helpercommands.player.turn.PerformAction;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.MovementHelper;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;

import static ru.konungstvo.chat.ChatColor.COMBAT;

public class GoExecutor extends CommandBase {
    public static final String NAME = "go";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        int blocksToCover;
        Player runner = DataHolder.inst().getPlayer(sender.getName());
        Player sub = runner.getSubordinate();
        if (sub == null) {
            sub = runner;
        }

        if (args.length > 0 && args[0].equals("end")) {
            boolean forced = false;
            if (args.length > 1 && args[1].equals("forced")) forced = true;
            boolean antiforced = false;
            if (args.length > 1 && args[1].equals("antiforced")) antiforced = true;
            boolean charge = false;
            if (args.length > 2 && args[2].equals("charge")) charge = true;
            boolean jump = false;
            if ((args.length > 2 && args[2].equals("jump")) || sub.getMovementHistory() == MovementHistory.JUMP) jump = true;

            System.out.println("ending with move history " + runner.getMovementHistory()); //??? почему не sub

            double blocksRemaining = MovementTracker.dropSuspect(sender.getName());

            System.out.println("jump " + blocksRemaining);

///* INFORM PLAYER */
//            Message mes = new Message("Вы завершили передвижение. Блоков осталось: " +
//                    blocksRemaining, COMBAT
//            );
//            runner.sendMessage(mes);
//
///* INFORM MASTERS */
//            TextComponentString mesToGm = new TextComponentString("[GM] " + sub.getName() + " завершает передвижение, остаток: " +
//                    blocksRemaining
//            );
//            mesToGm.getStyle().setColor(TextFormatting.GOLD);
//            DataHolder.inst().informMasterForCombat(sub.getName(), mesToGm);

/* CHANGE MOVEMENT HISTORY */
            if (sub.getMovementHistory() != MovementHistory.FREELY) {
                //sub.setMovementHistory(MovementHistory.LAST_TIME); //TODO сделать только при беге/разбеге
            }
            ClickContainer.deleteContainer("MovementCounter", (EntityPlayerMP) sender);

/* MOVE NPC */
            boolean riding = false;
            if (DataHolder.inst().getPlayer(sender.getName()).getSubordinate() != null) {
                System.out.println("Moving npc");
                Player gm = DataHolder.inst().getPlayer(sender.getName());
                gm.performCommand(NpcCommand.NAME + " move " + gm.getSubordinate().getName());
                EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(sub.getName());
                if (sender.getCommandSenderEntity() != null) {
                    BlockPos senderBlock = sender.getCommandSenderEntity().getPosition();
                    npc.wrappedNPC.setHome(senderBlock.getX(), senderBlock.getY(), senderBlock.getZ());
                    //для катания
                    if (npc.getRidingEntity() instanceof EntityNPCInterface) {
                        riding = true;
                        EntityNPCInterface mount = (EntityNPCInterface) npc.getRidingEntity();
                        gm.performCommand(NpcCommand.NAME + " move " + mount.getName());
                        mount.wrappedNPC.setHome(senderBlock.getX(), senderBlock.getY(), senderBlock.getZ());
                        Player mountnpc = DataHolder.inst().getPlayer(mount.getName());
/* ПЕРЕДАЕМ СКОРОСТЬ ПАССАЖИРАМ */
                        for (Entity passenger : mount.getPassengers()) {
                            if (passenger instanceof EntityNPCInterface) {
                                EntityNPCInterface npcPassenger = (EntityNPCInterface) passenger;
                                DataHolder.inst().getPlayer(npcPassenger.getName()).setMovementHistory(mountnpc.getMovementHistory());
                                npcPassenger.wrappedNPC.setHome(senderBlock.getX(), senderBlock.getY(), senderBlock.getZ());
                            } else if (passenger instanceof EntityPlayerMP) {
                                EntityPlayerMP playerMP = (EntityPlayerMP) passenger;
                                DataHolder.inst().getPlayer(playerMP.getName()).setMovementHistory(mountnpc.getMovementHistory());
                            }
                        }
                    }
                }
            } else {
                EntityPlayerMP player = ServerProxy.getForgePlayer(sender.getName());
                for (Entity passenger : player.getPassengers()) {
                    if (passenger instanceof EntityNPCInterface) {
                        EntityNPCInterface npcPassenger = (EntityNPCInterface) passenger;
                        DataHolder.inst().getPlayer(npcPassenger.getName()).setMovementHistory(runner.getMovementHistory());
                        if (sender.getCommandSenderEntity() != null) {
                            BlockPos senderBlock = sender.getCommandSenderEntity().getPosition();
                            npcPassenger.wrappedNPC.setHome(senderBlock.getX(), senderBlock.getY(), senderBlock.getZ());
                        }
                    } else if (passenger instanceof EntityPlayerMP) {
                        EntityPlayerMP playerMP = (EntityPlayerMP) passenger;
                        DataHolder.inst().getPlayer(playerMP.getName()).setMovementHistory(runner.getMovementHistory());
                    }
                }
            }



/* PRESS NEXT */
            if (DataHolder.inst().getCombatForPlayer(sub.getName()) != null && !jump) {
                if (DataHolder.inst().getCombatForPlayer(sub.getName()).getReactionList() != null) {
                    if (!antiforced && (sub.getMovementHistory() != MovementHistory.FREELY || forced)) { //если конец движения форсирован, то конец хода. если антифорсирован (надо для нпц), то нет. Жесть
                         //TODO протестировать
                        if(charge) {
                            runner.performCommand("/" + NextExecutor.NAME + " charge");
                        } else {
                            //sub.reduceRecoil();
                            runner.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                        }
                        return;
                    }
                }
            }

/* PRESS ACTION IF FREELY (И если движение не завершено форсировано по кнопке конец хода) */
            if (sub.getMovementHistory() == MovementHistory.FREELY && !forced && !jump) {
                runner.performCommand(PerformAction.NAME);
            }

            if (jump) {
                if ((blocksRemaining < 2.0) && !riding && !sub.cantBeCharged() && !sub.isLying()) {
                    sub.makeFall();
                }
                runner.performCommand("/boomd jumped");
                runner.performCommand("/fired jumped");
                sub.setMovementHistory(MovementHistory.JUMP);
            }

           return;
        }
        try {
            blocksToCover = Integer.parseInt(args[0]);

            boolean jump = false;
            boolean freely = false;
            if (args.length > 1 && args[1].equals("jump")) {
                jump = true;
            }
            // SEND PACKET TO CLIENT
            //ClientGoMessage result = new ClientGoMessage(blocksToCover);
            //KMPacketHandler.INSTANCE.sendTo(result, getCommandSenderAsPlayer(sender));


            Message message = new Message("Блоков пройдено: " + 0. + "/" + blocksToCover + "\n", ChatColor.COMBAT);
            if (jump) {
                MessageComponent stop = new MessageComponent("[Конец прыжка]", ChatColor.BLUE);
                stop.setClickCommand("/" + NAME + " end antiforced jump");
                message.addComponent(stop);
            } else {
                MessageComponent action = new MessageComponent("[Действие] ", ChatColor.BLUE);
                action.setClickCommand("/" + PerformAction.NAME + " from_movement");
                MessageComponent stop = new MessageComponent("[Конец хода]", ChatColor.BLUE);
                stop.setClickCommand("/" + NAME + " end forced");



                if (sub.getMovementHistory() == MovementHistory.FREELY) freely = true;

                if (blocksToCover >= 5.50 || freely) {
                    MessageComponent charge = null;
                    if (sub.getMovementHistory() == MovementHistory.NOW) {
                        if (sub.getChargeHistory() == ChargeHistory.NONE) {
                            charge = new MessageComponent("[Начать натиск] ", ChatColor.GRAY);
                            charge.setHoverText("Нельзя начинать натиск почти не сдвинувшись с места.", TextFormatting.RED);
                            //charge.setClickCommand("/" + GoExecutor.NAME + " end forced charge");
                        } else {
//                            action.setColor(ChatColor.GRAY);
//                            action.setHoverText("Вы не получите натиск, пока не пробежите хотя бы несколько блоков.", TextFormatting.GRAY);
//                            action.setClickCommand("/" + PerformAction.NAME + " from_movement cancel_charge");
                            charge = new MessageComponent("[Продолжить натиск] ", ChatColor.GRAY);
                            charge.setHoverText("Нельзя продолжить натиск почти не сдвинувшись с места.", TextFormatting.RED);
                            //charge.setClickCommand("/" + GoExecutor.NAME + " end forced charge");
                        }
//                        charge.setHoverText("Требует 6 блоков. Закончит ваш ход без траты разбега.\n" +
//                                "На следующий ход вы сможете, после бега,\n" +
//                                "атаковать оружием ближнего боя с бонусом.", TextFormatting.DARK_GREEN);
                    }
                    message.addComponent(action);
                    if (charge != null) message.addComponent(charge);
                }
                message.addComponent(stop);
            }
            //TextComponentString panel = MessageGenerator.generate(message);
            //ClientProxy.addClickContainer("MovementCounter", panel);
            Helpers.sendClickContainerToClient(message, "MovementCounter", getCommandSenderAsPlayer(sender));

            if(sender.getCommandSenderEntity() != null && !(sender.getCommandSenderEntity().getRidingEntity() instanceof EntityNPCInterface)) MovementTracker.teleportToCenter(sender.getName());
            MovementTracker.addSuspect(sender.getName());
            MovementTracker.attachMovementBadgeToServer(sender.getName(), blocksToCover, freely);
            System.out.println("Added suspect and badge on " + FMLCommonHandler.instance().getSide());

            //DataHolder.getInstance().getPlayer(sender.getName()).performCommand("/clientgo " + blocksToCover);

        } catch (Exception e) {
            e.printStackTrace();
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
        }
    }














    public static String executePointerCalc(Player executor, String... args) {
        int blocksNumber = -1;
        if (args.length > 0) {
            try {
                blocksNumber = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                return ChatColor.RED + e.toString();
            }
        }
        //PointerCalc.PointToPoint(ForgeCore.getBukkitPlayer(executor.getName()), blocksNumber);
        return null;
    }

    public static String executeMovement(Player executor, SkillDiceMessage skillDiceMessage, String moveType, Player originalExecutor) {
        return "";
        /*
        int startingLevel = skillDiceMessage.getDice().getResult();

        if ((moveType.equals("передвижение") || moveType.equals("быстрый бег") || moveType.equals("супербег")) &&
                (executor.getMovementHistory() != MovementHistory.LAST_TIME) && executor.getMovementHistory() != MovementHistory.NOW) {
            executor.sendMessage(ChatColor.RED + "Нельзя бежать без разбега! Используйте % разбег.");
            return null;
        }

        // FREE MOVEMENT
        String freeMovement = "";
        if (moveType.equals("сп") || moveType.equals("свободное передвижение")) {
            executor.setMovementHistory(MovementHistory.FREELY);
            startingLevel = skillDiceMessage.getDice().getBase();
            if (startingLevel < 1) startingLevel = 1;
            freeMovement = " " + skillDiceMessage.getDice().getBaseAsString() + skillDiceMessage.getDice().getModAsString();
        }

        // FAST CREATURES
        if (executor.getMovementTrait() == MovementTrait.FAST && (moveType.equals("разбег") || moveType.equals("разгон"))) {
            moveType = "передвижение";
        } else if (executor.getMovementTrait() == MovementTrait.SUPERFAST && (moveType.equals("разбег") || moveType.equals("разгон"))) {
            moveType = "быстрый бег";
        }

        // CONNECT TO PointerCalc
        Double modifier = MovementHelper.types.get(moveType);
        int blocksNumber = DataHolder.inst().getMovementHelper().getNumberOfBlocks(startingLevel, modifier);
        if (DataHolder.inst().getCombatForPlayer(executor.getName()) != null && blocksNumber > 0) {
            if (executor instanceof NPC) {
        //        PointerCalc.PointToPoint(ForgeCore.getBukkitPlayer(originalExecutor.getName()), blocksNumber);
                executor.setRemainingBlocks(originalExecutor.getRemainingBlocks());
                originalExecutor.setWalker(executor);
            } else {
        //        PointerCalc.PointToPoint(ForgeCore.getBukkitPlayer(executor.getName()), blocksNumber);
            }
        }

        // FALLING
        if (blocksNumber == -1 || startingLevel == -4) {
            executor.sendMessage(new Message("Ты упал и потерял разбег!", COMBAT));
            executor.setMovementHistory(MovementHistory.NONE);
            ServerProxy.informMasters(new Message(DeprecatedAttack.gmNotice + executor.getName() + " упал и потерял разбег!"), executor);
            return null;
        }
        if (startingLevel == -3) {
            executor.sendMessage(new Message("Ты потерял разбег!", COMBAT));
            ServerProxy.informMasters(new Message(DeprecatedAttack.gmNotice + executor.getName() + " потерял разбег!"), executor);
            executor.setMovementHistory(MovementHistory.NONE);
        }

        // Y R U RUNNING?
        if (moveType.equals("разбег") || moveType.equals("разгон") || moveType.equals("передвижение") ||
                moveType.equals("быстрый бег") || moveType.equals("супербег")) {
            if (blocksNumber >= 1 && startingLevel >= -2) {
                executor.setMovementHistory(MovementHistory.NOW);
            }
        }


        // SEND INFO
        if (moveType.equals("передвижение")) {
            moveType = "бег";
        }
        if (moveType.equals("сп")) {
            moveType = "свободное передвижение";
        }
        if (!moveType.isEmpty()) {
            moveType = " (" + moveType + freeMovement + ")";
        }

        String info = DeprecatedAttack.gmNotice + executor.getName() + " может пройти " + blocksNumber + " " +
                Helpers.getPlural(blocksNumber, "блок", "блока", "блоков") + moveType + ".";
        executor.sendMessage(COMBAT + "Вы можете пройти " + blocksNumber + " " +
                Helpers.getPlural(blocksNumber, "блок", "блока", "блоков") + moveType + ".");
        ServerProxy.informMasters(info, executor);
        DiscordBridge.sendMessage(info);
        return "lorem";

         */

    }

}
