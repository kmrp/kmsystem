package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEffect;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.ExpireDate;
import ru.konungstvo.player.Player;

import javax.xml.crypto.Data;

public class ScriptExec extends CommandBase {


    @Override
    public String getName() {
        return "scrnpc";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
//
//        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 2) {
//            player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
            return;
        }

        String name = sender.getName();
        System.out.println(name);
        if (!name.contains("CustomNPC")) return;

        if (args[0].equals("stimulator")) {
            Player player = DataHolder.inst().getPlayer(args[1]);
            Player sub = player;
            if (player.getSubordinate() != null) sub = player.getSubordinate();
            EntityPlayer ep = ServerProxy.getForgePlayer(player.getName());
            ItemStack stimulator = ep.getHeldItemMainhand();
            if (!stimulator.isEmpty() && stimulator.hasTagCompound()) {
                if (stimulator.getTagCompound() != null && stimulator.getTagCompound().hasKey("useable") && stimulator.getTagCompound().getCompoundTag("useable").getString("type").equals("stimulator")) {
                    NBTTagCompound stim = stimulator.getTagCompound().getCompoundTag("useable");
                    ExpireDate expireDate = new ExpireDate(stimulator);
                    if (expireDate.isExpired()) {
                        player.sendMessage("§4Срок годности истёк.");
                        return;
                    }
                    if (DataHolder.inst().getCombatForPlayer(sub.getName()) == null) {
                        player.sendMessage("§4Нет смысла использовать вне боя (сожгите если очень надо).");
                        return;
                    }
                    if (sub.hasStatusEffect(StatusType.MEDS) || sub.hasStatusEffect(StatusType.MEDS2)) {
                        player.sendMessage("§4Уже под действием стимулятора. Применение приведет к летальному исходу.");
                        return;
                    }
                    if (sub.hasStatusEffect(StatusType.HANGOVER)) {
                        player.sendMessage("§4Вы еще не отошли от прошлого стимулятора. Применение приведет к летальному исходу.");
                        return;
                    }
                    boolean justActivated = (sub.getCombatState() == CombatState.SHOULD_ACT);
                    int turns = stim.getInteger("turns");
                    if (turns == 0) {
                        turns = 1;
                        justActivated = false;
                    }
                    if (stim.hasKey("trait")) {
                        sub.addStatusEffect(stim.getString("trait"), StatusEnd.TURN_END, turns, StatusType.MEDS, stim.getString("trait"), -666,
                                new StatusEffect(StatusType.HANGOVER.toString(), StatusEnd.TURN_END, stim.getInteger("withdrawal"), StatusType.HANGOVER, "", stim.getInteger("debuff"), null, false), justActivated);

                    }
                    if (stim.hasKey("skills")) {
                        sub.addStatusEffect("Стимулятор навыков", StatusEnd.TURN_END, turns, StatusType.MEDS2, stim.getString("skills"), stim.getInteger("buff"),
                                (stim.hasKey("trait") ? null : new StatusEffect(StatusType.HANGOVER.toString(), StatusEnd.TURN_END, stim.getInteger("withdrawal"), StatusType.HANGOVER, "", stim.getInteger("debuff"), null, false)), justActivated);
                    }
                    if (stimulator.getTagCompound().hasKey("msgOnConsume")) {
                        sender.sendMessage(new TextComponentString(stimulator.getTagCompound().getString("[" + stimulator.getDisplayName() + "&f] " + "msgOnConsume")));
                    }
                    if (stimulator.getTagCompound().hasKey("notifyGms")) {
                        String resultStr = " x:" + sender.getPosition().getX() + " y:" + sender.getPosition().getY() + " z:" + sender.getPosition().getZ() + " употребил " + stimulator.getDisplayName() + ": " + stimulator.getTagCompound().getString("notifyGms");
                        String gamemsg = "§2[Discord] [" + sub.getName()  + "] §6[@GameMaster " + resultStr + "§6]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
                        ServerProxy.sendToAllMasters(new Message(gamemsg));
                        String discmsg = "[Discord] [" + sub.getName()  + "] [" + resultStr + "]";
                        DiscordBridge.sendMessageToMasters(discmsg);
                    }
                    stimulator.setCount(stimulator.getCount() - 1);
                    ServerProxy.sendMessageFromAndInformMasters(sub, new Message("§a" + sub.getName() + " §2использует стимулятор!"));
                    if (stim.hasKey("lore")) {
                        player.sendMessage(new Message(stim.getString("lore"), ChatColor.DARK_GREEN));
                    }
                    DiscordBridge.sendMessage(sub.getName() + " использует стимулятор: " + (stim.hasKey("trait") ? stim.getString("trait") : "") + (stim.hasKey("skills") ? stim.getString("skills") + " Бафф: " + stim.getInteger("buff") : "") + " Ходы: " + stim.getInteger("turns")  + " Отходняк: " + stim.getInteger("withdrawal") + " Штраф: " + stim.getInteger("debuff"));
                    player.performCommand("/geteffects");
                }
            }
        }

        else if (args[0].equals("food")) {
            EntityPlayerMP player = ServerProxy.getForgePlayer(args[1]);
            int amount = Integer.parseInt(args[2]);
            player.getFoodStats().setFoodLevel(Math.min(20, player.getFoodStats().getFoodLevel() + (amount)));
            player.sendMessage(new TextComponentString("§2Восстановлено " + amount + " сытости!"));
            DataHolder.inst().updateHungerAndThirst(player.getName());
        }

        else if (args[0].equals("ointment")) {
            Player player = DataHolder.inst().getPlayer(args[1]);
            Player sub = player;
            if (player.getSubordinate() != null) sub = player.getSubordinate();
            Player target = sub;
            if (args.length > 2) target = DataHolder.inst().getPlayer(args[2]);
            EntityPlayer ep = ServerProxy.getForgePlayer(player.getName());
            ItemStack stimulator = ep.getHeldItemMainhand();
            if (!stimulator.isEmpty() && stimulator.hasTagCompound()) {
                if (stimulator.getTagCompound() != null && stimulator.getTagCompound().hasKey("useable") && stimulator.getTagCompound().getCompoundTag("useable").getString("type").equals("ointment")) {
                    NBTTagCompound stim = stimulator.getTagCompound().getCompoundTag("useable");
                    ExpireDate expireDate = new ExpireDate(stimulator);
                    if (expireDate.isExpired()) {
                        player.sendMessage("§4Срок годности истёк.");
                        return;
                    }

                    if (!DataHolder.inst().getModifiers().hasStunningWounds(target.getName())) {
                        player.sendMessage("§4У цели нет ран оглушением.");
                        return;
                    }

                    if (DataHolder.inst().getCombatForPlayer(target.getName()) != null || DataHolder.inst().getCombatForPlayer(sub.getName()) != null) {
                        player.sendMessage("§4Нельзя использовать в бою.");
                        return;
                    }

                    SkillDiceMessage sdm = new SkillDiceMessage(sub.getName(), "% первая помощь");
                    sdm.build();
                    ServerProxy.sendMessageFromAndInformMasters(sub, sdm);

                    int succ = DataHolder.inst().getModifiers().cureStunningWoundsWithDice(target.getName(), sdm.getDice().getResult());

                    Message res = new Message();
                    if (succ == 0) {
                        res.addComponent(new MessageComponent(sub.getName() + " неуспешно лечит " + target.getName() + " с помощью " + stimulator.getDisplayName(), ChatColor.DARK_RED));
                    }
                    if (succ == 1) {
                        res.addComponent(new MessageComponent(sub.getName() + " частично лечит " + target.getName() + " с помощью " + stimulator.getDisplayName(), ChatColor.YELLOW));
                    }
                    if (succ == 2) {
                        res.addComponent(new MessageComponent(sub.getName() + " полностью лечит " + target.getName() + " с помощью " + stimulator.getDisplayName(), ChatColor.DARK_GREEN));
                    }
                    ServerProxy.sendMessageFromAndInformMasters(sub, res);
                    stimulator.setCount(stimulator.getCount() - 1);
                    if (stimulator.getTagCompound().hasKey("msgOnConsume")) {
                        sender.sendMessage(new TextComponentString(stimulator.getTagCompound().getString("[" + stimulator.getDisplayName() + "&f] " + "msgOnConsume")));
                    }
                    if (stimulator.getTagCompound().hasKey("notifyGms")) {
                        String resultStr = " x:" + sender.getPosition().getX() + " y:" + sender.getPosition().getY() + " z:" + sender.getPosition().getZ() + " употребил " + stimulator.getDisplayName() + ": " + stimulator.getTagCompound().getString("notifyGms");
                        String gamemsg = "§2[Discord] [" + sub.getName()  + "] §6[@GameMaster " + resultStr + "§6]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
                        ServerProxy.sendToAllMasters(new Message(gamemsg));
                        String discmsg = "[Discord] [" + sub.getName()  + "] [" + resultStr + "]";
                        DiscordBridge.sendMessageToMasters(discmsg);
                    }
                }
            }
        } else if (args[0].equals("model")) {
            System.out.println("/" + "custommodel select " + args[2] + " " + args[1]);
            MinecraftServer cm = FMLCommonHandler.instance().getMinecraftServerInstance();
            if (args[2].equals("remove")) cm.getCommandManager().executeCommand(cm, "/" + "custommodel clear " + args[1]);
            else cm.getCommandManager().executeCommand(cm, "/" + "custommodel select " + args[2] + " " + args[1]);
        }
    }
}

