package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NextExecutor extends CommandBase {
    public static final String NAME = "endturn";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList<String>();
        result.add("next");
        return result;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + PurgeModifiersExecutor.NAME);
        String acted = "";
        if (args.length > 0) {
            Player pl = DataHolder.inst().getPlayer(sender.getName());
            switch (args[0]) {
                case "combat":
                    acted = " combat";
                    break;
                case "resetrecoil":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.reduceRecoil();
                    }
                    break;
                case "resetmovement":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.reduceRecoil();
                    }
                    acted = " resetmovement";
                    break;
                case "extinguish":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().makeFall(); //тушение в queueexecutor
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.makeFall(); //тушение в queueexecutor
                        pl.reduceRecoil();
                    }
                    acted = " extinguish";
                    break;
                case "extinguishtarget":
                    //тушение в queueexecutor
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.reduceRecoil();
                    }
                    acted = " extinguishtarget " + args[1];
                    break;
                case "getuptarget":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.reduceRecoil();
                    }
                    acted = " getuptarget " + args[1];
                    break;
                case "bandaidtarget":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.reduceRecoil();
                    }
                    acted = " bandaidtarget " + args[1];
                    break;
                case "cpraidtarget":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.reduceRecoil();
                    }
                    acted = " cpraidtarget " + args[1];
                    break;
                case "charge":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setChargeHistory(ChargeHistory.CHARGE);
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.setChargeHistory(ChargeHistory.CHARGE);
                        pl.reduceRecoil();
                    }
                    acted = " charge";
                    break;
                case "concentrated":
                    acted = " concentrated";
                    if (args.length > 1) {
                        String skill = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                        acted = acted + " " + skill;
                    }
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.reduceRecoil();
                    }
                    break;
                case "magic":
                    acted = " magic";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.reduceRecoil();
                    }
                    break;
                case "defensivestance":
                    acted = " defensivestance";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                        //pl.getSubordinate().setDefensiveStance(true);
                    } else {
                        pl.reduceRecoil();
                        //pl.setDefensiveStance(true);
                    }
                    break;
                case "bipod":
                    acted = " bipod";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                        //pl.getSubordinate().setDefensiveStance(true);
                    } else {
                        pl.reduceRecoil();
                        //pl.setDefensiveStance(true);
                    }
                    break;
                case "aim":
                    acted = " aim";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                        //pl.getSubordinate().setDefensiveStance(true);
                    } else {
                        pl.reduceRecoil();
                        //pl.setDefensiveStance(true);
                    }
                    break;
                case "station":
                    acted = " station";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                        //pl.getSubordinate().setDefensiveStance(true);
                    } else {
                        pl.reduceRecoil();
                        //pl.setDefensiveStance(true);
                    }
                    break;
                default:
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().reduceRecoil();
                    } else {
                        pl.reduceRecoil();
                    }
            }
        }

        ClickContainer.deleteContainer("ToolPanel", getCommandSenderAsPlayer(sender));
        ClickContainer.deleteContainer("PerformAction", getCommandSenderAsPlayer(sender));
        DataHolder.inst().getPlayer(sender.getName()).performCommand("/queue next" + acted);
    }

}
