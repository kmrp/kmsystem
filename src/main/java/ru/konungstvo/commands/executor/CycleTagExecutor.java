package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class CycleTagExecutor extends CommandBase {
    public static final String NAME = "CycleTag";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if(player.getSubordinate() != null) player = player.getSubordinate();
        String toRoot = "";
        String oldContainer = "";
        if (args.length > 0) toRoot = args[0];
        if (args.length > 1) oldContainer = args[1];
        EntityLivingBase elb = DataHolder.inst().getNpcEntity(player.getName());

        try {
            ItemStack is = elb.getHeldItemMainhand();
            WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(is);
            weaponTagsHandler.changeDefaultWeaponToNext();
            TextComponentString ok = new TextComponentString("Тег оружия переключен на " + weaponTagsHandler.getDefaultWeaponName());
            sender.sendMessage(ok);
            if(!toRoot.isEmpty()) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + toRoot + " " + oldContainer);
            return;
        } catch (Exception ignored) {
            System.out.println(ignored);
            return;
        }

    }
}

