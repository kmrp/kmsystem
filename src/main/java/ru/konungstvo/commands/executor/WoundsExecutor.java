package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class WoundsExecutor extends CommandBase {
    public static String execute(Player executor, String... args) {
        if (args.length == 0 || !executor.hasPermission(Permission.GM)) {
            DataHolder.inst().updatePlayerWounds(executor.getName());
            return ChatColor.COMBAT + "Твоя пирамидка ран:\n" + executor.getWoundPyramid().toNiceString(executor.getName());
        }
        Player player = DataHolder.inst().getPlayer(args[0]);
        DataHolder.inst().updatePlayerWounds(player.getName());
        return ChatColor.COMBAT + "Пирамидка ран " + player.getName() + ":\n" + player.getWoundPyramid().toNiceString(player.getName());
    }

    @Override
    public String getName() {
        return "wounds";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());

        int bloodloss = 0;
        int mentalDebuff = 0;
        if (args.length == 0 || !player.hasPermission(Permission.GM)) {
            DataHolder.inst().updatePlayerWounds(player.getName());
            bloodloss = DataHolder.inst().getModifiers().getBloodloss(player.getName());
            mentalDebuff = DataHolder.inst().getModifiers().getMentalDebuff(player.getName());
            Message answer = new Message("Твоя пирамидка ран:\n", ChatColor.COMBAT);
            answer.addComponent(player.getWoundPyramid().toNiceMessage());
            if (bloodloss >0) answer.addComponent(new Message ("Твой уровень кровопотери: " + bloodloss, ChatColor.RED));
            if (mentalDebuff >0) answer.addComponent(new Message ("Дополнительный штраф от ментальных ран: " + mentalDebuff, ChatColor.GLOBAL));
            player.sendMessage(answer);
            return;
        }
        Player second = DataHolder.inst().getPlayer(args[0]);

        if (args.length == 3 && args[2].equals("reset")) {
            DataHolder.inst().resetWounds(second);
        }

        DataHolder.inst().updatePlayerWounds(second.getName());
        bloodloss = DataHolder.inst().getModifiers().getBloodloss(second.getName());
        mentalDebuff = DataHolder.inst().getModifiers().getMentalDebuff(second.getName());
        Message message = new Message("Пирамидка ран " + second.getName() + ":\n", ChatColor.COMBAT);
        message.addComponent(second.getWoundPyramid().toNiceMessage());
        if (bloodloss >0) message.addComponent(new Message ("Твой уровень кровопотери: " + bloodloss, ChatColor.RED));
        if (mentalDebuff >0) message.addComponent(new Message ("Дополнительный штраф от ментальных ран: " + mentalDebuff, ChatColor.GLOBAL));
        player.sendMessage(message);

    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
}
