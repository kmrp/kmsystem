package ru.konungstvo.control.network;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.util.io.netty.buffer.ByteBufUtil;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class ClickContainerMessage implements IMessage {
    public String name;
    public boolean delete;
    public String content;

    public ClickContainerMessage() {}

    public ClickContainerMessage(String name, boolean delete) {
        this.name = name;
        this.delete = true;
        this.content = "";
    }


    public ClickContainerMessage(String name, String content) {
        this.name = name;
        this.delete = false;
        this.content = content;
    }


    @Override
    public void fromBytes(ByteBuf buf) {
        name = ByteBufUtils.readUTF8String(buf);
        delete = buf.readBoolean();
        content = ByteBufUtils.readUTF8String(buf);
        System.out.println("name: " + name);

    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, name);
        buf.writeBoolean(delete);
        ByteBufUtils.writeUTF8String(buf, content);


    }
}
