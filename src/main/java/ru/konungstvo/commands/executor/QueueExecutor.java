package ru.konungstvo.commands.executor;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.network.MessageSoundReg;
import net.minecraft.command.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.chat.range.RangeFactory;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.reactions.Queue;
import ru.konungstvo.combat.reactions.Reaction;
import ru.konungstvo.combat.reactions.ReactionList;
import ru.konungstvo.combat.reactions.ViewType;
import ru.konungstvo.commands.helpercommands.player.turn.ToolPanel;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.control.network.ThirdPersonClientMessage;
import ru.konungstvo.exceptions.CombatException;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.exceptions.RuleException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handles everything to do with queues, both from GM's and player's point of view.
 */
public class QueueExecutor extends CommandBase {
    private static final Pattern rPat = Pattern.compile("([\\p{L}0-9_-]{1,32})+\\s?(ужасно|плохо|посредственно|нормально|хорошо|отлично|превосходно|легендарно)?\\s?([+-]?[0-9]{1})?");
    private static Logger logger = new Logger("QUEUE");
    public static final String NAME = "queue";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public List<String> getAliases() {
        List<String> res = new ArrayList();
        res.add("q");
        res.add("react");
        res.add("r");
        return res;
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        try {
            Message answer = new Message("");
            Player player = DataHolder.inst().getPlayer(iCommandSender.getName());
            if (!ServerProxy.hasPermission(iCommandSender.getName(), "km.gm") && strings.length == 0) {
                switch (player.getCombatState()) {
                    case SHOULD_ACT:
                        answer = new Message("Сейчас ваш ход!", ChatColor.COMBAT);
                        break;
                    case ACTED:
                        answer = new Message("Вы уже ходили!", ChatColor.COMBAT);
                        break;
                    case SHOULD_WAIT:
                        answer = new Message("Ваш ход ещё не наступил, подождите!", ChatColor.COMBAT);
                        break;
                }
//                ServerProxy.sendMessage(player, answer);
                player.sendMessage(answer);
            } else if (!ServerProxy.hasPermission(iCommandSender.getName(), "km.gm")) {
                if(strings.length > 1) {
                    executeAsFighter(player, strings[0], Arrays.copyOfRange(strings, 1, strings.length));
                } else {
                    executeAsFighter(player, strings[0]);
                }

            } else {
                answer = execute(player, strings);
                if (answer != null) {
                    TextComponentString res = MessageGenerator.generate(answer);
                    iCommandSender.sendMessage(res);
                }
            }
        } catch (DataException e) {
            System.out.println(e.toString());
            e.printStackTrace();
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            iCommandSender.sendMessage(error);
        }
    }

    // for GMs
    public static Message execute(Player executor, String... args) {
        if (args.length > 0 && !args[0].equals("next")) {
            if (!executor.hasPermission(Permission.GM)) {
                throw new PermissionException("1", "Недостаточно прав!");
            }
        }

        DataHolder dh = DataHolder.inst();
        Combat combat = dh.getCombat(executor.getAttachedCombatID());
        Message info = new Message();

        // if there is no combat, throw error
        // however, "add" command will create a combat
        if (combat == null) {
            if (args.length == 0 || !(args[0].equals("add"))) {
                if (executor.getSubordinate() != null && ((NPC) executor.getSubordinate()).getPrefferedMaster() == executor) {
                    combat = DataHolder.inst().getCombat(DataHolder.inst().getCombatForPlayer(executor.getSubordinate().getName()).getId());
                }
                if (combat == null) throw new DataException("QueueExecutor", "У вас нет привязанного боя!");
            }
            if (combat == null) {
                combat = dh.createCombat(executor.getName());
                executor.setAttachedCombatID(combat.getId());
                executor.sendMessage(new Message("Бой не найден, создаём новый под именем " + combat.getName(), ChatColor.DIM));
            }
        }

        // again, if there is no reactionList (queue) present, create it
        ReactionList reactionList = combat.getReactionList();
        if (reactionList == null) {
            executor.sendMessage(new Message("Очередь не найдена, создаётся новая...", ChatColor.DIM));
            reactionList = new ReactionList();
            combat.setReactionList(reactionList);
        }

        Queue queue = combat.getReactionList().getQueue();

        // if no arguments given, output the whole queue
        if (args.length == 0) return reactionList.toMessage();

        // check if we are using a range. Can be used with "go" and "turns" commands
        Range range = RangeFactory.build(args[0]);
        range = Helpers.convertToDieRange(range);
        args[0] = args[0].substring(range.getNumberOfSymbolsToStrip());

        switch (args[0]) {
            case "help":
                return new Message("Тут будет хелп. Пока что на сайте смотрите.", ChatColor.DIM);
            case "add":
                String argsStr = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                Matcher rMat = rPat.matcher(argsStr);
                String level;
                String mod;
                String name;

                searching:
                while (rMat.find()) {
                    name = rMat.group(1);
                    level = rMat.group(2);
                    mod = rMat.group(3);
                    for (Reaction reaction : reactionList.getReactions()) {
                        if (reaction.getPlayerName().equals(name)) {
                            executor.sendMessage(new Message(name + " уже в списке реакций, пропускаем...", ChatColor.DIM));
                            continue searching;
                        }
                    }
                    Player player = reactionList.addReaction(new Reaction(name, level, mod));
                    player.setCombatState(CombatState.SHOULD_WAIT);
                    player.setMovementHistory(MovementHistory.NONE);
                    player.setChargeHistory(ChargeHistory.NONE);
                    player.setRemainingBlocks(-1);
                    player.riposted = null;
                    player.clearAllOpportunities();
                    player.clearAllDefends();
                    player.clearEffects();
                    player.grenadesUsed = 0;
                    player.setRecoil(0);
                    if (!dh.isNpc(player.getName()) && !player.hasPermission(Permission.GM)) {
                        if (combat.getThirdPersonBlocked()) {
                            KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(-666), ServerProxy.getForgePlayer(player.getName()));
                        } else {
                            KMPacketHandler.INSTANCE.sendTo(new ThirdPersonClientMessage(0), ServerProxy.getForgePlayer(player.getName()));
                        }
                    }
                    combat.addFighter(player.getName());

                }
                return new Message("Реакции были добавлены!", ChatColor.COMBAT);
            case "edit":
                argsStr = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                rMat = rPat.matcher(argsStr);

                while (rMat.find()) {
                    name = rMat.group(1);
                    level = rMat.group(2);
                    if (level == null) {
                        for (Reaction reaction : reactionList.getReactions()) {
                            if (reaction.getPlayerName().equals(name)) {
                                level = reaction.getSkillLevel();
                            }
                        }
                    }
                    mod = rMat.group(3);
                    if (mod == null) {
                        for (Reaction reaction : reactionList.getReactions()) {
                            if (reaction.getPlayerName().equals(name)) {
                                mod = reaction.getMod();
                            }
                        }
                    }
                    reactionList.addReaction(new Reaction(name, level, mod));
                }
                return new Message("Указанные реакции были изменены!", ChatColor.COMBAT);
            case "remove":
            case "delete":
                for (int i = 1; i < args.length; i++) {
                    if (DataHolder.inst().getPlayer(args[i]).getCombatState().equals(CombatState.SHOULD_ACT)) {
                        if (queue.getNext().getName().equals(args[i])) executor.performCommand("/queue next");
                        if (queue.getNext().getName().equals(args[i]))
                            executor.performCommand("/queue next"); //Да, серьезно. Ведь он может быть первым в следующем раунде!!! Аааааааааааааааа
                    }
                    reactionList.removeReaction(args[i]);
                    DataHolder.inst().getPlayer(args[i]).removeCprTo();
                    queue.removePlayer(args[1]);
                }
                return new Message("Указанные реакции были удалены!", ChatColor.COMBAT);
            case "end":
                return new Message("Error: Используйте /combat end, чтобы удалить весь бой вместе с очередью.", ChatColor.RED);
                /*
                DataHolder.getInstance().removeCombat(combat.getId());
                executor.setAttachedCombatID(-1);
                return ChatColor.COMBAT + "Бой и очередь были удалены.";
                */
            case "num":
                Message roundNum = new Message("Номер раунда: " + reactionList.getRoundNumber(), ChatColor.COMBAT);
//                executor.sendMessage(roundStart);
                return roundNum;
            case "go":
                reactionList.setRoundNumber(reactionList.getRoundNumber() + 1);
                queue.setRoundNumber(reactionList.getRoundNumber());
                ArrayList<Player> shifted = new ArrayList<>();
                for (Player p : combat.getFighters()) {
                    if (p.hasShifted()) shifted.add(p);
                    p.processStatusEffects(StatusEnd.ROUND_START);
                }

                queue.reset();

                for (Player p : combat.getMustNotActNextRound()) {
                    System.out.println(p.getName() + " AAAAAAAAAAAAAAAH" + (p.getAttachedCombatID() == combat.getId()));
                    p.setCombatState(CombatState.ACTED);
                }

                combat.clearMustNotActNextRound();

                String lethalcompany = combat.proceedLethalCompanyAndToString();
                Message lethalCompanyMessage = null;

                Message roundStart = new Message("Начался раунд " + reactionList.getRoundNumber() + "!", ChatColor.COMBAT);
                if (!lethalcompany.isEmpty()) {
                    MessageComponent meme = new MessageComponent(" [Броски выносливости] ", ChatColor.DARK_GRAY);
                    meme.setUnderlined(true);
                    meme.setHoverText(lethalcompany, TextFormatting.DARK_GRAY);
                    roundStart.addComponent(meme);
                }

                Player combatmaster = DataHolder.inst().getMasterForCombat(combat.getId());
                if (combatmaster != null) {
                    ServerProxy.sendMessageFromAndForAllPlayersInCombatAndInformMastersAndAllAround(combat, combatmaster.getDefaultRange(), roundStart);
                } else {
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(roundStart);
                    }
                    ServerProxy.informMasters(roundStart, executor, false);
                }


                // Create dice messages for every fighter in queue
                for (Reaction reaction : reactionList.getReactions()) {
                    Player player = dh.getPlayer(reaction.getPlayerName());
                    SkillDiceMessage message = new SkillDiceMessage(player.getName(), "% реакция");
                    if (shifted.contains(player)) message.getDice().addMod(new Modificator(-1, ModificatorType.SHIFTED));
//                    int woundsMod = 0;
//                    int armorMod = 0;
//                    int shieldMod = 0;
//                    PercentDice percentDice = new PercentDice();
                    if (player != null) {
//
//                        // FREEZE ALL PLAYERS
                        player.freeze();
//
                        player.setPreviousDefenses(0);
                        player.getPercentDice().setPreviousDefenses(0);
                        player.setHasAttacked(false);
                        player.riposted = null;
//                        woundsMod = player.getWoundsMod();
//                        armorMod = player.getArmorMod();
//                        shieldMod = player.getShieldMod();
//                        player.getPercentDice().setSkill(player.getSkill("реакция").getPercent());
//                        percentDice = player.getPercentDice();
                    }
//                    SkillDice skillDice = new SkillDice(
//                            reaction.getSkillLevel(),
//                            "реакция",
//                            Integer.parseInt(reaction.getMod()),
//                            woundsMod,
//                            armorMod,
//                            0,
//                            shieldMod,
//                            percentDice
//                    );
//                    SkillDiceMessage message = new SkillDiceMessage(
//                            reaction.getPlayerName(),
//                            "123",
//                            range,
//                            skillDice
//                    );
                    message.build();
                    queue.addResult(message);
                }

                // Sort queue and get first one to make turn
                reactionList.getQueue().sort();
                boolean test = false;
                for (SkillDiceMessage result : queue.getResults()) {
                    if (dh.getPlayer(result.getPlayerName()).getCombatState() != (CombatState.ACTED)) {
                        dh.getPlayer(result.getPlayerName()).setCombatState(CombatState.SHOULD_ACT);
                        test = true;
                        break;
                    }
                }
                if (!test) dh.getPlayer(queue.getResults().get(0).getPlayerName()).setCombatState(CombatState.SHOULD_ACT);

                //why is it even here? Just to suffer?
                if (reactionList.getQueue().getResults().isEmpty()) {
                    return queue.toMessage();
                }

                /*
                for (Player p : combat.getFighters()) {
                    if (DataHolder.inst().isNpc(p.getName())) continue;
                    MinecraftServer cm = FMLCommonHandler.instance().getMinecraftServerInstance();
                    cm.getCommandManager().executeCommand(cm,"/freezeplayer " + p.getName());
                }

                 */

                    switch (reactionList.getViewType()) {
                    case AUTO: //send to all members and GMs
                        if (combatmaster != null) {
                            ServerProxy.sendMessageFromAndForAllPlayersInCombatAndInformMastersAndAllAround(combat, combatmaster.getDefaultRange(), queue.toMessage());
                        } else {
                            for (Player p : combat.getFighters()) {
                                p.sendMessage(queue.toMessage());
                            }
                            ServerProxy.informMasters(queue.toMessage(), executor, false);
                        }
                        //   
                        //   
                        break;
                    case SIMPLE: //send to range
                        //executor.sendMessage(queue.getResultsAsMessage());
                        // 
                        if (DataHolder.inst().getMasterForCombat(combat.getId())  !=  null) ServerProxy.sendMessageFromAndForPlayersInCombat(DataHolder.inst().getMasterForCombat(combat.getId()), queue.toMessage(), combat, DataHolder.inst().getMasterForCombat(combat.getId()).getDefaultRange());
                        else {
                            for (Player p : combat.getFighters()) {
                                p.sendMessage(queue.toMessage());
                            }
                            ServerProxy.informMasters(queue.toMessage(), executor);
                        }
                        //ServerProxy.sendMessage(executor, range, queue.getResultsAsMessage());
                        break;
                    case SILENT: //send only to GM(s)
                        //ServerProxy.informMasters(queue.getResultsAsMessage(), executor);
                        ServerProxy.informMasters(queue.toMessage(), executor);
                        //  
                        //   
                        break;
                }


                if (combat.processExplosionsRoundStart()) {
                    combat.setWaitForBoom(true);
                    combat.processExplosions2();
                    return null;
                } else if (combat.revealExplosions(queue.getResultsAboveNormal())) {
                    combat.setWaitForBoom(true);
                    combat.processExplosions2();
                    return null;
                } else {
                    combat.setWaitForBoom(false);
                }

                info.addComponent(new MessageComponent(queue.getNext().getName() + " ходит первым.", ChatColor.COMBAT));

                
                ServerProxy.informMasters(info, executor);
                logger.info(info.toString());

                Player nextOne = queue.getNext();
                nextOne.processMovement();
                nextOne.clearOpportunities2();
                nextOne.clearDefends();
                nextOne.processStatusEffects(StatusEnd.TURN_START);
                if (!dh.isNpc(nextOne.getName())) {
                    try {
                        EntityLivingBase rider = ServerProxy.getForgePlayer(nextOne.getName());
                        if (rider != null && rider.getRidingEntity() != null) {
                            Entity mount = rider.getLowestRidingEntity();
                            Player mountPlayer = null;
                            if(mount instanceof EntityNPCInterface) {
                                EntityNPCInterface npc = (EntityNPCInterface) mount;
                                mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                                if (rider.getRidingEntity() == mount) {
                                    if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement(); // если в той же очереди, то не надо. Маунт походит (и потеряет уровень разгона) в свой ход
                                    nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                    nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                }
                            } else if (mount instanceof EntityPlayerMP) {
                                EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                                Player player = DataHolder.inst().getPlayer(playerMP.getName());
                                if (player.getMovementHistory() == MovementHistory.NOW) {
                                    nextOne.setMovementHistory(player.getMovementHistory());
                                }
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                    if (nextOne.hasStatusEffect(StatusType.STUNNED) && !nextOne.cantBeStunned()) {
                        info.purge();
                        info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextOne.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                        for (Player p : combat.getFighters()) {
                            p.sendMessage(info);
                        }
                        ServerProxy.informMasters(info, executor);
                        
                        logger.info(info.toString());
                        nextOne.performCommand("/" + NextExecutor.NAME);
                    } else {
                        nextOne.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                        if (nextOne.getSubordinate() != null) nextOne.removeSubordinate();
                        nextOne.performCommand("/" + ToolPanel.NAME);
                        GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 1F, 1F), ServerProxy.getForgePlayer(nextOne.getName()));
                    }
                } else {

                    try {
                        EntityLivingBase rider = (EntityLivingBase) DataHolder.inst().getNpcEntity(nextOne.getName());
                        if (rider != null && rider.getRidingEntity() != null) {
                            Entity mount = rider.getLowestRidingEntity();
                            Player mountPlayer = null;
                            if(mount instanceof EntityNPCInterface) {
                                EntityNPCInterface npc = (EntityNPCInterface) mount;
                                mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                                if (rider.getRidingEntity() == mount) {
                                    if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                                    nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                    nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                }
                            } else if (mount instanceof EntityPlayerMP) {
                                EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                                Player player = DataHolder.inst().getPlayer(playerMP.getName());
                                if (player.getMovementHistory() == MovementHistory.NOW) {
                                    nextOne.setMovementHistory(player.getMovementHistory());
                                }
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                    Player gm = dh.getMasterForNpcInCombat(nextOne);
                    if (gm == null) {
                        System.out.println("gm null wtf");
                    }
                    if (nextOne.hasStatusEffect(StatusType.STUNNED) && !nextOne.cantBeStunned()) {
                        info.purge();
                        info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextOne.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                        for (Player p : combat.getFighters()) {
                            p.sendMessage(info);
                        }
                        ServerProxy.informMasters(info, executor);
                        
                        logger.info(info.toString());
                        dh.getMasterForNpcInCombat(nextOne).performCommand("/" + NextExecutor.NAME);
                    } else {
                        gm.setSubordinate(nextOne);
                        gm.sendMessage(new Message(String.format("Ходит %s!", nextOne.getName()), ChatColor.BLUE));
                        gm.performCommand("/" + ToolPanel.NAME);
                        GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 0.5F, 1F), ServerProxy.getForgePlayer(gm.getName()));
                    }
                }
                dh.saveCombat(combat);
                return null;

            case "turns":
                if (args.length > 1 && (args[1].equals("-s") || args[1].equals("-silent"))) {
                    return reactionList.getQueue().toMessage();
                }
                if (reactionList.getQueue().getResults().isEmpty()) {
                    return reactionList.getQueue().toMessage();
                }
                executor.sendMessage(new Message("Очередь отправлена в рейндж " + range, ChatColor.DIM));
                ServerProxy.sendMessage(executor, range, reactionList.getQueue().toMessage());
                return null;

            case "show":
                return reactionList.toMessage();

            case "next":
                String previous = queue.getNext().getName();
                //combat.removeAttack(combat.getAttackByAttacker(previous));
                //queue.getNext().setRemainingBlocks(-1);
                /*
                if (queue.getNext().getMovementHistory() == MovementHistory.NOW) {
                    queue.getNext().setMovementHistory(MovementHistory.LAST_TIME);
                }
                else if (queue.getNext().getMovementHistory() == MovementHistory.LAST_TIME) {
                    queue.getNext().setMovementHistory(MovementHistory.NONE);
                }

                 */
                info.purge();
                boolean shifted2 = false;
                if (!combat.getWaitForBoom()) {
                    Player previousPlayer = queue.getNext();
                    shifted2 = previousPlayer.hasShifted();
                    DataHolder.inst().getModifiers().updateBloodlossAndStunning(previousPlayer, previous);
                    boolean burned = false;
                    if (previousPlayer.checkIfEndedTurnOnFire()) burned = true;
                    if (previousPlayer.hasStatusEffect(StatusType.BURNING) && !previousPlayer.cantBurn() && !previousPlayer.getWoundPyramid().onlyDeadlyLeft()) {
                        String wound = previousPlayer.inflictDamage(3, "ожог от горения");
                        previousPlayer.takeAwayDurFromAllArmor(1);
                        if (DataHolder.inst().isNpc(previousPlayer.getName()))
                            ((NPC) previousPlayer).setWoundsInDescription();
                        if (wound.equals("критическая рана") || wound.equals("смертельная рана")) burned = true;
                    }

                    queue.proceed();

                    info.purge();
                    if (args.length > 1 && !args[1].equals("combat")) {
                        previousPlayer.reduceRecoil();
                    }
                    if (burned) {
                        info.addComponent(new MessageComponent(previous + " заканчивает свой ход и падает обугленный.", ChatColor.DARK_RED));
                        queue.removePlayerBc(previous, "обуглен");
                    } else if (previousPlayer.getBloodloss() > 99) {
                        info.addComponent(new MessageComponent(previous + " заканчивает свой ход и теряет сознание от кровопотери.", ChatColor.DARK_RED));
                        queue.removePlayerBc(previous, "обескровлен");
                    } else if (args.length > 1 && args[1].equals("resetmovement")) {
                        info.addComponent(new MessageComponent(previous + " заканчивает свой ход небоевым действием.", ChatColor.COMBAT));
                    } else if (args.length > 1 && args[1].equals("extinguish")) {
                        info.addComponent(new MessageComponent(previous + " заканчивает свой ход катанием по земле и тушится.", ChatColor.COMBAT));
                        previousPlayer.removeStatusEffect(StatusType.BURNING);
                    } else if (args.length > 1 && args[1].equals("extinguishtarget")) {
                        info = new Message(previousPlayer.getName() + " заканчивает свой ход тушением " + args[1] + ".", ChatColor.COMBAT);
                        DataHolder.inst().getPlayer(args[2]).removeStatusEffect(StatusType.BURNING);
                    } else if (args.length > 1 && args[1].equals("getuptarget")) {
                        info = new Message(previousPlayer.getName() + " заканчивает свой ход подниманием " + args[2] + ".", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("bandaidtarget")) {
                        info = new Message(previousPlayer.getName() + " заканчивает свой ход первой помощью " + args[2] + ".", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("cpraidtarget")) {
                        info = new Message(previousPlayer.getName() + " заканчивает свой ход зажиманием ран " + args[2] + ".", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("charge")) {
                        info = new Message(previous + " заканчивает свой ход началом натиска.", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("concentrated")) {
                        String skill = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
                        info = new Message(previous + " заканчивает свой ход концентрацией на навыке " + skill + ".", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("magic")) {
                        info = new Message(previous + " заканчивает свой ход подготовкой способности.", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("defensivestance")) {
                        info = new Message(previous + " заканчивает свой ход переходом в глухую оборону.", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("aim")) {
                        info = new Message(previous + " заканчивает свой ход переходом в режим прицеливания.", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("bipod")) {
                        info = new Message(previous + " заканчивает свой ход установкой сошек.", ChatColor.COMBAT);
                    } else if (args.length > 1 && args[1].equals("station")) {
                        info = new Message(previous + " заканчивает свой ход установкой оружия на станок.", ChatColor.COMBAT);
                    } else {
                        info.addComponent(new MessageComponent(previous + " заканчивает свой ход.", ChatColor.COMBAT));
                    }

                    previousPlayer.riposted = null;

                    System.out.println("processing1 " + previousPlayer.getName());
                    previousPlayer.processStatusEffects(StatusEnd.TURN_END);

                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, executor);

                    logger.info(info.toString());

                    //System.out.println("next in q: " + queue.getNext().getName());

                    if (queue.getNext() == null) {
                        if (queue.isAutoRenewing()) {
                            QueueExecutor.execute(executor, "go");
                            return null;
                        }
                    }
                    //queue.getNext().setRemainingBlocks(-1);
                    switch (reactionList.getViewType()) {
                        case AUTO: //send to all members and GMs
                            Player combatmaster1 = DataHolder.inst().getMasterForCombat(combat.getId());
                            if (combatmaster1 != null) {
                                ServerProxy.sendMessageFromAndForAllPlayersInCombatAndInformMasters(combat, combatmaster1.getDefaultRange(), queue.toMessage());
                            } else {
                                for (Player p : combat.getFighters()) {
                                    p.sendMessage(queue.toMessage());
                                }
                                ServerProxy.informMasters(queue.toMessage(), executor);
                            }
                            break;
                        case SIMPLE: //send to range
                            if (DataHolder.inst().getMasterForCombat(combat.getId()) != null)
                                ServerProxy.sendMessageFromAndForPlayersInCombat(DataHolder.inst().getMasterForCombat(combat.getId()), queue.toMessage(), combat, DataHolder.inst().getMasterForCombat(combat.getId()).getDefaultRange());
                            else {
                                for (Player p : combat.getFighters()) {
                                    p.sendMessage(queue.toMessage());
                                }
                                ServerProxy.informMasters(queue.toMessage(), executor);
                            }
                            break;
                        case SILENT: //send only to GM(s)
                            executor.sendMessage(queue.toMessage());
                            break;
                    }
                }

                if (!shifted2 && combat.processExplosions()) {
                    combat.setWaitForBoom(true);
                    combat.processExplosions2();
                    return null;
                } else {
                    combat.setWaitForBoom(false);
                }

                queue.proceedIfNone();
                if (queue.getNext() != null) {
                    info.purge();
                    info.addComponent(new MessageComponent("Очередь переходит к " + queue.getNext().getName() + ".", ChatColor.COMBAT));
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, executor);
                    
                    logger.info(info.toString());

                    nextOne = queue.getNext();
                    nextOne.processMovement();
                    nextOne.clearOpportunities2();
                    nextOne.clearDefends();
                    nextOne.processStatusEffects(StatusEnd.TURN_START);

                    if (!dh.isNpc(nextOne.getName())) {
                        //TODO перенести эту кашу в отдельную функцию

                        try {
                            EntityLivingBase rider = ServerProxy.getForgePlayer(nextOne.getName());
                            if (rider != null && rider.getRidingEntity() != null) {
                                Entity mount = rider.getLowestRidingEntity();
                                Player mountPlayer = null;
                                if(mount instanceof EntityNPCInterface) {
                                    EntityNPCInterface npc = (EntityNPCInterface) mount;
                                    mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                                    if (rider.getRidingEntity() == mount) {
                                        if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    }
                                } else if (mount instanceof EntityPlayerMP) {
                                    EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                                    Player player = DataHolder.inst().getPlayer(playerMP.getName());
                                    if (player.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(player.getMovementHistory());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println(e);
                        }

                        if (nextOne.hasStatusEffect(StatusType.STUNNED) && !nextOne.cantBeStunned()) {
                            info.purge();
                            info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextOne.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                            for (Player p : combat.getFighters()) {
                                p.sendMessage(info);
                            }
                            ServerProxy.informMasters(info, executor);
                            
                            logger.info(info.toString());

                            nextOne.performCommand("/" + NextExecutor.NAME);

                        } else {
                            nextOne.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                            if (nextOne.getSubordinate() != null) nextOne.removeSubordinate();
                            nextOne.performCommand("/toolpanel");
                            GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 1F, 1F), ServerProxy.getForgePlayer(nextOne.getName()));
                        }
                    } else {

                        try {
                            EntityLivingBase rider = null;
                            rider = (EntityLivingBase) DataHolder.inst().getNpcEntity(nextOne.getName());
                            if (rider != null && rider.getRidingEntity() != null) {
                                Entity mount = rider.getLowestRidingEntity();
                                Player mountPlayer = null;
                                if(mount instanceof EntityNPCInterface) {
                                    EntityNPCInterface npc = (EntityNPCInterface) mount;
                                    mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                                    if (rider.getRidingEntity() == mount) {
                                        if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    }
                                } else if (mount instanceof EntityPlayerMP) {
                                    EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                                    Player player = DataHolder.inst().getPlayer(playerMP.getName());
                                    if (player.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(player.getMovementHistory());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println(e);
                        }

                        Player gm = dh.getMasterForNpcInCombat(nextOne);
                        if (gm == null) {
                            System.out.println("gm null wtf");
                        }
                        if (nextOne.hasStatusEffect(StatusType.STUNNED) && !nextOne.cantBeStunned()) {
                            info.purge();
                            info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextOne.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                            for (Player p : combat.getFighters()) {
                                p.sendMessage(info);
                            }
                            ServerProxy.informMasters(info, executor);
                            
                            logger.info(info.toString());
                            dh.getMasterForNpcInCombat(nextOne).performCommand("/" + NextExecutor.NAME);

                        } else {
                            gm.setSubordinate(nextOne);
                            gm.sendMessage(new Message(String.format("Ходит %s!", nextOne.getName()), ChatColor.BLUE));
                            gm.performCommand("/" + ToolPanel.NAME);
                            GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 0.5F, 1F), ServerProxy.getForgePlayer(gm.getName()));
                        }
                    }

                }
                dh.saveCombat(combat);
                return null;

            //TODO
            case "shift":
                if (args.length < 3) {
                    throw new CombatException("", "Укажите два ника: кого перемещать, и после кого его ставить в очереди.");
                }
                Player p1 = DataHolder.inst().getPlayer(args[1]);
                if (p1 == null) {
                    throw new DataException("", "Игрок " + args[1] + " не найден!");
                }
                executeAsFighter(p1, "shift", args[2]);
                dh.saveCombat(combat);
                return null;
            case "actnow":
                if (args.length < 2) {
                    throw new CombatException("1", "Ты не указаваешь, кого поднять в очереди.");
                }
                Player loser = queue.getNext();
                Player nextInQueue = DataHolder.inst().getPlayer(args[1]);
                queue.shift(nextInQueue, loser.getName(), true, true);
                queue.shift(loser, nextInQueue.getName(), true, true);
                loser.setCombatState(CombatState.SHOULD_WAIT);
                loser.performCommand("/containerpurge");
                nextInQueue.setCombatState(CombatState.SHOULD_ACT);
                Message mes1 = new Message(nextInQueue.getName() + " ходит прямо сейчас.", ChatColor.COMBAT);
                for (Player p : combat.getFighters()) {
                    p.sendMessage(mes1);
                    p.sendMessage(queue.toMessage());
                }
                ServerProxy.informMasters(mes1, executor);
                ServerProxy.informMasters(queue.toMessage(), executor);
                
                if (nextInQueue.hasStatusEffect(StatusType.STUNNED) && !nextInQueue.cantBeStunned()) {
                    info = new Message();
                    info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextInQueue.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, nextInQueue);
                    
                    logger.info(info.toString());
                    if (DataHolder.inst().isNpc(nextInQueue.getName())) {
                        DataHolder.inst().getMasterForNpcInCombat(nextInQueue).performCommand("/" + NextExecutor.NAME);
                    } else {
                        nextInQueue.performCommand("/" + NextExecutor.NAME);
                    }

                } else {
                    if (DataHolder.inst().isNpc(nextInQueue.getName())) {
                        DataHolder.inst().getMasterForNpcInCombat(nextInQueue).setSubordinate(nextInQueue);
                        DataHolder.inst().getMasterForNpcInCombat(nextInQueue).performCommand("/toolpanel");
                    } else {
                        nextInQueue.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                        nextInQueue.performCommand("/toolpanel");
                        GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 1F, 1F), ServerProxy.getForgePlayer(nextInQueue.getName()));
                    }
                }
                return null;
            case "autorenew":
                if (args.length < 2) {
                    throw new RuleException("Напишите /queue autorenew on либо /queue autorenew off.");
                }
                switch (args[1]) {
                    case "on":
                        queue.setAutoRenewing(true);
                        return new Message("Очередь будет пробрасываться автоматически в начале каждого раунда.", ChatColor.DIM);
//                        return ChatColor.DIM + "Очередь будет пробрасываться " + ChatColor.GMCHAT + "автоматически в начале каждого раунда.";
                    case "off":
                        queue.setAutoRenewing(false);
                        return new Message("Очередь не будет пробрасываться автоматически в начале каждого раунда. " +
                                "Чтобы пробросить очередь, пишите /queue go в начале каждого раунда.", ChatColor.DIM);
//                        return ChatColor.DIM + "Чтобы пробросить очередь, " + ChatColor.GMCHAT + "пишите /q go в начале каждого раунда.";
                }
            case "viewtype":
                if (args.length < 2) {
                    throw new RuleException("Укажите тип viewtype: auto, simple либо silent.");
                }
                switch (args[1]) {
                    case "auto":
                        reactionList.setViewType(ViewType.AUTO);
                        return new Message("Дайсы и очередь будут видны всем участникам боя.", ChatColor.COMBAT);
                    case "simple":
                        reactionList.setViewType(ViewType.SIMPLE);
                        return new Message("Дайсы и очередь не будут выводиться автоматически, используйте /queue turns.", ChatColor.COMBAT);
                    case "silent":
                        reactionList.setViewType(ViewType.SILENT);
                        return new Message("Дайсы и очередь будут видны только ГМ-ам.", ChatColor.COMBAT);

                }
            case "allow":
                if (args.length < 2) {
                    throw new CombatException("", "Укажите имя игрока!");
                }
                Player p = dh.getPlayer(args[1]);
                p.setCombatState(CombatState.SHOULD_ACT);
                p.setHasAttacked(false);
                p.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 2F, 1F), ServerProxy.getForgePlayer(p.getName()));


                info.purge();
                info.addComponent(new MessageComponent("[", ChatColor.COMBAT));
                info.addComponent(new MessageComponent("GM", ChatColor.DICE));
                info.addComponent(new MessageComponent("] Игрок " + p.getName() + " может ходить.", ChatColor.COMBAT));


                ServerProxy.informMasters(info, executor);
                
                logger.info(info.toString());
                return null;
            case "acted":
                if (args.length < 2) {
                    throw new CombatException("", "Укажите имя игрока!");
                }
                p = dh.getPlayer(args[1]);
                p.setCombatState(CombatState.ACTED);
                p.sendMessage(new Message("Твой ход закончен.", ChatColor.COMBAT));

                info.purge();
                info.addComponent(new MessageComponent("[", ChatColor.COMBAT));
                info.addComponent(new MessageComponent("GM", ChatColor.DICE));
                info.addComponent(new MessageComponent("] Ход игрока " + p.getName() + " закончен.", ChatColor.COMBAT));
                
                ServerProxy.informMasters(info, executor);
                logger.info(info.toString());
                return null;

            case "wait":
                if (args.length < 2) {
                    throw new CombatException("", "Укажите имя игрока!");
                }
                p = dh.getPlayer(args[1]);
                p.setCombatState(CombatState.SHOULD_WAIT);
                p.sendMessage(new Message("Твоя очередь ещё не наступила, дождись своего хода.", ChatColor.COMBAT));
                info.purge();
                info.addComponent(new MessageComponent("[", ChatColor.COMBAT));
                info.addComponent(new MessageComponent("GM", ChatColor.DICE));
                info.addComponent(new MessageComponent("] Игрок " + p.getName() + " будет ждать своего хода.", ChatColor.COMBAT));
                ServerProxy.informMasters(info, executor);
                logger.info(info.toString());
                return null;

            case "freeze":
                if (queue.isFreezed()) {
                    queue.setFreezed(false);
                    executor.sendMessage(new Message("Вы разморозили очередь!", ChatColor.COMBAT));
                    for (Player p2 : combat.getFighters()) {
                        p2.sendMessage(new Message("Очередь была разморожена.", ChatColor.COMBAT));
                    }
                    queue.getNext().sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                    GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 2F, 1F), ServerProxy.getForgePlayer(queue.getNext().getName()));

                } else {
                    queue.setFreezed(true);
                    executor.sendMessage(new Message("Вы заморозили очередь! Игрока не могут использовать /next и /shift.", ChatColor.COMBAT));
                    for (Player p2 : combat.getFighters()) {
                        p2.sendMessage(new Message("Очередь была заморожена. Дождитесь решения ГМа.", ChatColor.BLUE));
                    }
                }
                return null;
                /*
            case "super":
                if (args.length < 2) {
                    return ChatColor.RED + "Вы не указали, кого сделать супер-игроком!";
                }
                String superName = args[1];
                Player superPlayer = dh.getPlayer(superName);
                if (superPlayer == null) {
                    return ChatColor.RED + "Нет игрока с ником " + superName + "!";
                }
                if (superPlayer.isSuperFighter()) {
                    superPlayer.setSuperFighter(false);
                    return ChatColor.GRAY + superName + " больше не супер-игрок!";
                }
                if (!superPlayer.isSuperFighter()) {
                    superPlayer.setSuperFighter(true);
                    return ChatColor.GRAY + superName + " теперь супер-игрок!";
                }

                 */
        }
        return new Message("Неверный ввод!", ChatColor.RED);

    }

    public static void executeAsFighter(Player executor, String command, String... args) {
        DataHolder dh = DataHolder.inst();
        Combat combat = dh.getCombatForPlayer(executor.getName());
        if (combat == null) throw new RuleException("Вы не находитесь в бою!");
        ReactionList reactionList = combat.getReactionList();
        Queue queue = combat.getReactionList().getQueue();
        if (queue.isFreezed()) {
            executor.sendMessage(new Message("Очередь заморожена. Ждите решения ГМа.", ChatColor.RED));
            return;
        }

        switch (command) {
            case "next":
                if (executor.getCombatState() != CombatState.SHOULD_ACT) {
                    throw new RuleException("Сейчас не твой ход.");
                }

                if (combat.isAttacker(executor.getName())) {
                    throw new RuleException("Ты не можешь закончить свой ход, пока " + combat.getAttackByAttacker(executor.getName()).getDefender().getName() +
                            " не проведёт защиту!");
                }
//                if (executor.getMovementHistory() == MovementHistory.NOW) {
//                    executor.setMovementHistory(MovementHistory.LAST_TIME);
//                } else if (executor.getMovementHistory() == MovementHistory.LAST_TIME) {
//                    executor.setMovementHistory(MovementHistory.NONE);
//                }
                Message info;
                boolean shift = false;
                if (!combat.getWaitForBoom()) {
                Player prev = queue.getNext();
                shift = prev.hasShifted();
                DataHolder.inst().getModifiers().updateBloodlossAndStunning(queue.getNext(), queue.getNext().getName());
                boolean burned = false;
                if (prev.checkIfEndedTurnOnFire()) burned = true;
                if (prev.hasStatusEffect(StatusType.BURNING) && !prev.cantBurn() && !prev.getWoundPyramid().onlyDeadlyLeft()) {
                    String wound = prev.inflictDamage(3, "ожог от горения");
                    prev.takeAwayDurFromAllArmor(1);
                    if (wound.equals("критическая рана") || wound.equals("смертельная рана")) burned = true;
                }
                    queue.proceed();
                    switch (reactionList.getViewType()) {
                        case AUTO: //send to all members and GMs
                            Player combatmaster2 = DataHolder.inst().getMasterForCombat(combat.getId());
                            if (combatmaster2 != null) {
                                ServerProxy.sendMessageFromAndForAllPlayersInCombatAndInformMasters(combat, combatmaster2.getDefaultRange(), queue.toMessage());
                            } else {
                                for (Player p : combat.getFighters()) {
                                    p.sendMessage(queue.toMessage());
                                }
                                ServerProxy.informMasters(queue.toMessage(), executor);
                            }
                            break;
                        case SIMPLE: //send to range
                            if (DataHolder.inst().getMasterForCombat(combat.getId()) != null)
                                ServerProxy.sendMessageFromAndForPlayersInCombat(DataHolder.inst().getMasterForCombat(combat.getId()), queue.toMessage(), combat, DataHolder.inst().getMasterForCombat(combat.getId()).getDefaultRange());
                            else {
                                for (Player p : combat.getFighters()) {
                                    p.sendMessage(queue.toMessage());
                                }
                                ServerProxy.informMasters(queue.toMessage(), executor);
                            }
                            break;
                        case SILENT: //send only to GM(s)
                            ServerProxy.informMasters(queue.toMessage(), executor);
                            break;
                    }

                    if (args.length > 0 && !args[0].equals("combat")) {
                        prev.reduceRecoil();
                    }
                    if (burned) {
                        info = new Message(prev.getName() + " заканчивает свой ход и падает обугленный.", ChatColor.DARK_RED);
                        queue.removePlayerBc(prev.getName(), "обуглен");
                    } else if (prev.getBloodloss() > 99) {
                        info = new Message(prev.getName() + " заканчивает свой ход и теряет сознание от кровопотери.", ChatColor.DARK_RED);
                        queue.removePlayerBc(prev.getName(), "обескровлен");
                    } else if (args.length > 0 && args[0].equals("resetmovement")) {
                        info = new Message(prev.getName() + " заканчивает свой ход небоевым действием.", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("extinguish")) {
                        info = new Message(prev.getName() + " заканчивает свой ход катанием по земле и тушится.", ChatColor.COMBAT);
                        prev.removeStatusEffect(StatusType.BURNING);
                    } else if (args.length > 0 && args[0].equals("extinguishtarget")) {
                        info = new Message(prev.getName() + " заканчивает свой ход тушением " + args[1] + ".", ChatColor.COMBAT);
                        DataHolder.inst().getPlayer(args[1]).removeStatusEffect(StatusType.BURNING);
                    } else if (args.length > 0 && args[0].equals("getuptarget")) {
                        info = new Message(prev.getName() + " заканчивает свой ход подниманием " + args[1] + ".", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("bandaidtarget")) {
                        info = new Message(prev.getName() + " заканчивает свой ход первой помощью " + args[1] + ".", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("cpraidtarget")) {
                        info = new Message(prev.getName() + " заканчивает свой ход зажиманием ран " + args[1] + ".", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("charge")) {
                        info = new Message(prev.getName() + " заканчивает свой ход началом натиска.", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("concentrated")) {
                        String skill = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                        info = new Message(prev.getName() + " заканчивает свой ход концентрацией на навыке " + skill + ".", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("magic")) {
                        info = new Message(prev.getName() + " заканчивает свой ход подготовкой способности.", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("defensivestance")) {
                        info = new Message(prev.getName() + " заканчивает свой ход переходом в глухую оборону.", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("aim")) {
                        info = new Message(prev.getName() + " заканчивает свой ход переходом в режим прицеливания.", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("bipod")) {
                        info = new Message(prev.getName() + " заканчивает свой ход установкой сошек.", ChatColor.COMBAT);
                    } else if (args.length > 0 && args[0].equals("station")) {
                        info = new Message(prev.getName() + " заканчивает свой ход установкой оружия на станок.", ChatColor.COMBAT);
                    } else {
                        info = new Message(prev.getName() + " заканчивает свой ход.", ChatColor.COMBAT);
                    }

                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, executor);

                    logger.info(info.toString());

                    System.out.println("processing " + prev.getName());
                    prev.processStatusEffects(StatusEnd.TURN_END);
                }

                if (!shift && combat.processExplosions()) {
                    combat.setWaitForBoom(true);
                    combat.processExplosions2();
                    return;
                } else {
                    combat.setWaitForBoom(false);

                }
                queue.proceedIfNone();
                if (queue.getNext() != null) {
                    info = new Message("Очередь переходит к " + queue.getNext().getName() + ".", ChatColor.COMBAT);
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, executor);
                    
                    logger.info(info.toString());

                    Player nextOne = queue.getNext();
                    nextOne.processMovement();
                    nextOne.clearOpportunities2();
                    nextOne.clearDefends();
                    nextOne.processStatusEffects(StatusEnd.TURN_START);

                    if (!dh.isNpc(nextOne.getName())) {
                        try {
                            EntityLivingBase rider = ServerProxy.getForgePlayer(nextOne.getName());
                            if (rider != null && rider.getRidingEntity() != null) {
                                Entity mount = rider.getLowestRidingEntity();
                                Player mountPlayer = null;
                                if(mount instanceof EntityNPCInterface) {
                                    EntityNPCInterface npc = (EntityNPCInterface) mount;
                                    mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                                    if (rider.getRidingEntity() == mount) {
                                        if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement(); // если в той же очереди, то не надо. Маунт походит (и потеряет уровень разгона) в свой ход
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    }
                                } else if (mount instanceof EntityPlayerMP) {
                                    EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                                    Player player = DataHolder.inst().getPlayer(playerMP.getName());
                                    if (player.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(player.getMovementHistory());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println(e);
                        }

                        if (nextOne.hasStatusEffect(StatusType.STUNNED) && !nextOne.cantBeStunned()) {
                            info.purge();
                            info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextOne.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                            for (Player p : combat.getFighters()) {
                                p.sendMessage(info);
                            }
                            ServerProxy.informMasters(info, executor);
                            
                            logger.info(info.toString());
                            nextOne.performCommand("/" + NextExecutor.NAME);
                        } else {
                            nextOne.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                            nextOne.performCommand("/" + ToolPanel.NAME);
                            GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 1F, 1F), ServerProxy.getForgePlayer(nextOne.getName()));
                        }
                    } else {

                        try {
                            EntityLivingBase rider = (EntityLivingBase) DataHolder.inst().getNpcEntity(nextOne.getName());
                            if (rider != null && rider.getRidingEntity() != null) {
                                Entity mount = rider.getLowestRidingEntity();
                                Player mountPlayer = null;
                                if(mount instanceof EntityNPCInterface) {
                                    EntityNPCInterface npc = (EntityNPCInterface) mount;
                                    mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                                    if (rider.getRidingEntity() == mount) {
                                        if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                                    }
                                } else if (mount instanceof EntityPlayerMP) {
                                    EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                                    Player player = DataHolder.inst().getPlayer(playerMP.getName());
                                    if (player.getMovementHistory() == MovementHistory.NOW) {
                                        nextOne.setMovementHistory(player.getMovementHistory());
                                    }
                                }
                            }
                        } catch (Exception e) {
                            System.out.println(e);
                        }

                        Player gm = dh.getMasterForNpcInCombat(nextOne);
                        if (gm == null) {
                            System.out.println("gm null wtf");
                        }
                        if (nextOne.hasStatusEffect(StatusType.STUNNED) && !nextOne.cantBeStunned()) {
                            info.purge();
                            info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextOne.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                            for (Player p : combat.getFighters()) {
                                p.sendMessage(info);
                            }
                            ServerProxy.informMasters(info, executor);
                            
                            logger.info(info.toString());
                            dh.getMasterForNpcInCombat(nextOne).performCommand("/" + NextExecutor.NAME);
                        } else {
                            gm.setSubordinate(nextOne);
                            gm.sendMessage(new Message(String.format("Ходит %s!", nextOne.getName()), ChatColor.BLUE));
                            gm.performCommand("/" + ToolPanel.NAME);
                            GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 0.5F, 1F), ServerProxy.getForgePlayer(gm.getName()));
                        }
                    }
                }
                if (queue.getNext() == null && queue.isAutoRenewing()) {
                    QueueExecutor.execute(dh.getMasterForCombat(combat.getId()), "go");
                }
                dh.saveCombat(combat);
                return;
            case "actnow":
                if (!executor.hasPermission(Permission.GM)) return;
                if (args.length < 1) {
                    throw new CombatException("1", "Ты не указаваешь, кого поднять в очереди.");
                }
                Player loser = queue.getNext();
                Player nextInQueue = DataHolder.inst().getPlayer(args[0]);
                queue.shift(nextInQueue, loser.getName(), true, true);
                queue.shift(loser, args[0], true, true);
                loser.setCombatState(CombatState.SHOULD_WAIT);
                loser.performCommand("/containerpurge");
                nextInQueue.setCombatState(CombatState.SHOULD_ACT);
                Message mes1 = new Message(args[0] + " ходит прямо сейчас.", ChatColor.COMBAT);
                for (Player p : combat.getFighters()) {
                    p.sendMessage(mes1);
                    p.sendMessage(queue.toMessage());
                }
                ServerProxy.informMasters(mes1, executor);
                ServerProxy.informMasters(queue.toMessage(), executor);
                
                if (nextInQueue.hasStatusEffect(StatusType.STUNNED) && !nextInQueue.cantBeStunned()) {
                    info = new Message();
                    info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextInQueue.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, nextInQueue);
                    
                    logger.info(info.toString());

                    nextInQueue.performCommand("/" + NextExecutor.NAME);

                } else {
                    nextInQueue.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                    if (nextInQueue.getSubordinate() != null) nextInQueue.removeSubordinate();
                    nextInQueue.performCommand("/toolpanel");
                    GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 1F, 1F), ServerProxy.getForgePlayer(nextInQueue.getName()));
                }
                return;
            case "shift":
                if (args.length < 1) {
                    throw new CombatException("1", "Ты не указаваешь, после кого хочешь встать в очереди.");
                }
                if (executor.hasShifted()) {
                    throw new RuleException("Ты уже перемещался в этот раунд.");
                }

                queue.shift(executor, args[0]);
                Message mes = new Message(" смещается в очереди, ходит после " + args[0] + ".", ChatColor.COMBAT);
                for (Player p : combat.getFighters()) {
                    p.sendMessage(mes);
                    p.sendMessage(queue.toMessage());
                }
                ServerProxy.informMasters(mes, executor);
                ServerProxy.informMasters(queue.toMessage(), executor);
                
                logger.info(mes.toString());


                Message additionalInfo = new Message("[", ChatColor.COMBAT);
                additionalInfo.addComponent(new MessageComponent("GM", ChatColor.DICE));
                additionalInfo.addComponent(new MessageComponent("]" + executor.getName() + " сместился в очереди, после " + args[0] + ".", ChatColor.COMBAT));
                ServerProxy.informMasters(additionalInfo, executor);

                // We do not return message but directly send it because this command could be executed from execute() by GM
                // if it's stupid and it works, it ain't stupid!
                executor.sendMessage(new Message("Ты сместился в очереди и будешь ходить после " + args[0] + ".", ChatColor.COMBAT));
                dh.saveCombat(combat);
                return;
        }
        executor.sendMessage(new Message("Неверный ввод!", ChatColor.RED));
    }



}
