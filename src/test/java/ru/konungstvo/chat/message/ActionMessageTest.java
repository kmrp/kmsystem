package ru.konungstvo.chat.message;

import net.minecraft.util.text.TextComponentString;
import org.junit.Test;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.range.Range;

import static org.junit.Assert.*;

public class ActionMessageTest {
    @Test
    public void ActionMessage_BuildsCorrectly() {
        RoleplayMessage roleplayMessage = new ActionMessage("Volpe", "doing stuff", Range.NORMAL);
        roleplayMessage.build();
        System.out.println(roleplayMessage.toString());
        TextComponentString res = MessageGenerator.generate(roleplayMessage);
        System.out.println(res.toString());
//        assertEquals("TextComponent{text='* ', siblings=[" +
//                              "TextComponent{text='Volpe', siblings=[], style=Style{hasParent=true, color=§a, bold=false, italic=false, underlined=false, obfuscated=null, clickEvent=null, hoverEvent=null, insertion=null}}, " +
//                              "TextComponent{text=' doing stuff', siblings=[], style=Style{hasParent=true, color=§r, bold=false, italic=false, underlined=false, obfuscated=null, clickEvent=null, hoverEvent=null, insertion=null}}], style=Style{hasParent=false, color=§r, bold=false, italic=false, underlined=false, obfuscated=null, clickEvent=null, hoverEvent=null, insertion=null}}",
//                              res.toString());
    }

}