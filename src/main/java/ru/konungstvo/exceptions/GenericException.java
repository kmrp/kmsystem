package ru.konungstvo.exceptions;

public class GenericException extends RuntimeException {
    private final String errorName;

    public GenericException(String errorMessage) {
        this("Exception", errorMessage);
    }

    public GenericException(String errorName, String errorMessage) {
        super(errorMessage);
        this.errorName = errorName;
    }

    public String getErrorName() {
        return errorName;
    }

}
