package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.combat.movement.MovementTrait;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.Arrays;

public class TraitExecutor extends CommandBase {

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String execute(Player executor, String... args) {
        if (args.length < 1) {
            return "Тут будет хлеб";
        }

        Player pl = DataHolder.inst().getPlayer(args[0]);
        if (pl == null) {
            return "Игрок " + args[0] + " не найден!";
        }

        if (args.length == 1) {
            String result = "";
            if (pl.getMovementTrait() == MovementTrait.FAST) {
                result += "\nочень быстрый";
            } else if (pl.getMovementTrait() == MovementTrait.SUPERFAST) {
                result += "\nсверхбыстрый";
            }
            if (pl.isHardened()) {
                result += "\nтолстокожий";
            }
            if (pl.isInsensitive()) {
                result += "\nнечувствительный";
            }
            if (result.isEmpty()) {
                result = " Пусто!";
            }
            return "НЕ ГОТОВО Трейты для " + args[0] + ":" + result;
        }



        String arg = args[1];
        if (arg.equals("clear")) {
            pl.clearTraits();
            return "Трейты для " + args[0] + " очищены.";
        }

        boolean remove = false;
        if (arg.equals("remove") || arg.equals("delete")) {
            arg = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
            remove = true;
        } else if (arg.equals("add")) {
            arg = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
        } else {
            arg = String.join(" ", Arrays.copyOfRange(args,1, args.length));
        }

        switch (arg) {
            case "нечувствительность": //чек
                pl.setInsensitive(!remove);
                break;
            case "сосредоточенность": //чек
                pl.setCollected(!remove);
                break;
            case "стойкость": //чек
                pl.setPersistent(!remove);
                break;
            case "податливость": //чек
                pl.setIrresolute(!remove);
                break;

            case "быстрота": //чек
                if (remove) pl.setMovementTrait(MovementTrait.NORMAL);
                else pl.setMovementTrait(MovementTrait.FAST);
                break;
            case "сверхбыстрота": //чек
                if (remove) pl.setMovementTrait(MovementTrait.NORMAL);
                else pl.setMovementTrait(MovementTrait.SUPERFAST);
                break;

            case "толстокожесть": //чек
                if (remove) pl.setHardened(0);
                else pl.setHardened(1);
                break;
            case "толстокожесть+": //чек
                if (remove) pl.setHardened(0);
                else pl.setHardened(2);
                break;
            case "толстокожесть++": //чек
                if (remove) pl.setHardened(0);
                else pl.setHardened(3);
                break;

            case "невосприимчивость к натиску": //чек
                pl.setCantBeCharged(!remove);
                break;
            case "невосприимчивость к кровотечению": //чек
                pl.setCantBleed(!remove);
                break;
            case "невосприимчивость к оглушению": //чек
                pl.setCantBeStunned(!remove);
                break;
            case "невосприимчивость к повреждению конечностей": //чек
                pl.setCantGetLimbInjury(!remove);
                break;
            case "невосприимчивость к горению": //чек
                pl.setCantBurn(!remove);
                break;
            case "невосприимчивость к ментальным ранам": //чек
                pl.setApatic(!remove);
                break;
            case "крохотность": //чек
                pl.setSmall(!remove);
                break;
            case "гигантизм": //чек
                pl.setBig(!remove);
                break;

            case "сопротивляемость к дробящему": //чек
                if (remove) pl.setCrushingRes(0);
                else pl.setCrushingRes(1);
                break;
            case "уязвимость к дробящему": //чек
                if (remove) pl.setCrushingRes(0);
                else pl.setCrushingRes(-1);
                break;
            case "сопротивляемость к колющему": //чек
                if (remove) pl.setPiercingRes(0);
                else pl.setPiercingRes(1);
                break;
            case "уязвимость к колющему": //чек
                if (remove) pl.setPiercingRes(0);
                else pl.setPiercingRes(-1);
                break;
            case "сопротивляемость к режуще-рубящему": //чек
                if (remove) pl.setSlashingRes(0);
                else pl.setSlashingRes(1);
                break;
            case "уязвимость к режуще-рубящему": //чек
                if (remove) pl.setSlashingRes(0);
                else pl.setSlashingRes(-1);
                break;
            case "сопротивляемость к современному огнестрельному": //чек
                if (remove) pl.setModernFirearmRes(0);
                else pl.setModernFirearmRes(1);
                break;
            case "уязвимость к современному огнестрельному": //чек
                if (remove) pl.setModernFirearmRes(0);
                else pl.setModernFirearmRes(-1);
                break;
            case "сопротивляемость к устаревшему дальнему": //чек
                if (remove) pl.setRangedOldRes(0);
                else pl.setRangedOldRes(1);
                break;
            case "уязвимость к устаревшему дальнему": //чек
                if (remove) pl.setRangedOldRes(0);
                else pl.setRangedOldRes(-1);
                break;
            case "сопротивляемость к ближним атакам": //чек
                if (remove) pl.setMeleeRes(0);
                else pl.setMeleeRes(1);
                break;
            case "уязвимость к ближним атакам": //чек
                if (remove) pl.setMeleeRes(0);
                else pl.setMeleeRes(-1);
                break;
            case "сопротивляемость к дальним атакам": //чек
                if (remove) pl.setRangedRes(0);
                else pl.setRangedRes(1);
                break;
            case "уязвимость к дальним атакам": //чек
                if (remove) pl.setRangedRes(0);
                else pl.setRangedRes(-1);
                break;
            default:
                return "Трейт " + arg + " не найден";
        }
        if (remove) {
            return "Трейт " + arg + " был убран для игрока " + pl.getName() + ".";
        }
        return "Трейт " + arg + " был добавлен игроку " + pl.getName() + ".";
    }

    @Override
    public String getName() {
        return "trait";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }
}
