package ru.konungstvo.player.wounds;

public enum WoundType {
    DEADLY(4, "смертельная рана"),
    CRITICAL(3, "критическая рана"),
    SEVERE(2, "тяжёлая рана"),
    LIGHT(1, "лёгкая рана"),
    SCRATCH(0, "царапина");

    private int mod;
    private String desc;
    WoundType(int mod, String desc) {
        this.mod = mod;
        this.desc = desc;
    }

    public int getMod() {
        if (mod < 0) {
            return 0;
        }
        return -mod;
    }

    public int getInt() {
        return mod;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescFromDamage(int damage) {
        if (damage < 0) return null;
        if (damage <= 3) return SCRATCH.getDesc();
        if (damage <= 6) return LIGHT.getDesc();
        if (damage <= 9) return SEVERE.getDesc();
        if (damage <= 12) return CRITICAL.getDesc();
        return DEADLY.getDesc();
    }
}
