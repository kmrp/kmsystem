package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.konungstvo.bridge.ServerProxy.findAllPlayersInRange;

public class ExamineExecutor extends CommandBase {
    private static String START_MESSAGE = "Выберите кого осматривать!\n";
    public static final String NAME = "examine";

    @Override
    public String getName() {
        return "examine";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        String oldoldContainer = "";
        Player target = null;
        if(args.length > 0 && !args[0].equals("%IGNORE%")) {
            target = DataHolder.inst().getPlayer(args[0]);
            if (target == null) return;
        }
        if (args.length > 1) {
            oldContainer = args[1];
        }

        if (args.length > 2) {
            oldoldContainer = args[2];
        }

        if (!oldContainer.isEmpty()) {
            ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));

            if (oldContainer.equals("Examine")) return;
        }


        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();
        EntityLivingBase forgePlayer;
        EntityLivingBase forgeTarget;

        if (!DataHolder.inst().isNpc(subordinate.getName())) {
            forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
        } else {
            forgePlayer = DataHolder.inst().getNpcEntity(subordinate.getName());
        }

        if (target == null) {
            Message chooseExamine = new Message(START_MESSAGE, ChatColor.COMBAT);
            if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) == null) {
                sender.sendMessage(new TextComponentString("Осматривать можно только в бою."));
            }
            List<Player> targets = findAllPlayersInRange(subordinate, 16);
            for (Player potentialTarget : targets) {
                if (potentialTarget == subordinate) continue;
                if (potentialTarget.hasPermission(Permission.GM)) continue;
                if (potentialTarget != null) {
                    if (!DataHolder.inst().isNpc(potentialTarget.getName())) {
                        forgeTarget = ServerProxy.getForgePlayer(potentialTarget.getName());
                    } else {
                        forgeTarget = (EntityLivingBase) DataHolder.inst().getNpcEntity(potentialTarget.getName());
                    }
                } else {
                    continue;
                }
                if (player.hasPermission(Permission.GM) || (forgeTarget.canEntityBeSeen(forgePlayer) && forgeTarget.getDistance(forgePlayer) <= 16)) {
                    MessageComponent potTarg = new MessageComponent("[" + potentialTarget.getName() + "] ", ChatColor.BLUE);
                    potTarg.setHoverText("[Убедитесь, что цель в пределах видимости вашего персонажа!]", TextFormatting.RED);
                    potTarg.setClickCommand("/examine " + potentialTarget.getName());
                    chooseExamine.addComponent(potTarg);
                }
            }
            MessageComponent back = new MessageComponent("[Назад]", ChatColor.GRAY);
            back.setClickCommand("/ToRoot Examine " + oldContainer + " " + oldoldContainer);
            chooseExamine.addComponent(back);

            Helpers.sendClickContainerToClient(chooseExamine, "Examine", (EntityPlayerMP) sender);
//            String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(chooseExamine));
//            ClickContainerMessage result = new ClickContainerMessage("Examine", json);
//            KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
            return;
        }

        if (target != null) {
            if (!DataHolder.inst().isNpc(target.getName())) {
                forgeTarget = ServerProxy.getForgePlayer(target.getName());
            } else {
                forgeTarget = DataHolder.inst().getNpcEntity(target.getName());
            }
        } else {
            return;
        }


        System.out.println("is null " + (forgeTarget == null) + " " + (forgePlayer == null));
        if (forgePlayer == null || forgeTarget == null) return;
        if (player.hasPermission(Permission.GM) || forgeTarget.getDistance(forgePlayer) <= 16) {
            Message examineResult = new Message();
            target.updateArmor();
            DataHolder.inst().updatePlayerWounds(target.getName());
            MessageComponent name = new MessageComponent("Информация о §a" + target.getName() + "§e:\n", ChatColor.DICE);
            MessageComponent wounds = new MessageComponent("[Раны] ", ChatColor.RED);
            wounds.setHoverText(target.getWoundPyramid().toNiceString(target.getName()), TextFormatting.GOLD);
            MessageComponent armor = new MessageComponent("[Броня] ", ChatColor.GRAY);
            armor.setHoverText(target.getArmor().toNiceString(), TextFormatting.GOLD);
            MessageComponent effects = null;
            if (target.hasStatusEffects()) {
                effects = new MessageComponent("[Эффекты] ", ChatColor.COMBAT);
                effects.setHoverText(target.getEffectMessage(true), TextFormatting.GOLD);
            }
            MessageComponent weapon = new MessageComponent("[Оружие] ", ChatColor.BLUE);
            StringBuilder weaponhover = new StringBuilder();
            for (int hand = 1; hand > -1; hand--) {
                if (WeaponTagsHandler.hasWeaponTags(target.getItemForHand(1))) {
                    String handName = (hand == 1 ? "Правая" : "Левая");
                    WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(target.getItemForHand(hand));
                    if (weaponTagsHandler.isWeapon()) {
                        Weapon weapon1 = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon());
                        if (weapon1.isRanged()) {
                            if (weapon1.getFirearm().getProjectile().getCaliber().equals("способность")) {
                                weaponhover.append(handName).append(" рука [???]\n");
                            } else {
                                weaponhover.append(handName).append(" рука [").append(weapon1.getName()).append(": ");
                                weaponhover.append(weapon1.getFirearm().getSortString()).append(" ").append(weapon1.getFirearm().getWeaponType()).append("]\n");
                            }
                        } else {
                            if (weapon1.getMelee().isMagic()) {
                                weaponhover.append(handName).append(" рука [???]\n");
                            } else {
                                weaponhover.append(handName).append(" рука [").append(weapon1.getName()).append(": ");
                                weaponhover.append(weapon1.getMelee().getCategory()).append(" ").append((weapon1.getMelee().isSmall() ? "короткое " : ""))
                                        .append((weapon1.getMelee().isTwohanded() ? "двуручное " : "")).append((weapon1.getMelee().isNonlethal() ? "нелетальное " : ""))
                                        .append((weapon1.getMelee().isPolearm() ? "древковое " : "")).append("]\n");
                            }
                        }
                    } else if (weaponTagsHandler.isShield()) {
                        Shield shield = new Shield(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon());
                        weaponhover.append(handName).append(" рука [").append(shield.getName()).append(": ").append(shield.getType()).append(" ").append(shield.getMaterial()).append("]\n");
                    }
                }
            }
            weapon.setHoverText(weaponhover.toString().trim(), TextFormatting.BLUE);

            MessageComponent traitsMes = new MessageComponent("[Трейты] ", ChatColor.YELLOW);
            String traits = target.getTraitsString();
            traitsMes.setHoverText(traits, TextFormatting.YELLOW);



            MessageComponent close = new MessageComponent("[x]", ChatColor.GRAY);
            close.setClickCommand("/examine %IGNORE% Examine");


            examineResult.addComponent(name);
            examineResult.addComponent(wounds);
            examineResult.addComponent(armor);
            if (!traits.isEmpty()) examineResult.addComponent(traitsMes);
            if (effects != null) examineResult.addComponent(effects);
            if (!weaponhover.toString().isEmpty()) examineResult.addComponent(weapon);
            if (target.getMovementHistory() == MovementHistory.RUN_UP || target.getMovementHistory() == MovementHistory.NOW || target.getMovementHistory() == MovementHistory.LAST_TIME) {
                MessageComponent runup = new MessageComponent("[Разбег] ", ChatColor.DARK_AQUA);
                examineResult.addComponent(runup);
            }
            if (target.getChargeHistory() != ChargeHistory.NONE) {
                MessageComponent runup = new MessageComponent("[Натиск] ", ChatColor.DARK_RED);
                examineResult.addComponent(runup);
            }
            examineResult.addComponent(close);
            ServerProxy.sendMessage(subordinate, subordinate.getDefaultRange(), new Message(subordinate.getName() + " осматривает " + target.getName() + "...", ChatColor.COMBAT));

            Helpers.sendClickContainerToClient(examineResult, "Examine", (EntityPlayerMP) sender);
//            String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(examineResult));
//            ClickContainerMessage result = new ClickContainerMessage("Examine", json);
//            KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);

            if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT) {
                player.performCommand("/toolpanel");
            }
        }
    }
}
