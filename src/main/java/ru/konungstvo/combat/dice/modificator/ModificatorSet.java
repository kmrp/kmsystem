package ru.konungstvo.combat.dice.modificator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ModificatorSet  {
    List<Modificator> set;

    public ModificatorSet() {
        this.set = new ArrayList<>();
    }

    public ModificatorSet add(Modificator modificator) {
        for (Modificator mod : set) {
            if (modificator.getType().equals(mod.getType())) {
                mod.add(modificator.getMod());
                return this;
            }
        }
        set.add(modificator);
        return this;
    }

    public void remove(ModificatorType modificatorType) {
        for (Modificator mod : set) {
            if (modificatorType.equals(mod.getType())) {
                set.remove(mod);
                return;
            }
        }
    }


    public List<Modificator> getMods() {
        return set;
    }

    public Modificator getMod(ModificatorType type) {
        for (Modificator mod : set) {
            if (mod.getType().equals(type))
                return mod;
        }
        return null;
    }
}
