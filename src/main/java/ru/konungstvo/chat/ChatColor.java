package ru.konungstvo.chat;

import net.minecraft.util.text.TextFormatting;

/*
 color: The color to render this text in. Valid values are "black", "dark_blue", "dark_green", "dark_aqua",
 "dark_red", "dark_purple", "gold", "gray", "dark_gray", "blue", "green", "aqua", "red", "light_purple",
 "yellow", "white", and "reset" (cancels out the effects of colors used by parent objects). Technically,
 "bold", "italic", "underlined", "strikethrough", and "obfuscated" are also accepted, but it may be
 better practice to use the tags below for such formats.
 */
public enum ChatColor {
    PREFIX(TextFormatting.DARK_GRAY),
    YELLOW(TextFormatting.YELLOW),
    STORY(TextFormatting.GOLD),
    NICK(TextFormatting.GREEN),
    GMCHAT(TextFormatting.GOLD),
    CMCHAT(TextFormatting.DARK_AQUA),
    OOC(TextFormatting.LIGHT_PURPLE),
    RED(TextFormatting.RED),
    DARK_RED(TextFormatting.DARK_RED),
    DICE(TextFormatting.YELLOW),
    DEFAULT(TextFormatting.RESET),
    COMBAT(TextFormatting.GOLD),
    GLOBAL(TextFormatting.AQUA),
    DIM(TextFormatting.GRAY),
    BLUE(TextFormatting.BLUE),
    GRAY(TextFormatting.GRAY),
    PURPLE(TextFormatting.LIGHT_PURPLE),
    DARK_PURPLE(TextFormatting.DARK_PURPLE),
    DARK_GREEN(TextFormatting.DARK_GREEN),
    DARK_BLUE(TextFormatting.DARK_BLUE),
    DARK_AQUA(TextFormatting.DARK_AQUA),
    DARK_GRAY(TextFormatting.DARK_GRAY);
    /*
    PREFIX("§8"),
    STORY("§e"),
    NICK("§a"),
    GMCHAT("§6"),
    BDCHAT("§3"),
    OOC("§d"),
    RED("§4"),
    DICE("§e"),
    DEFAULT("§f"),
    COMBAT("§6"),
    GLOBAL("§b"),
    DIM("§8"),
    BLUE("§3"),
    GREY("§7"),
    PURPLE("§5");
     */

    private TextFormatting color;
    ChatColor(TextFormatting color) {
        this.color = color;
    }
    public TextFormatting get() {
        return color;
    }
}
