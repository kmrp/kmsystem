package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import java.util.Arrays;
import java.util.List;

public class DefensiveStance extends CommandBase {
    private static String START_MESSAGE = "Выберите %s!\n";

    @Override
    public String getName() {
        return "defensivestance";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        String oldest = "";

        if(args.length > 0) oldContainer = args[0];
        if(args.length > 1) oldest = args[1];
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());

        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (subordinate.getSubordinate() != null) {
            subordinate = subordinate.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(subordinate.getName());
            if (!DataHolder.inst().isNpc(subordinate.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
            }
        }

        ItemStack leftHand = forgePlayer.getHeldItemOffhand();
        ItemStack rightHand = forgePlayer.getHeldItemMainhand();
        WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
        WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);
        Weapon weapon = null;
        Message message;

        Shield shieldLeft = null;
        Shield shieldRight = null;

        if (leftWeapon.isShield()) shieldLeft = new Shield(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());
        if (rightWeapon.isShield()) shieldRight = new Shield(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());

        if (subordinate.hasDefensiveStance() && oldContainer.equals("turnoff")) {
            if(args.length > 1) oldContainer = args[1];
            if(args.length > 2) oldest = args[2];
            subordinate.setDefensiveStance(false);
            if(!oldContainer.isEmpty()) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + oldContainer + " " + oldest);
            return;
        }

        if((leftHand.isEmpty() || shieldLeft == null || !shieldLeft.fitForDefStance()) && (!rightHand.isEmpty() || shieldRight == null || !shieldRight.fitForDefStance())) {
            player.sendMessage(new Message("Нет подходящего щита.", ChatColor.RED));
            return;
        }

        // REMOVE PREVIOUS PANEL
        if (oldContainer.equals("continue")) {
            ClickContainerMessage remove = new ClickContainerMessage("PerformAction", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        }
            if (!oldContainer.isEmpty() &&  !oldContainer.equals("continue")) {
                ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            }
            System.out.println(subordinate.getAttachedCombatID() != -1);
            System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()) != null);
            //System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()).getReactionList().getQueue().getNext() == subordinate);
            if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && DataHolder.inst().getCombatForPlayer(subordinate.getName()).getReactionList().getQueue().getNext() == subordinate) {
                subordinate.addStatusEffect("концентрация", StatusEnd.TURN_END, 1, StatusType.CONCENTRATED, "блокирование", true);
                subordinate.setMovementHistory(MovementHistory.NONE);
                subordinate.setChargeHistory(ChargeHistory.NONE);
                System.out.println("test");
                subordinate.setDefensiveStance(true);
                if (player.getCombatState().equals(CombatState.SHOULD_ACT)) player.performCommand("/"+ NextExecutor.NAME +" defensivestance");
            } else {
                subordinate.addStatusEffect("концентрация", StatusEnd.TURN_END, 1, StatusType.CONCENTRATED, "блокирование");
                subordinate.setDefensiveStance(true);
            }
            return;

    }
}
