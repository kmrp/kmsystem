package ru.konungstvo.combat.dice;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Dice of type "NdN+N", such as d6 or 2d3 or d5+1.
 */
public class CommonDice {
    private int max; // max value of the die, dN
    private int count = 1; // value before 'd', how many times to throw, Nd
    private int mod = 0; // value to add to result, d+N
    private int result = 0;
    private String comment = "";
    private List<Integer> dice = new ArrayList<>();

    public static String regex = "^-?([0-9]{0,2})d([0-9]{1,3})([-,+][0-9]{0,3})?(.*)?";

    public CommonDice(String toParse) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(toParse);

        if (!matcher.find()) {
            throw new IllegalStateException("Doesn't match dice pattern!");
        }

        // parse values and set boundaries
        if (matcher.group(1) != null && !matcher.group(1).equals("")) {
            int newCount = Integer.valueOf(matcher.group(1));
            if (newCount > 10 || newCount < 2) throw new IllegalStateException("Too many or to few dices to throw! Should be between 2 and 10.");
            this.count = newCount;
        }
        if (matcher.group(3) != null && !matcher.group(3).equals("")) {
            this.mod = Integer.valueOf(matcher.group(3));
            if (this.mod > 100 || this.mod < -100) throw new IllegalStateException("Modifier is too big or to low! Should be between -100 and 100.");
        }
        if (matcher.group(4) != null) {
            this.comment = matcher.group(4).replaceAll("§.", "");
        }
        this.max = Integer.valueOf(matcher.group(2));
        if (this.max > 100) throw new IllegalStateException("Value is too big!");

    }

    // throw dice
    public int cast() {
        if (this.result != 0) throw new IllegalStateException("Dice was already thrown!");
        Random random = new Random();
        for (int i = 0; i < this.count; i++) {
            int ran = random.nextInt(this.max)+1;
            dice.add(ran);
            this.result += ran;
        }
        return this.result;
    }

    public int getMax() {
        return max;
    }

    public int getCount() {
        return count;
    }

    public int getMod() {
        return mod;
    }

    public String getCountAsString() {
        if (this.count == 1)
            return "";
        return String.valueOf(this.count);
    }

    public String getMaxAsString() {
        return String.valueOf(this.max);
    }

    public String getModAsString() {
        if (this.mod == 0)
            return "";
        if (this.mod > 0)
            return "+" + String.valueOf(this.mod);
        return String.valueOf(this.mod);
    }

    public int getFinalResult() {
        return result + this.mod;
    }

    public String getResultAsString() {
        if (this.result == 0) cast();
        if (this.count == 1 && this.mod == 0)
            return "Выпадает " + String.valueOf(this.result);
        if (this.count == 1)
            return "Выпадает " + String.valueOf(this.result) + ", результат: " + getFinalResult();
        if (this.count > 1) {
            StringBuilder result = new StringBuilder("Выпадает: ");
            for (Integer i : dice) result.append(String.valueOf(i)).append(", ");
            result.append("результат: ");
            result.append(getFinalResult());
            return result.toString();
        }
        return "";
    }

    public static boolean matches(String context) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(context);
        return matcher.find();
    }

    public String getComment() {
        if (!comment.equals("")) {
            return " (" + comment.trim() + ")";
        }
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
