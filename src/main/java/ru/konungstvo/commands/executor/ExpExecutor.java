package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;

import static ru.konungstvo.control.Helpers.hasSpaceForXp;

public class ExpExecutor extends CommandBase {


    @Override
    public String getName() {
        return "expa";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        DataHolder dh = DataHolder.inst();
        if (args.length < 1) {
            //Считать вес
            ArrayList<String> xp = Helpers.getXp(player.getName());

            if (xp.size() == 0) {
                player.sendMessage(new Message("§aЗа сегодня накоплено опыта §2[0/5]§a:\n" +
                        "§cВы не накопили опыта за сегодня."));

            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("§aЗа сегодня накоплено опыта §2[").append(xp.size()).append("/5]§a:\n");
                for (String line : xp) {
                    sb.append("§7").append(line.split(":")[0]).append(" §a+").append(line.split(":")[1]).append("‰\n");
                }
                player.sendMessage(new Message(sb.toString().trim()));
            }
            return;

        }
        if (!sender.getName().contains("CustomNPC") && !player.hasPermission(Permission.GM)) {
            throw new PermissionException("1", "Недостаточно прав!");
        }

        if (args[0].equals("add")) {
            if (args.length < 4) return;
            Player target = DataHolder.inst().getPlayer(args[1]);
            int amount = Integer.parseInt(args[2]);
            String skill = String.join(" ", Arrays.copyOfRange(args, 3, args.length));
            if (!hasSpaceForXp(target.getName())) {
                target.sendMessage(new Message("Нет места для опыта.", ChatColor.DARK_RED));
                return;
            }
            Helpers.addXp(target.getName(), skill + ":" + amount);
            target.sendMessage(new Message("Получено " + amount + " опыта к навыку " + skill + "!", ChatColor.NICK));
            return;
        }
    }
}
