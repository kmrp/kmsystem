package ru.konungstvo.combat.equipment;

import org.json.simple.parser.ParseException;

public class Trash extends Weapon {
    public Trash() {
        this("trash");
    }
    public Trash(String name) {
        super(name, null);
    }

    @Override
    public void fillFromJson(String jsonStr) throws ParseException {
    }

    // Damage and reach by Trash is hardcoded (always the same)
    @Override
    public int getDamage() {
        return 1;
    }

    @Override
    public double getReach() {
        return 2;
    }


}
