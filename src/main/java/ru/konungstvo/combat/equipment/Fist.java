package ru.konungstvo.combat.equipment;

import java.util.ArrayList;

public class Fist extends Weapon {

    public Fist() {
        this("fist");
    }
    public Fist(String name) {
        super(name);
    }
    // Damage and reach by Fist is hardcoded (always the same)
    @Override
    public int getDamage() {
        return 0;
    }

    @Override
    public double getReach() {
        return 1.8;
    }

    //@Override
    //public boolean isMelee() {return true;}

    @Override
    public boolean isFist() {return true;}

    @Override
    public ArrayList<String> getProficiencies() {
        return new ArrayList<String>();
    }

}
