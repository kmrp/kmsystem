package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.*;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.command.LoreException;
import ru.konungstvo.kmrp_lore.helpers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArmorMergeCommand extends CommandBase {
    public static final String NAME = "armormerge";
    public static final List<String> segments = Arrays.asList("голова","шея","торс","левое плечо","левый локоть","левое предплечье","левая кисть","правое плечо","правый локоть","правое предплечье","правая кисть","живот","пах","правое бедро","левое бедро","правое колено","левое колено","правая голень","левая голень","правая стопа","левая стопа");

    @Override
    public String getName() { return NAME; }

    @Override
    public String getUsage(ICommandSender sender) { return null; }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }
        ClickContainer.deleteContainer("ArmorMerge", getCommandSenderAsPlayer(sender));

        ItemStack item = ((EntityPlayerMP) sender).getHeldItemMainhand();

        TextComponentString result;

        if(item.isEmpty()) {
            result = new TextComponentString("Нет брони в руках.");
            result.getStyle().setColor(TextFormatting.GRAY);
            sender.sendMessage(result);
            return;
        }

        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(item);
        WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(item);
        WeightHandler weightHandler = new WeightHandler(item);

        if(!weaponTagsHandler.getWeaponTags().hasKey("Броня")) {
            result = new TextComponentString("Нет брони в руках.");
            result.getStyle().setColor(TextFormatting.GRAY);
            sender.sendMessage(result);
            return;
        }

        ItemStack item2 = ((EntityPlayerMP) sender).getHeldItemOffhand();
        if(item2.isEmpty()) {
            Message message = new Message("Выберите броню и возьмите во вторую руку: \n", ChatColor.COMBAT);
            MessageComponent mc = new MessageComponent(" [Шлем]", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " helmet");
            message.addComponent(mc);
            mc = new MessageComponent(" [Нагрудник]", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " chest");
            message.addComponent(mc);
            mc = new MessageComponent(" [Поножи]", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " leggings");
            message.addComponent(mc);
            mc = new MessageComponent(" [Ботинки]", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " boots");
            message.addComponent(mc);
            MessageComponent close = new MessageComponent(" [Закрыть]", ChatColor.GRAY);
            close.setClickCommand("/" + GiveMeArmor.NAME + " close");
            message.addComponent(close);
            Helpers.sendClickContainerToClient(message, "GiveMeArmor", getCommandSenderAsPlayer(sender));
            return;
        }

        WeaponTagsHandler weaponTagsHandler2 = new WeaponTagsHandler(item2);
        WeaponDurabilityHandler weaponDurabilityHandler2 = new WeaponDurabilityHandler(item2);
        WeightHandler weightHandler2 = new WeightHandler(item2);

        if(!weaponTagsHandler2.getWeaponTags().hasKey("Броня")) {
            Message message = new Message("Выберите броню и возьмите во вторую руку: \n", ChatColor.COMBAT);
            MessageComponent mc = new MessageComponent("[Шлем] ", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " helmet");
            message.addComponent(mc);
            mc = new MessageComponent("[Нагрудник] ", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " chest");
            message.addComponent(mc);
            mc = new MessageComponent("[Поножи] ", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " leggings");
            message.addComponent(mc);
            mc = new MessageComponent("[Ботинки] ", ChatColor.BLUE);
            mc.setClickCommand("/" + GiveMeArmor.NAME + " boots");
            message.addComponent(mc);
            MessageComponent close = new MessageComponent("[Закрыть]", ChatColor.GRAY);
            close.setClickCommand("/" + GiveMeArmor.NAME + " close");
            message.addComponent(close);
            Helpers.sendClickContainerToClient(message, "GiveMeArmor", getCommandSenderAsPlayer(sender));
            return;
        }

        NBTTagCompound armor1 = weaponTagsHandler.getWeaponTags().getCompoundTag("Броня").getCompoundTag("armor");
        NBTTagCompound armor2 = weaponTagsHandler2.getWeaponTags().getCompoundTag("Броня").getCompoundTag("armor");

        if (args.length != 0) {
            String segment = String.join(" ", Arrays.copyOfRange(args, 0, args.length));
            if(!segments.contains(segment) || !armor1.hasKey(segment) || armor2.hasKey(segment)) return;
            NBTTagCompound segcomp = armor1.getCompoundTag(segment);

            if(armor1.getKeySet().size() == 1 && armor2.getKeySet().isEmpty()) {
                ItemLoreHandler itemLoreHandler = new ItemLoreHandler(item);
                ItemLoreHandler itemLoreHandler2 = new ItemLoreHandler(item2);
                if(item.hasTagCompound() && item.getTagCompound().hasKey("display") && item.getTagCompound().getCompoundTag("display").hasKey("Lore")) {
                    itemLoreHandler2.setName(itemLoreHandler.getName());
                    itemLoreHandler2.getDisplayOrCreate().setTag("Lore", itemLoreHandler.getLoreOrCreate());
                    try {
                        itemLoreHandler.purgeLore();
                        itemLoreHandler.setName("Empty");
                    } catch (LoreException e) {
                        throw new RuntimeException(e);
                    }
                }
            } else if(armor2.getKeySet().isEmpty() && segcomp.hasKey("l")) {
                ItemLoreHandler itemLoreHandler2 = new ItemLoreHandler(item2);
                NBTTagList nbtTagList = segcomp.getTagList("l", 8);
                itemLoreHandler2.getDisplayOrCreate().setTag("Lore", nbtTagList);
                if (segcomp.hasKey("n")) {
                    itemLoreHandler2.setName(segcomp.getString("n"));
                    segcomp.removeTag("n");
                }
                segcomp.removeTag("l");
            } else if (armor1.getKeySet().size() == 1 && !armor2.getKeySet().isEmpty()) {
                ItemLoreHandler itemLoreHandler = new ItemLoreHandler(item);
                if(item.hasTagCompound() && item.getTagCompound().hasKey("display") && item.getTagCompound().getCompoundTag("display").hasKey("Lore")) {
                    NBTTagList nbtTagList = item.getTagCompound().getCompoundTag("display").getTagList("Lore", 8);
                    segcomp.setString("n", itemLoreHandler.getName());
                    segcomp.setTag("l", nbtTagList);
                    try {
                        itemLoreHandler.purgeLore();
                        itemLoreHandler.setName("Empty");
                    } catch (LoreException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            if (armor2.getKeySet().size() == 1) {
                for (String key : armor2.getKeySet()) {
                    NBTTagCompound segcomp1 = armor2.getCompoundTag(key);
                    ItemLoreHandler itemLoreHandler2 = new ItemLoreHandler(item2);
                    if(!segcomp1.hasKey("l") && item2.hasTagCompound() && item2.getTagCompound().hasKey("display") && item2.getTagCompound().getCompoundTag("display").hasKey("Lore")) {
                        NBTTagList nbtTagList = item2.getTagCompound().getCompoundTag("display").getTagList("Lore", 8);
                        segcomp1.setTag("l", nbtTagList);
                        try {
                            itemLoreHandler2.purgeLore();
                        } catch (LoreException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    if(!segcomp1.hasKey("n")) {
                        segcomp1.setString("n", itemLoreHandler2.getName());
                        itemLoreHandler2.setName("Объединенная броня");
                    }
                    break;
                }
            }

            armor2.setTag(segment, segcomp);
            armor1.removeTag(segment);

            if (armor1.getKeySet().size() == 1) {
                for (String key : armor1.getKeySet()) {
                    NBTTagCompound segcomp1 = armor1.getCompoundTag(key);
                    ItemLoreHandler itemLoreHandler = new ItemLoreHandler(item);
                    if(segcomp1.hasKey("l")) {
                        NBTTagList nbtTagList = segcomp1.getTagList("l", 8);
                        itemLoreHandler.getDisplayOrCreate().setTag("Lore", nbtTagList);
                        segcomp1.removeTag("l");
                    }
                    if(segcomp1.hasKey("n")) {
                        itemLoreHandler.setName(segcomp1.getString("n"));
                        segcomp1.removeTag("n");
                    }
                    break;
                }
            }


            int num1 = armor1.getKeySet().size();
            int num2 = armor2.getKeySet().size();

            double weight1 = 0;
            double dur1 = 0;
            double curdur1 = 0;

            double weight2 = 0;
            double dur2 = 0;
            double curdur2 = 0;

            for (String key : armor1.getKeySet()) {
                NBTTagCompound segment1 = armor1.getCompoundTag(key);
                weight1 += segment1.getDouble("w");
                dur1 += segment1.getInteger("d");
            }

            for (String key : armor2.getKeySet()) {
                NBTTagCompound segment2 = armor2.getCompoundTag(key);
                weight2 += segment2.getDouble("w");
                dur2 += segment2.getInteger("d");
            }

            if(num1 != 0) {
                dur1 = Math.ceil(dur1 / num1);
            } else {
                dur1 = 0;
            }

            if(num2 != 0) {
                dur2 = Math.ceil(dur2 / num2);
            } else {
                dur2 = 0;
            }

            double rat1 = (double) weaponDurabilityHandler.getDurability() / weaponDurabilityHandler.getMaxDurability();

            System.out.println(curdur2);
            System.out.println(num2);
            System.out.println(dur2);
            System.out.println(rat1);
            result = new TextComponentString(curdur2 + " " + num2 + " " + dur2 + " " + rat1);
            result.getStyle().setColor(TextFormatting.GRAY);
            sender.sendMessage(result);

            if(num2 > 0) {
                curdur2 = (weaponDurabilityHandler2.getDurability() * (num2 - 1) + dur2*rat1)/num2;
            } else {
                curdur2 = 0;
            }

            if(!item.getDisplayName().contains("§7[?]")) {
                item.setStackDisplayName(item.getDisplayName() + " §7[?]");
            }

            if(!item2.getDisplayName().contains("§7[?]")) {
                item2.setStackDisplayName(item2.getDisplayName() + " §7[?]");
            }

            weight1 = Math.ceil(weight1);
            weight2 = Math.ceil(weight2);
//            weaponDurabilityHandler.setDurability((int) Math.floor(dur1*rat1));
//            weaponDurabilityHandler2.setDurability((int) Math.floor(curdur2));
//            weaponDurabilityHandler.setMaxDurability((int)dur1);
//            weaponDurabilityHandler2.setMaxDurability((int)dur2);
            weightHandler.setWeight((int) weight1);
            weightHandler2.setWeight((int) weight2);
        }

        Message message = new Message("Выберите сегмент брони для трансфера: \n", ChatColor.COMBAT);
        for(String segment : segments) {
            if(!armor1.hasKey(segment) || armor2.hasKey(segment)) continue;
            MessageComponent mc = new MessageComponent("["+ segment +"] ", ChatColor.BLUE);
            mc.setClickCommand("/" + ArmorMergeCommand.NAME + " " + segment);
            message.addComponent(mc);
        }
        MessageComponent close = new MessageComponent("[Закрыть]", ChatColor.GRAY);
        close.setClickCommand("/" + ArmorMergeCommand.NAME + " close");
        message.addComponent(close);
        Helpers.sendClickContainerToClient(message, "ArmorMerge", getCommandSenderAsPlayer(sender));
    }
}
