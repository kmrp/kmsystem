package ru.konungstvo.combat.dice;

import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorSet;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;

import java.io.Serializable;

public class SkillDice extends FudgeDice implements Serializable {
    private String skillName;
    public static String regex = "(\\s[+,-][0-9][0-9]?[0-9]?%?)?(.*)?";

    // constructor for when we have no mod
    public SkillDice(String initial, String skill) {
        this(initial, skill, 0);
    }

    public SkillDice(String initial, String skill, int customMod) {
        this(initial, skill, customMod, 0, 0, 0, 0, 0, 0, 0, 0, new PercentDice());
    }


    public SkillDice(String initial, String skill, int customMod, int woundsMod, int armorMod, int coverMod, int shieldMod, int weightMod, int psychWeight, int eqBuff, int concentrationMod, PercentDice percentDice) {
        this(initial, skill,
                new ModificatorSet()
                        .add(new Modificator(customMod, ModificatorType.CUSTOM))
                        .add(new Modificator(woundsMod, ModificatorType.WOUNDS))
                        .add(new Modificator(armorMod, ModificatorType.ARMOR))
                        .add(new Modificator(coverMod, ModificatorType.COVER))
                        .add(new Modificator(shieldMod, ModificatorType.SHIELD))
                        .add(new Modificator(weightMod, ModificatorType.WEIGHT))
                        .add(new Modificator(psychWeight, ModificatorType.PSYCH_WEIGHT))
                        .add(new Modificator(eqBuff, ModificatorType.EQUIPMENT))
                        .add(new Modificator(concentrationMod, ModificatorType.CONCENTRATED))
        );
        setPercentDice(percentDice);


    }

    public SkillDice(String initial, String skill, ModificatorSet set) {
        super(initial, set);
        this.skillName = skill;

    }


    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    /* Why was this even a thing? Superclass method should do it
    public String getModAsString() {
        String mod = String.valueOf(getMod());
        if (getMod() == 0) return  "";
        if (getMod() > 0) return " +" + mod;
        return " " + mod;
    }
    */

    public String getSkillInfo() {
        return skillName +
                //getModAsString()
                getFinalModAsString()
                ;
    }

    @Override
    public String toString() {
        return "[" + getSkillName() + "] от " + getInitial() + " и результатом " + getResultAsString();
    }

    public static boolean matches(String playerName, String context) throws DataException {
        Player player = DataHolder.inst().getPlayer(playerName);
        String skillName = player.getSkillNameFromContext(context);
        return skillName != null;
    }

    @Override
    public boolean equals(Object obj) {
        SkillDice other = (SkillDice) obj;
        return this.getResult() == other.getResult();
    }


}
