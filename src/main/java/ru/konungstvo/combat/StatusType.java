package ru.konungstvo.combat;

import ru.konungstvo.chat.ChatColor;

public enum StatusType {
    STUNNED("оглушение", ChatColor.DARK_AQUA),
    BURNING("горение", ChatColor.YELLOW),
    WEAKENED("слабость", ChatColor.GRAY),
    CONCENTRATED("концентрация", ChatColor.DARK_GREEN),
    BLEEDING("кровотечение", ChatColor.DARK_RED),
    SHIELD("щит", ChatColor.BLUE),
    PREPARING("подготовка", ChatColor.GRAY),
    PREPARED("подготовлено", ChatColor.YELLOW),
    COOLDOWN("восстановление", ChatColor.GRAY),
    TRAIT_EFFECT("трейт", ChatColor.DARK_BLUE),
    MEDS("стимулятор", ChatColor.NICK),
    MEDS2("стимулятор2", ChatColor.DARK_GREEN),
    BUFF("усиление", ChatColor.DARK_GREEN),
    HANGOVER("отходняк", ChatColor.GRAY),
    GRENADE_PREPARED("заготовлена граната", ChatColor.RED),
    CUSTOM("кастом", ChatColor.GRAY);

    private String desc;
    private ChatColor color;
    StatusType(String desc, ChatColor color) {
        this.desc = desc;
        this.color = color;
    }

    @Override
    public String toString() {
        return desc;
    }
    public ChatColor color() {
        return color;
    }
}

