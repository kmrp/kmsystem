package ru.konungstvo.item;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import ru.konungstvo.bridge.Core;
import ru.konungstvo.item.queuehelper.NpcAdder;
import ru.konungstvo.item.queuehelper.QueueAdder;
import ru.konungstvo.item.queuehelper.QueueRemover;

@GameRegistry.ObjectHolder(Core.MODID)
@Mod.EventBusSubscriber
public class ModItems {
    public static final Item FIRST_ITEM = null;
    public static final Item QUEUE_ADDER = null;
    public static final Item QUEUE_REMOVER = null;
    public static final Item NPC_ADDER = null;


    @Mod.EventBusSubscriber(modid = Core.MODID)
    public static class RegistrationHandler {

        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            final Item[] items = {
                    new Item().setRegistryName(Core.MODID, "first_item").setUnlocalizedName(Core.MODID + "." + "first_item").setCreativeTab(Core.TOOLS_TAB),
                    new QueueAdder().setRegistryName(Core.MODID, "queue_adder").setUnlocalizedName(Core.MODID + "." + "queue_adder").setCreativeTab(Core.TOOLS_TAB),
                    new NpcAdder().setRegistryName(Core.MODID, "npc_adder").setUnlocalizedName(Core.MODID + "." + "npc_adder").setCreativeTab(Core.TOOLS_TAB),
                    new QueueRemover().setRegistryName(Core.MODID, "queue_remover").setUnlocalizedName(Core.MODID + "." + "queue_remover").setCreativeTab(Core.TOOLS_TAB)
            };

            event.getRegistry().registerAll(items);
        }
    }

    @SubscribeEvent
    public static void registerRenders(ModelRegistryEvent event) {

        System.out.println("SIGN VARIETY registerRenders() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

        ModelLoader.setCustomModelResourceLocation(QUEUE_ADDER, 0, new ModelResourceLocation(QUEUE_ADDER.getRegistryName(), "queue_adder"));
        ModelLoader.setCustomModelResourceLocation(QUEUE_REMOVER, 0, new ModelResourceLocation(QUEUE_REMOVER.getRegistryName(), "queue_remover"));
        ModelLoader.setCustomModelResourceLocation(NPC_ADDER, 0, new ModelResourceLocation(NPC_ADDER.getRegistryName(), "queue_remover"));
    }

}
