package ru.konungstvo.control;

import org.json.simple.JSONObject;

//TODO: handle non-existent config.json
public class Config {
    private String path;
    private Logger logger;
    public Config(String path) {
        this.path = path;
        this.logger = new Logger("Config");
    }

    public String readConfig(String name) {
        try {
            return (String) Helpers.readJson(path).get(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //Should it really be here? Do we actually need it? Dunno.
    public void writeConfig(String name, String value) {
        try {
            JSONObject jsonObject = Helpers.readJson(path);
            jsonObject.put(name, value);
            Helpers.writeJson(path, jsonObject.toJSONString());
            logger.debug("Putting " + value + " in " + name);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
