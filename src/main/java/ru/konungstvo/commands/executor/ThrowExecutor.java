package ru.konungstvo.commands.executor;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.client.render.RenderHelperer;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.network.MessageSound;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import noppes.npcs.api.NpcAPI;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.MovementTracker;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class ThrowExecutor extends CommandBase {


    @Override
    public String getName() {
        return "throw";
    }

    public static final String START_MESSAGE = "Наведитесь и используйте оружие: \n";
    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        int hand = 1;
        if (args.length > 0 && args[0].equals("0")) hand = 0;
        Player subordinate = player;
        if (player.getSubordinate() != null) subordinate = player.getSubordinate();
        System.out.println("test1");
        EntityLivingBase forgePlayer = player.getEntity();
        EntityLivingBase forgePlayerSub = subordinate.getEntity();
        WeaponTagsHandler weaponTagsHandler = null;
        Weapon weapon = null;
        ItemStack activeItem = null;
        if (hand == 1) activeItem = forgePlayerSub.getHeldItemMainhand();
        else activeItem = forgePlayerSub.getHeldItemOffhand();
        if (activeItem == null || activeItem.isEmpty()) return;
        weaponTagsHandler = new WeaponTagsHandler(activeItem);
        System.out.println("test2");
        if (!weaponTagsHandler.isWeapon()) {
            sender.sendMessage(new TextComponentString("Нет гранаты."));
            return;
        }
        if (!weaponTagsHandler.isFirearm()) {
            sender.sendMessage(new TextComponentString("Нет гранаты."));
            return;
        }
        if (!weaponTagsHandler.isGrenade()) {
            sender.sendMessage(new TextComponentString("Нет гранаты."));
            return;
        }
        if (activeItem.getDisplayName().contains("[?]")) {
            sender.sendMessage(new TextComponentString("Граната не подтверждена."));
            return;
        }
        if (activeItem.getDisplayName().contains("ФИТИЛЬ ИСПОРЧЕН")) {
            sender.sendMessage(new TextComponentString("Фитиль испорчен."));
            return;
        }
        if (activeItem.getCount() > 1) {
            sender.sendMessage(new TextComponentString("Возьмите не более одной гранаты в руку."));
            return;
        }
        Combat combat = DataHolder.inst().getCombatForPlayer(subordinate.getName());
        if (combat == null) {
            player.sendMessage("§4Нет боя.");
            return;
        }
        ClickContainerMessage remove = new ClickContainerMessage("ChooseSkill", true);
        KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));


        NBTTagCompound grenadestats = weaponTagsHandler.getGrenade();

        int step = grenadestats.getInteger("distance");
        int range = grenadestats.getInteger("range");
        int loss = grenadestats.getInteger("loss");
        int angle = grenadestats.getInteger("angle");
        boolean ricochet = grenadestats.getBoolean("ricochet");
        int maxrange = grenadestats.getInteger("maxrange");
        int offset = grenadestats.getInteger("offset");
        String skill = "восприятие";
        String type = "с ударником";
        String tempType = grenadestats.getString("type");
        switch (tempType) {
            case "с ударником":
                type = "с ударником";
                break;
            case "с запалом":
                type = "с запалом";
                break;
            case "непрямой":
            case "непрямой наводки":
                type = "непрямой наводки";
                break;
        }
        int damage = grenadestats.getInteger("damage");
        BoomType boomType = BoomType.DEFAULT;
        String boomString = grenadestats.getString("boomType");
        switch (boomString) {
            case "дефолт":
            case "о":
            case "осколочная":
                boomType = BoomType.DEFAULT;
                break;
            case "г":
            case "гор":
            case "горение":
            case "заж":
            case "зажигательная":
            case "зажигательный":
                boomType = BoomType.FIRE;
                break;
            case "газ":
            case "газовая":
            case "газовый":
                boomType = BoomType.GAS;
                break;
            case "сг":
            case "слабыйгаз":
            case "слабеющийгаз":
            case "слабый_газ":
            case "слабеющий_газ":
            case "газик":
            case "газок":
                boomType = BoomType.WEAK_GAS;
                break;
            case "сш":
            case "свето-шумовая":
            case "свето-шумовой":
                boomType = BoomType.STUNNING;
                break;
            case "дым":
            case "дымовая":
                boomType = BoomType.SMOKE;
                break;
        }
        boolean defendable = !grenadestats.hasKey("defendable") || grenadestats.getBoolean("defendable");
        boolean ignoresArmor = grenadestats.hasKey("ignoresarmor") && grenadestats.getBoolean("ignoresarmor");

        if (args.length < 2 || !(args[1].equals("ready") || args[1].equals("prepare")  || args[1].equals("cancel"))) {
            Message message = new Message("");
            message.addComponent(new MessageComponent(START_MESSAGE));
            MessageComponent thr = new MessageComponent("[" + weaponTagsHandler.getDefaultWeaponName() + "] ", ChatColor.COMBAT);
            thr.setClickCommand("/throw " + hand + " ready");
            thr.setHoverText("Отправить снаряд в полёт!", TextFormatting.RED);
            if (subordinate.preparedGrenade(weaponTagsHandler.getDefaultWeaponName()) && activeItem.getDisplayName().contains("ЗАПАЛ АКТИВИРОВАН")) {
                thr.setColor(ChatColor.RED);
                thr.setHoverText("Снаряд подготовлен к взрыву! Не взорвись!", TextFormatting.RED);
            }
            MessageComponent prep = null;
            if (type.equals("с запалом")) {
                if (!subordinate.preparedGrenade(weaponTagsHandler.getDefaultWeaponName()) || !activeItem.getDisplayName().contains("ЗАПАЛ АКТИВИРОВАН")) {
                    prep = new MessageComponent("[" + weaponTagsHandler.getDefaultWeaponName() + " (заготовить)] ", ChatColor.COMBAT);
                    prep.setClickCommand("/throw " + hand + " prepare");
                    prep.setHoverText("Заготовить снаряд для броска!\nПри броске бомба взорвётся сразу даже на ближней дистанции.\nНе является концентрацией.\nЕсли не бросить бомбу в следующий ход, она взорвется в руке.", TextFormatting.RED);
                } else {
                    prep = new MessageComponent("[" + weaponTagsHandler.getDefaultWeaponName() + " (отменить)] ", ChatColor.COMBAT);
                    prep.setClickCommand("/throw " + hand + " cancel");
                    prep.setHoverText("Отменить взрыв снаряда, если конструкция позволяет.\nНапример, можно задуть фитиль.\nСнаряд при этом будет испорчен.", TextFormatting.RED);
                }
            }

            MessageComponent mod = new MessageComponent("[Мод] ", ChatColor.GRAY);
            mod.setClickCommand("/modifiersbutton Throw");
            MessageComponent back = new MessageComponent("[Назад] ", ChatColor.GRAY);
            back.setClickCommand("/toroot Throw");
            message.addComponent(thr);
            if (type.equals("с запалом")) message.addComponent(prep);
            message.addComponent(mod);
            message.addComponent(back);
            Helpers.sendClickContainerToClient(message, "Throw", getCommandSenderAsPlayer(sender), player);
            return;
        }

        ClickContainerMessage rem = new ClickContainerMessage("Throw", true);
        KMPacketHandler.INSTANCE.sendTo(rem, getCommandSenderAsPlayer(sender));

        if (args[1].equals("prepare")) {
            Message resultt = new Message(subordinate.getName() + " подготавливает " + weaponTagsHandler.getDefaultWeaponName() + " к броску!", ChatColor.GMCHAT);
            ServerProxy.sendMessageFromAndInformMasters(subordinate, resultt);
            Boom boom = new Boom(subordinate.getEntity().getPosition(), boomType, damage, DataHolder.inst().getCombatForPlayer(player.getName()).getId(), ServerProxy.getForgePlayer(sender.getName()).dimension, defendable, ignoresArmor);
            boom.deactivate();
            combat.addBoom(boom);
            activeItem.setStackDisplayName(activeItem.getDisplayName() + " ЗАПАЛ АКТИВИРОВАН");
            subordinate.addStatusEffect(weaponTagsHandler.getDefaultWeaponName(), StatusEnd.TURN_END, 1, StatusType.GRENADE_PREPARED, weaponTagsHandler.getDefaultWeaponName(), boom.getUuid(), null,subordinate.getCombatState() == CombatState.SHOULD_ACT);
            if (subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/next");
            return;
        }

        if (args[1].equals("cancel")) {
            Message resultt = new Message(subordinate.getName() + " предотвращает взрыв " + weaponTagsHandler.getDefaultWeaponName(), ChatColor.GMCHAT);
            ServerProxy.sendMessageFromAndInformMasters(subordinate, resultt);
            activeItem.setStackDisplayName(activeItem.getDisplayName().replaceAll(" ЗАПАЛ АКТИВИРОВАН", "") + " ФИТИЛЬ ИСПОРЧЕН");
            subordinate.removePreparedGrenade(weaponTagsHandler.getDefaultWeaponName());
            if (subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/next");
            return;
        }

        EntityPlayerMP playerMP = ServerProxy.getForgePlayer(subordinate.getName());
        GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_FLY, 4F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 16F));        try {
            SkillSaverHandler skillSaver;
            if (hand == 0) skillSaver = new SkillSaverHandler(forgePlayerSub.getHeldItemOffhand());
            else skillSaver = new SkillSaverHandler(forgePlayerSub.getHeldItemMainhand());
            skill = skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString());
        } catch (Exception ignored) {

        }
        int distance = ServerProxy.getDistance(player.getEntity(), 0, 0, range, loss, angle, ricochet, maxrange, (hand == 1 ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND), type.equals("непрямой наводки"));

        if (type.equals("с запалом") && subordinate.preparedGrenade(weaponTagsHandler.getDefaultWeaponName()) && activeItem.getDisplayName().contains("ЗАПАЛ АКТИВИРОВАН")) {
            type = "с ударником";
            subordinate.removePreparedGrenade(weaponTagsHandler.getDefaultWeaponName());
        }

        SkillDiceMessage sdm = new SkillDiceMessage(subordinate.getName(), "% " + skill);
        if (subordinate.grenadier && subordinate.grenadesUsed < 2) {
            subordinate.grenadesUsed++;
            sdm.getDice().addAdvantage("Гренадёр (талант)");
        }
        sdm.build();
        ServerProxy.sendMessageFromAndInformMasters(subordinate, sdm);
        int diff = distance / step;
        if (diff > 7) diff = 6;
        else diff--; //от ужасно считаем
        Message result = new Message("", ChatColor.GMCHAT);
        double pitchOffset = 0;
        double yawOffset = 0;
        if (diff >= sdm.getDice().getResult()) {
            Random random = new Random();
            int randomResult = random.nextInt(8) + 1;
            switch (randomResult) {
                case 1:
                    pitchOffset += offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 2:
                    pitchOffset -= offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 3:
                    yawOffset += offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 4:
                    yawOffset -= offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 5:
                    pitchOffset += offset * (diff - sdm.getDice().getResult() + 1);
                    yawOffset += offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 6:
                    pitchOffset += offset * (diff - sdm.getDice().getResult() + 1);
                    yawOffset -= offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 7:
                    pitchOffset -= offset * (diff - sdm.getDice().getResult() + 1);
                    yawOffset += offset * (diff - sdm.getDice().getResult() + 1);
                    break;
                case 8:
                    pitchOffset -= offset * (diff - sdm.getDice().getResult() + 1);
                    yawOffset -= offset * (diff - sdm.getDice().getResult() + 1);
                    break;
            }

            MessageComponent tcs = new MessageComponent("Сложность была " + DataHolder.inst().getSkillTable().get(diff) + " (" + distance +
                    "), (d8 > " + randomResult + ") происходит " + (yawOffset != 0 ? ("сдвиг на " + yawOffset + " по горизонтали ") : "") + (yawOffset != 0 && pitchOffset != 0 ? "и ":"") + (pitchOffset != 0 ? ("сдвиг на " + pitchOffset + " по вертикали") : ""), ChatColor.GMCHAT);
            result.addComponent(tcs);
        } else {
            MessageComponent tcs = new MessageComponent("Сложность была " + DataHolder.inst().getSkillTable().get(diff) + " (" + distance +
                    "), в яблочко!", ChatColor.GMCHAT);
            result.addComponent(tcs);
        }
        ServerProxy.sendMessageFromAndInformMasters(subordinate, result);
        BlockPos bp = ServerProxy.getBlockPos(player.getEntity(), pitchOffset, yawOffset, range, loss, angle, ricochet, maxrange, (hand == 1 ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND), type.equals("непрямой наводки"));
        if (bp == null) {
            result = new Message("Улетело вникуда...", ChatColor.RED);
            ServerProxy.sendMessageFromAndInformMasters(subordinate, result);
            return;
        }
        Boom boom = new Boom(bp, boomType, damage, DataHolder.inst().getCombatForPlayer(player.getName()).getId(), ServerProxy.getForgePlayer(sender.getName()).dimension, defendable, ignoresArmor);
        combat.addBoom(boom);
        int secDistance = 0;
        if ((type.equals("с запалом"))) secDistance = ServerProxy.getDistance(player.getEntity(), pitchOffset, yawOffset, range, loss, angle, ricochet, maxrange, (hand == 1 ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND), type.equals("непрямой наводки"));
        boolean instaboom = (type.equals("с ударником")) || ((type.equals("с запалом")) && secDistance / step > 3);
        if (subordinate.getCombatState() != CombatState.SHOULD_ACT && instaboom) boom.setNextWhenDone(false);
        else boom.setNextWhenDone(true);
        activeItem.setCount(0);
        if (instaboom) boom.findTargetsAndStartExplosion();
        else {
            if (!type.equals("непрямой наводки")) {
                boom.setToBeBoomIn(combat.getReactionList().getReactions().size());
                boom.spawnNpc();
            } else {
                boom.setToBeRevealed(true);
                boom.spawnNpc();
            }
            Message resultt = new Message(subordinate.getName() + " совершает бросок " + weaponTagsHandler.getDefaultWeaponName() + ", взрыв будет не сразу.", ChatColor.GMCHAT);
            ServerProxy.sendMessageFromAndInformMasters(subordinate, resultt);
            if (subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/next");
        }
    }
}