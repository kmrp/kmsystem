package ru.konungstvo.chat.message;

import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FudgeDiceMessage extends RoleplayMessage {
    private FudgeDice fudgeDice;
    private String comment = "";

    public FudgeDiceMessage(String playerName, String content) {
        this(playerName, content, Range.NORMAL_DIE);
    }

    public FudgeDiceMessage(String playerName, String content, Range range) {
        super(playerName, content, range);

        if (this instanceof SkillDiceMessage) return; // TODO this feels wrong. Maybe refactor?

        Player player = DataHolder.inst().getPlayer(playerName);
        if (player == null) throw new DataException("Игрок " + playerName + " не найден!");
        player.resetPercentDice();
        int woundsPercentMod = player.getWoundsMod();
        System.out.println("WoundsPercentMod is " + woundsPercentMod);
        int woundsMod = 0;
        int cutPart = woundsPercentMod % 100;
        System.out.println("cutPart is " + -cutPart);
        player.getPercentDice().setWound(-cutPart);
        if (woundsPercentMod > 100) {
            woundsMod = -(woundsPercentMod / 100);
        }

        player.getPercentDice().setCustom(0);
        if (player.getModifiersState().getModifier("percent") != null) {
            player.getPercentDice().setCustom(player.getModifiersState().getModifier("percent"));
            System.out.println("Custom percent set to " + player.getPercentDice().getCustom());
        }

        System.out.println("FudgeDiceMessage: " + "woundsMod=" + woundsMod);

        int bloodloss = player.getBloodloss();
        player.getPercentDice().setBloodloss(-bloodloss);
        System.out.println("FudgeDiceMessage: " + "bloodloss=" + -bloodloss);

        int mentalDebuff = player.getMentalDebuff();
        player.getPercentDice().setMentalDebuff(-mentalDebuff);
        System.out.println("FudgeDiceMessage: " + "mentalDebuff=" + -mentalDebuff);
        //parse message to extract needed values
        Pattern pattern = Pattern.compile(FudgeDice.regex);
        Matcher matcher = pattern.matcher(content.toLowerCase());
        if (!matcher.find()) throw new IllegalStateException("Неверно введённый 4dF дайс!");
        String initial = matcher.group(1);
        int mod = 0;
        if (matcher.group(2) != null && !matcher.group(2).equals("")) {
            String modifier = matcher.group(2).substring(1);
            if(modifier.contains("%")) {
                System.out.println("TRUE");
                int percentMod = Integer.parseInt(modifier.replace("%","").trim());
                player.getPercentDice().setCustom(percentMod);
            } else {
                mod = Integer.parseInt(modifier.substring(1));
                if(mod < -9) mod = -9;
                if(mod > 9) mod = 9;
            }
        }
        player.getPercentDice().setSkill(0);
        FudgeDice fudgeDice = new FudgeDice(initial, mod, woundsMod, 0, 0, player.getPercentDice());
        if (player.getCustomAdvantage() != 0) {
            if (player.getCustomAdvantage() > 0) fudgeDice.addAdvantage("Кастом");
            else fudgeDice.addDisadvantage("Кастом");
        }
        this.fudgeDice = fudgeDice;

        String comment = "";
        if (matcher.group(3) != null && !matcher.group(3).equals("")) {
            comment = matcher.group(3).substring(1).replaceAll("§.", "").replaceAll("&([a-z0-9])", "§$1");
        }
        this.comment = comment;
    }

    @Override
    public void build() {
        //if (isBuilt()) return;
        if (fudgeDice != null) {

            fudgeDice.cast();
            // get info about mod if needed
            String modInfoString;
            //modInfoString = " [" + fudgeDice.getInitial() + fudgeDice.getModAsString() + "]";
            modInfoString = " [" + fudgeDice.getInitial() + fudgeDice.getFinalModAsString() + "]";
            if (modInfoString.equals(" [" + fudgeDice.getInitial() + "]")) modInfoString = "";
            if (!comment.equals("")) comment = " (" + comment.replaceAll("&([a-z0-9])", "§$1") + "§e)";

            this.addInput("info", fudgeDice.getInfo());
            this.addInput("baseLevel", fudgeDice.getBaseAsString());
            this.addInput("modInfo", modInfoString);
            this.addInput("result", fudgeDice.getResultAsString());
            this.addInput("comment", comment);

            //if (getDice().wasChangedByPercentDice()) {
            int percentDice = getDice().getPercentDice().get();
            String percentDiceStr = String.valueOf(percentDice);
            percentDiceStr += "%";
            String dbuff = "Бафф: ";
            if (percentDice < 0) dbuff = "Дебафф: ";
            String debug = "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
                    " " + (Math.abs(percentDice) >= getDice().getPersentResult() && percentDice != 0 ? (percentDice < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) : "") + "Бросок: " + getDice().getPersentResult() + "/100";
            debug += "\n" + getDice().getPercentDice().toString();
            this.addInput("debug", debug);

            // }
        } else {
            System.out.println("fudgeDice seems to appear null");
        }
        super.build();
    }

    public void rebuild() {
        unbuild();
        this.build();
    }

    protected String getPercentInfo() {
        int percentDice = getDice().getPercentDice().get();
        if (percentDice >= 100) percentDice = percentDice % 100;
        if (percentDice <= -100) percentDice = percentDice % 100;
        String percentDiceStr = String.valueOf(percentDice);
        percentDiceStr += "%";

        String dbuff = "Бафф: ";
        if (percentDice < 0) dbuff = "Дебафф: ";
        return "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
                " " + (Math.abs(percentDice) >= getDice().getPersentResult() && percentDice != 0 ? (percentDice < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) : "") + "Бросок: " + getDice().getPersentResult() + "/100" + TextFormatting.GRAY
                + "\n" + getDice().getPercentDice().toString()
        ;
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        template.add(new MessageComponent("(( ", ChatColor.DICE));
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));

        if (range.toString().contains("GM")) {
            template.add(new MessageComponent(" (to GM)"));
            template.add(new MessageComponent(" бросает 4dF ", ChatColor.DICE));
            //template.add(new MessageComponent(" бросает 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))", ChatColor.DICE));
        } else if (range.toString().contains("CM")) {
            template.add(new MessageComponent(" (to CM)", ChatColor.CMCHAT));
            template.add(new MessageComponent(" бросает 4dF ", ChatColor.DICE));
            // {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))", ChatColor.DICE));

        } else {
            //template.add(new MessageComponent(" {rangeDescription} 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" {rangeDescription} 4dF ", ChatColor.DICE));
        }
        MessageComponent percentage = new MessageComponent("{info}", ChatColor.DICE);
        percentage.setHoverText((getPercentInfo() + "\n" + getDice().getAdvDisadvString()).trim(), TextFormatting.GOLD);
        template.add(percentage);

        //template.add(new MessageComponent(" от {baseLevel}{modInfo}{comment}. Результат: {result} ))", ChatColor.DICE));
        template.add(new MessageComponent(" от {baseLevel}", ChatColor.DICE));
        MessageComponent modInfo = new MessageComponent("{modInfo}", ChatColor.DICE);
        String modString = fudgeDice.getModAsString();
        if (!modString.isEmpty()) modInfo.setHoverText(modString, TextFormatting.GRAY);
        template.add(modInfo);
//        template.add()"{modInfo}";
        template.add(new MessageComponent("{comment}", ChatColor.DICE));
        template.add(new MessageComponent(". Результат: {result} ))", ChatColor.DICE));



        return template;
    }

    /*
    @Override
    protected void generateTemplate() {
        String template;
        if (range.toString().contains("GM")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + " §f(to GM)" +
                    ChatColor.DICE.get() + " бросает 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))";
        } else if (range.toString().contains("BD")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + ChatColor.BDCHAT.get() + " (to BD)" +
                    ChatColor.DICE.get() + " бросает 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))";
        } else {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))";
        }
      //  this.setTemplate(template);
    }

     */

    public FudgeDice getDice() {
        return fudgeDice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
