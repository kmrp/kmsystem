package ru.konungstvo.commands.executor;

import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

@Deprecated
public class PromoteExecutor {
    public static String execute(Player executor, String[] args) {
        if (args.length == 0 || args[0].equals("help")) return "Тут будет хелп"; //TODO
        if (args.length == 1) return "§8Кого вы хотите повысить? Укажите ник, например: /promote gm NickHere";
        switch (args[0]) {
            case "gm":
                DataHolder.inst().getPlayer(args[1]).addPermission(Permission.GM);
//                DataHolder.getInstance().promoteGameMaster(args[1]);
                return "§8" + args[1] + " теперь GameMaster!";
            case "builder":
                DataHolder.inst().getPlayer(args[1]).addPermission(Permission.BD);
//                DataHolder.getInstance().promoteBuilder(args[1]);
                return "§8" + args[1] + " теперь Builder!";
        }
        return "§8Ничего не произошло. Убедитесь, что вы правильно ввели команду.";
    }
}
