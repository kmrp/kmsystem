package ru.konungstvo.commands.helpercommands.player.defense;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Equipment;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.equipment.Trash;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

public class SelectDefenseItem extends CommandBase {
    public static final String NAME = "selectdefenseitem";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    // Для парирования и блокирования: выбираем предмет. Берём автоматически, если он один. Даём игроку выбор, если их два.
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player defender = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (defender.getSubordinate() != null) {
            defender = defender.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(defender.getName());
            if (!DataHolder.inst().isNpc(defender.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(defender.getName());
            }
        }

        Duel duel = DataHolder.inst().getDuelForDefender(defender.getName());

        // DEFENSE TYPE!
        String typeStr = args[0];

        ItemStack leftHand;
        ItemStack rightHand;
        try {
            leftHand = forgePlayer.getHeldItemOffhand();
            rightHand = forgePlayer.getHeldItemMainhand();
        } catch (NullPointerException e) {
            e.printStackTrace();
            player.sendError(new Exception("Нельзя использовать " + typeStr + " с пустыми руками"));
            player.performCommand("/" + ToRoot.NAME + " SelectDefense");
            return;
        }

        if (leftHand.isEmpty() && rightHand.isEmpty()) {
            player.sendError(new Exception("Нельзя использовать " + typeStr + " с пустыми руками"));
            player.performCommand(ToRoot.NAME + " SelectDefense");
            return;
        }

        if (typeStr.equals(DefenseType.PARRY.name())) {
            boolean parry = true;
            WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
            WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);
            Weapon weapon;

            // ЕСЛИ ОРУЖИЕ В ЛЕВОЙ РУКЕ
            if (!leftHand.isEmpty() && rightHand.isEmpty()) {

                if (leftWeapon.isWeapon()) {
                    weapon = new Weapon(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());

                } else {
                    weapon = new Trash(leftWeapon.getDefaultWeaponName());
                }

                if (parry && duel.getActiveAttack().getWeapon().isRanged()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет парировать эту дальнюю атаку."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }

                duel.getDefense().setDefenseType(DefenseType.PARRY);
                duel.getDefense().setActiveHand(0);
                duel.getDefense().setDefenseItem(weapon);
                player.performCommand("/" + ChooseDefenseSkill.NAME + " " + duel.getDefense().getDefenseType().toString());

                // ЕСЛИ ОРУЖИЕ В ПРАВОЙ РУКЕ
            } else if (leftHand.isEmpty() && !rightHand.isEmpty()) {
                if (rightWeapon.isWeapon()) {
                    weapon = new Weapon(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());
                } else {
                    weapon = new Trash(rightWeapon.getDefaultWeaponName());
                }
                System.out.println("Created WEAPON! " + weapon.toString());

                if (parry && duel.getActiveAttack().getWeapon().isRanged()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет парировать эту дальнюю атаку."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }
                duel.getDefense().setDefenseType(DefenseType.PARRY);
                duel.getDefense().setActiveHand(1);
                duel.getDefense().setDefenseItem(weapon);
                player.performCommand("/" + ChooseDefenseSkill.NAME + " " + duel.getDefense().getDefenseType().toString());

            } else {
                // give choice
                // GENERATE CLICK PANEL
                Message message = new Message("Выберите оружие для защиты: ", ChatColor.COMBAT);

                MessageComponent mes = new MessageComponent("[Левая рука] ", ChatColor.BLUE);
                mes.setClickCommand("/" + ActiveHandDefense.NAME + " 0 " + typeStr);
                message.addComponent(mes);

                mes = new MessageComponent("[Правая рука] ", ChatColor.BLUE);
                mes.setClickCommand("/" + ActiveHandDefense.NAME + " 1 " + typeStr);
                message.addComponent(mes);

                // SEND PACKET TO CLIENT
                {
                    Helpers.sendClickContainerToClient(message, "ActiveHandDefense", (EntityPlayerMP) sender, defender);
                }


            }


        } else if (typeStr.equals(DefenseType.BLOCKING.name())) {


            WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
            WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);

            if (!leftWeapon.isShield() && !rightWeapon.isShield()) {
                player.sendError(new Exception("Нельзя использовать " + typeStr + " без щита"));
                player.performCommand(ToRoot.NAME + " SelectDefense");
                return;
            }
            duel.getDefense().setDefenseType(DefenseType.BLOCKING);
            Equipment equipment;

            // ЕСЛИ ЩИТ В ЛЕВОЙ РУКЕ
            if (!leftHand.isEmpty() && leftWeapon.isShield() && !rightWeapon.isShield()) {
                equipment = new Shield(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());
                /*
                try {
                    equipment.fillFromNBT(leftWeapon.getDefaultWeapon());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                 */


                System.out.println("Equip: " + equipment.toString());
                duel.getDefense().setActiveHand(0);
                duel.getDefense().setDefenseItem(equipment);
                player.performCommand("/" + ChooseDefenseSkill.NAME + " " + duel.getDefense().getDefenseType().toString());

                // ЕСЛИ ЩИТ В ПРАВОЙ РУКЕ
            } else if (!leftWeapon.isShield() && !rightHand.isEmpty() && rightWeapon.isShield()) {
                equipment = new Shield(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());
                /*
                try {
                    equipment.fillFromNBT(rightWeapon.getDefaultWeapon());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                 */
                duel.getDefense().setDefenseItem(equipment);
                duel.getDefense().setActiveHand(1);
                System.out.println("Equip: " + equipment.toString());

                player.performCommand("/" + ChooseDefenseSkill.NAME + " " + duel.getDefense().getDefenseType().toString());

            } else {
                // give choice
                // GENERATE CLICK PANEL
                Message message = new Message("Выберите щит для защиты: ", ChatColor.COMBAT);

                MessageComponent mes = new MessageComponent("[Левая рука] ", ChatColor.BLUE);
                mes.setClickCommand("/activehanddefense 0 " + typeStr);
                message.addComponent(mes);

                mes = new MessageComponent("[Правая рука] ", ChatColor.BLUE);
                mes.setClickCommand("/activehanddefense 1 " + typeStr);
                message.addComponent(mes);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "ActiveHandDefense", getCommandSenderAsPlayer(sender), defender);

            }

        } else if (typeStr.equals(DefenseType.COUNTER.name())) {
            duel.getDefense().setDefenseType(DefenseType.COUNTER);

            WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
            WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);
            Weapon weapon;

            // ЕСЛИ ОРУЖИЕ В ЛЕВОЙ РУКЕ
            if (!leftHand.isEmpty() && rightHand.isEmpty()) {

                if (leftWeapon.isWeapon()) {
                    weapon = new Weapon(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());

                } else {
                    weapon = new Trash(leftWeapon.getDefaultWeaponName());
                }

                if (false && weapon.isRanged()) {
                    player.sendError(new Exception("Нельзя контратаковать дальним оружием."));
                    player.performCommand(ToRoot.NAME + " SelectDefense");
                    return;
                }

                if (duel.getDefense().getDefenseType() == DefenseType.COUNTER && duel.getActiveAttack().getWeapon().isRanged() && weapon.isMelee()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать эту дальнюю атаку."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }

                if (duel.getDefense().getDefenseType() == DefenseType.COUNTER) {// redundant if clause?
                    if (duel.getActiveAttack().getWeapon().isMelee() && weapon.isMelee() && weapon.getReach() < duel.getActiveAttack().getWeapon().getReach()) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }

                if (!DataHolder.inst().eligibleForCounter(defender)) {
                    System.out.println(weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach()));
                    System.out.println(weapon.isMelee());
                    System.out.println(duel.getActiveAttack().getWeapon().isRanged());
                    System.out.println(weapon.isFist());
                    if (duel.getActiveAttack().getWeapon().isMelee()) System.out.println(weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach());
                    if (weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach())) {

                    } else {
                        player.sendError(new Exception("Контратака недоступна."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }

                duel.getDefense().setActiveHand(0);
                duel.getDefense().setDefenseItem(weapon);
                player.performCommand("/" + ChooseDefenseSkill.NAME + " " + duel.getDefense().getDefenseType().toString());

                // ЕСЛИ ОРУЖИЕ В ПРАВОЙ РУКЕ
            } else if (leftHand.isEmpty() && !rightHand.isEmpty()) {
                if (rightWeapon.isWeapon()) {
                    weapon = new Weapon(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());
                } else {
                    weapon = new Trash(rightWeapon.getDefaultWeaponName());
                }
                System.out.println("Created WEAPON! " + weapon.toString());

                if (false && weapon.isRanged()) {
                    player.sendError(new Exception("Нельзя контратаковать дальним оружием."));
                    player.performCommand(ToRoot.NAME + " SelectDefense");
                    return;
                }

                if (duel.getDefense().getDefenseType() == DefenseType.COUNTER && !duel.getActiveAttack().getWeapon().isRanged()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать эту дальнюю атаку."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }


                    if (duel.getActiveAttack().getWeapon().isMelee() && weapon.getReach() < duel.getActiveAttack().getWeapon().getReach()) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }

                if (!DataHolder.inst().eligibleForCounter(defender)) {
                    System.out.println(weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach()));
                    System.out.println(weapon.isMelee());
                    System.out.println(duel.getActiveAttack().getWeapon().isRanged());
                    System.out.println(weapon.isFist());
                    if (duel.getActiveAttack().getWeapon().isMelee()) System.out.println(weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach());
                    if (weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach())) {

                    } else {
                        player.sendError(new Exception("Контратака недоступна."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }


                    duel.getDefense().setActiveHand(1);
                    duel.getDefense().setDefenseItem(weapon);
                    player.performCommand("/" + ChooseDefenseSkill.NAME + " " + duel.getDefense().getDefenseType().toString());

                } else {
                    // give choice
                    // GENERATE CLICK PANEL
                    Message message = new Message("Выберите оружие для контратаки: ", ChatColor.COMBAT);

                    MessageComponent mes = new MessageComponent("[Левая рука] ", ChatColor.BLUE);
                    mes.setClickCommand("/" + ActiveHandDefense.NAME + " 0 " + typeStr);
                    message.addComponent(mes);

                    mes = new MessageComponent("[Правая рука] ", ChatColor.BLUE);
                    mes.setClickCommand("/" + ActiveHandDefense.NAME + " 1 " + typeStr);
                    message.addComponent(mes);

                    // SEND PACKET TO CLIENT
                    {
                        Helpers.sendClickContainerToClient(message, "ActiveHandDefense", (EntityPlayerMP) sender, defender);

                    }


                }

        }
    }
}
