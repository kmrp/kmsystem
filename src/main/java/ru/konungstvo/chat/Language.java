package ru.konungstvo.chat;

import java.util.Arrays;
import java.util.List;

import static ru.konungstvo.chat.message.LanguageMessage.*;


public enum Language {

    HINTE( "Хин-те", 'h', "§4§lか§r", hinte),
    THAL( "Таль", 't',  "§9§lᛃ§r", thal),
    ROBO( "Таль", 't',  "§1§læ§r", robo);

    private String name;
    private char ch;
    private String sign;
    private List<String> bits;

    Language(String name, char ch, String sign, List<String> bits) {
        this.name = name;
        this.ch = ch;
        this.sign = sign;
        this.bits = bits;
    }

    public static Language getLanguageByCh(char letter) {
        for (Language language : Language.values()) {
            if (language.ch == letter) return language;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public String getSign() {
        return sign;
    }

    public List<String> getBits() {
        return bits;
    }
}
