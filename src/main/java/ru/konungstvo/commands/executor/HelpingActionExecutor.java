package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class HelpingActionExecutor extends CommandBase {
    private static String START_MESSAGE = "Выберите %s!\n";
    public static final String NAME = "helpingaction";

    @Override
    public String getName() {
        return "helpingaction";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        if(args.length > 0) oldContainer = args[0];
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);

            if (!DataHolder.inst().isNpc(subordinate.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
            } else {
                forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(subordinate.getName());
            }

        System.out.println(String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
        // REMOVE PREVIOUS PANEL
        switch (oldContainer) {
            case "extinguish": {
                if(args.length > 1) oldContainer = args[1];
                else oldContainer = "";
                // CREATE CLICK PANEL
                Message message = new Message("");
                MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                //close.setBold(true);
                close.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + oldContainer);
                MessageComponent start = new MessageComponent("Выберите кого потушить:\n", ChatColor.COMBAT);
                message.addComponent(start);
                List<Player> targets = new ArrayList<>();
                targets = ServerProxy.getAllFightersInRange(subordinate, 2);
                boolean found = false;
                for (Player friend : targets) {
                    if (!friend.hasStatusEffect(StatusType.BURNING)) continue;
                    found = true;
                    //if (opponent.getName().equals(sender.getName())) continue;
                    MessageComponent oppComponent = new MessageComponent("[" + friend.getName() + "] ", ChatColor.BLUE);
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/helpingaction extinguishtarget %s %s", friend.getName(), oldContainer)
                    );
                    message.addComponent(oppComponent);
                }
                if (!found) {
                    player.sendMessage(new Message("Нет горящих целей поблизости.", ChatColor.RED));
                    return;
                }
                message.addComponent(close);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "HelpingAction", getCommandSenderAsPlayer(sender), subordinate);
                return;
            }
            case "extinguishtarget": {
                Player friend = DataHolder.inst().getPlayer(args[1]);
                if (ServerProxy.getRoundedDistanceBetween(subordinate, friend) > 2) {
                    player.sendMessage(new Message("Слишком далеко.", ChatColor.RED));
                    player.performCommand("/helpingaction " + (args.length > 2 ? args[2] : ""));
                    return;
                }

                int turns = 0;
                for (StatusEffect statusEffect : friend.getStatusEffects()) {
                    if (statusEffect.getStatusType() != StatusType.BURNING) continue;
                    if (statusEffect.getTurnsLeft() > turns) {
                        turns = statusEffect.getTurnsLeft();
                    }
                }
                turns -= 3;
                friend.removeStatusEffect(StatusType.BURNING);
                ItemStack is2 = subordinate.getItemForHand(0);
                ItemStack is1 = subordinate.getItemForHand(1);
                if (!is1.isEmpty() && is1.hasTagCompound() && is1.getTagCompound() != null && is1.getTagCompound().hasKey("medical") &&
                        is1.getTagCompound().getString("medical").equals("extinguisher")) {
                    is1.setCount(is1.getCount() - 1);
                    turns = 0;
                } else if (!is2.isEmpty() && is2.hasTagCompound() && is2.getTagCompound() != null && is2.getTagCompound().hasKey("medical") &&
                        is2.getTagCompound().getString("medical").equals("extinguisher")) {
                    is2.setCount(is2.getCount() - 1);
                    turns = 0;
                }
                if (turns > 0) {
                    friend.addStatusEffect("горение", StatusEnd.TURN_END, turns, StatusType.BURNING);
                }


                ClickContainerMessage remove = new ClickContainerMessage("HelpingAction", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + (turns > 0 ? " частично" : "") + " тушит " + args[1] + "!", ChatColor.DARK_GREEN));
                if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/" + NextExecutor.NAME + " combat");
                else {
                    player.performCommand("/toolpanel");
                }
                return;
            }
            case "getup": {
                if(args.length > 1) oldContainer = args[1];
                else oldContainer = "";
                // CREATE CLICK PANEL
                Message message = new Message("");
                MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                //close.setBold(true);
                close.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + oldContainer);
                MessageComponent start = new MessageComponent("Выберите кого поднять:\n", ChatColor.COMBAT);
                message.addComponent(start);
                List<Player> targets = new ArrayList<>();
                targets = ServerProxy.getAllFightersInRange(subordinate, 2);
                boolean found = false;
                for (Player friend : targets) {
                    if (!friend.isLying()) continue;
                    found = true;
                    //if (opponent.getName().equals(sender.getName())) continue;
                    MessageComponent oppComponent = new MessageComponent("[" + friend.getName() + "] ", ChatColor.BLUE);
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/helpingaction getuptarget %s", friend.getName())
                    );
                    message.addComponent(oppComponent);
                }
                if (!found) {
                    player.sendMessage(new Message("Нет лежащих целей поблизости.", ChatColor.RED));
                    return;
                }
                message.addComponent(close);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "HelpingAction", getCommandSenderAsPlayer(sender), subordinate);
                return;
            }
            case "getuptarget": {
                Player friend = DataHolder.inst().getPlayer(args[1]);
                if (ServerProxy.getRoundedDistanceBetween(subordinate, friend) > 2) {
                    player.sendMessage(new Message("Слишком далеко.", ChatColor.RED));
                    player.performCommand("/helpingaction " + (args.length > 2 ? args[2] : ""));
                    return;
                }

                ClickContainerMessage remove = new ClickContainerMessage("HelpingAction", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " поднимает " + args[1] + "!", ChatColor.DARK_GREEN));
                friend.makeStand();
                if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/" + NextExecutor.NAME + " combat");
                else {
                    player.performCommand("/toolpanel");
                }
                return;
            }
            case "bandaid": {
                String bandage = (args.length > 1 ? args[1] : "");
                String oldoldcontainer = (args.length > 2 ? args[2] : "");
                if (bandage.equals("bandage") || bandage.equals("custom")) {
                        if (bandage.equals("bandage")) {
                            ItemStack is2 = subordinate.getItemForHand(0);
                            ItemStack is1 = subordinate.getItemForHand(1);
                            if (!is1.isEmpty() && is1.hasTagCompound() && is1.getTagCompound() != null && is1.getTagCompound().hasKey("medical") &&
                                    is1.getTagCompound().getString("medical").equals("bandage")) {

                            } else if (!is2.isEmpty() && is2.hasTagCompound() && is2.getTagCompound() != null && is2.getTagCompound().hasKey("medical") &&
                                    is2.getTagCompound().getString("medical").equals("bandage")) {

                            } else {
                                player.sendMessage(new Message("Нет бинтов в руках.", ChatColor.RED));
                                return;
                            }
                        }
                        // CREATE CLICK PANEL
                        Message message = new Message("");
                        MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                        //close.setBold(true);
                        close.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + oldoldcontainer);
                        MessageComponent start = new MessageComponent("Выберите кого перебинтовать:\n", ChatColor.COMBAT);
                        message.addComponent(start);
                        List<Player> targets = new ArrayList<>();
                        targets = ServerProxy.getAllFightersInRange(subordinate, 2);
                        targets.add(subordinate);
                        boolean found = false;
                        for (Player friend : targets) {
                            Combat combat = DataHolder.inst().getCombatForPlayer(friend.getName());
                            if (!friend.hasStatusEffect(StatusType.BLEEDING) && (combat == null || !combat.hasLethalCompany(friend))) continue;
                            found = true;
                            //if (opponent.getName().equals(sender.getName())) continue;
                            MessageComponent oppComponent = new MessageComponent("[" + friend.getName() + "] ", ChatColor.BLUE);
                            oppComponent.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/helpingaction bandaidtarget %s %s %s", bandage, friend.getName(), oldoldcontainer)
                            );
                            message.addComponent(oppComponent);
                        }
                        if (!found) {
                            player.sendMessage(new Message("Нет подходящих целей поблизости.", ChatColor.RED));
                            return;
                        }
                        message.addComponent(close);

                        // SEND PACKET TO CLIENT
                        Helpers.sendClickContainerToClient(message, "HelpingAction", getCommandSenderAsPlayer(sender), subordinate);

                } else {
                    // CREATE CLICK PANEL
                    Message message = new Message("");
                    MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                    close.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + bandage);
                    MessageComponent start = new MessageComponent("Выберите чем бинтовать:\n", ChatColor.COMBAT);
                    message.addComponent(start);
                    MessageComponent ban = new MessageComponent("[Бинт] ", ChatColor.COMBAT);
                    ban.setClickCommand("/helpingaction bandaid bandage " + bandage);
                    message.addComponent(ban);
                    MessageComponent custom = new MessageComponent("[Кусок ткани] ", ChatColor.RED);
                    custom.setClickCommand("/helpingaction bandaid custom " + bandage);
                    custom.setUnderlined(true);
                    custom.setHoverText(new TextComponentString("§4Штраф -1 при перевязке без бинта."));
                    message.addComponent(custom);
                    message.addComponent(close);
                    // SEND PACKET TO CLIENT
                    Helpers.sendClickContainerToClient(message, "HelpingAction", getCommandSenderAsPlayer(sender), subordinate);
                }
                return;
            }
            case "bandaidtarget": {
                String bandage = (args.length > 1 ? args[1] : "");
                String friendName = (args.length > 2 ? args[2] : "");
                String oldoldcontainer = (args.length > 3 ? args[3] : "");
                Player friend = DataHolder.inst().getPlayer(friendName);
                if (ServerProxy.getRoundedDistanceBetween(subordinate, friend) > 2) {
                    player.sendMessage(new Message("Слишком далеко.", ChatColor.RED));
                    player.performCommand("/helpingaction " + oldoldcontainer);
                    return;
                }
                if (friend.hasStatusEffect(StatusType.BURNING)) {
                    player.sendMessage(new Message("Нельзя стабилизировать горящих, сперва потушите!", ChatColor.RED));
                    player.performCommand("/helpingaction " + oldoldcontainer);
                    return;
                }

                if (friend.getCprFrom() != null) {
                    player.sendMessage(new Message("Нельзя перевязать, пока игроку зажимают раны!", ChatColor.RED));
                    player.performCommand("/helpingaction " + oldoldcontainer);
                    return;
                }

                if (bandage.equals("bandage")) {
                    ItemStack is2 = subordinate.getItemForHand(0);
                    ItemStack is1 = subordinate.getItemForHand(1);
                    if (!is1.isEmpty() && is1.hasTagCompound() && is1.getTagCompound() != null && is1.getTagCompound().hasKey("medical") &&
                            is1.getTagCompound().getString("medical").equals("bandage")) {
                        is1.setCount(is1.getCount() - 1);
                    } else if (!is2.isEmpty() && is2.hasTagCompound() && is2.getTagCompound() != null && is2.getTagCompound().hasKey("medical") &&
                            is2.getTagCompound().getString("medical").equals("bandage")) {
                        is2.setCount(is2.getCount() - 1);
                    } else {
                        player.sendMessage(new Message("Нет бинтов в руках.", ChatColor.RED));
                        return;
                    }
                }
                Player friendd = DataHolder.inst().getPlayer(friendName);
                int stabilizationdifficulty = -666;
                int bloodhelpdifficulty = -666;
                if (friendd.hasStatusEffect(StatusType.BLEEDING)) {
                    if (friendd.getStrongestBleeding() < 5) bloodhelpdifficulty = -1;
                    else if (friendd.getStrongestBleeding() < 10) bloodhelpdifficulty = 0;
                    else if (friendd.getStrongestBleeding() < 20) bloodhelpdifficulty = 1;
                    else if (friendd.getStrongestBleeding() < 40) bloodhelpdifficulty = 2;
                    else bloodhelpdifficulty = 3;
                }
                Combat combat = DataHolder.inst().getCombatForPlayer(friend.getName());
                if (combat != null && combat.hasLethalCompany(friendd)) {
                    stabilizationdifficulty = 2;
                }

                SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% первая помощь");
                if (!bandage.equals("bandage")) sdm.getDice().addMod(new Modificator(-1, ModificatorType.NOBANDAGE));
                sdm.build();
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), sdm);
                if (bloodhelpdifficulty != -666 && stabilizationdifficulty == -666) {
                    if (sdm.getDice().getResult() >= bloodhelpdifficulty) {
                        friendd.removeStatusEffect(StatusType.BLEEDING);
                        Message success = new Message(subordinate.getName() + " ", ChatColor.DARK_GREEN);
                        MessageComponent succcomp;
                        succcomp = new MessageComponent("успешно");
                        succcomp.setUnderlined(true);
                        succcomp.setHoverText(new TextComponentString("§2Сложность была " + DataHolder.inst().getSkillTable().get(bloodhelpdifficulty)));
                        success.addComponent(succcomp);
                        success.addComponent(new MessageComponent(" останавливает кровотечение " + friendd.getName() + "!"));
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), success);
                    } else {
                        Message fail = new Message(subordinate.getName() + " ", ChatColor.DARK_RED);
                        MessageComponent failcomp;
                        failcomp = new MessageComponent("безуспешно");
                        failcomp.setUnderlined(true);
                        failcomp.setHoverText(new TextComponentString("§4Сложность была " + DataHolder.inst().getSkillTable().get(Math.max(bloodhelpdifficulty, stabilizationdifficulty))));
                        fail.addComponent(failcomp);
                        fail.addComponent(new MessageComponent(" пытается остановить кровотечение " + friendd.getName() + "!"));
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), fail);                    }
                }
                if (stabilizationdifficulty != -666) {
                    if (sdm.getDice().getResult() >= Math.max(bloodhelpdifficulty, stabilizationdifficulty)) {
                        combat.getLethalCompany2().put(friendd, combat.getLethalCompany2().get(friendd) + "\n§2§lРаны стабилизированы при помощи " + subordinate.getName());
                        combat.removeLethalCompany(friendd);
                        friendd.removeStatusEffect(StatusType.BLEEDING);
                        Message success = new Message(subordinate.getName() + " ", ChatColor.DARK_GREEN);
                        MessageComponent succcomp;
                        succcomp = new MessageComponent("успешно");
                        succcomp.setUnderlined(true);
                        succcomp.setHoverText(new TextComponentString("§2Сложность была " + DataHolder.inst().getSkillTable().get(Math.max(bloodhelpdifficulty, stabilizationdifficulty))));
                        success.addComponent(succcomp);
                        success.addComponent(new MessageComponent(" стабилизирует раны " + friendd.getName() + "!"));
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), success);
                    } else if (bloodhelpdifficulty != -666 && sdm.getDice().getResult() > bloodhelpdifficulty) {
                        Message fail = new Message(subordinate.getName() + " ", ChatColor.DARK_RED);
                        MessageComponent failcomp;
                        failcomp = new MessageComponent("безуспешно");
                        failcomp.setUnderlined(true);
                        failcomp.setHoverText(new TextComponentString("§4Сложность была " + DataHolder.inst().getSkillTable().get(Math.max(bloodhelpdifficulty, stabilizationdifficulty))));
                        fail.addComponent(failcomp);
                        fail.addComponent(new MessageComponent(" пытается стабилизировать раны " + friendd.getName() + ", "));
                        fail.addComponent(new MessageComponent("но ", ChatColor.DARK_GREEN));
                        MessageComponent succcomp;
                        succcomp = new MessageComponent("успешно");
                        succcomp.setHoverText(new TextComponentString("§2Сложность была " + DataHolder.inst().getSkillTable().get(bloodhelpdifficulty)));
                        succcomp.setColor(ChatColor.DARK_GREEN);
                        fail.addComponent(succcomp);
                        fail.addComponent(new MessageComponent(" останавливает кровотечение.", ChatColor.DARK_GREEN));
                        friendd.removeStatusEffect(StatusType.BLEEDING);

                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), fail);
                    } else {
                        Message fail = new Message(subordinate.getName() + " ", ChatColor.DARK_RED);
                        MessageComponent failcomp;
                        failcomp = new MessageComponent("безуспешно");
                        failcomp.setUnderlined(true);
                        failcomp.setHoverText(new TextComponentString("§4Сложность была " + DataHolder.inst().getSkillTable().get(Math.max(bloodhelpdifficulty, stabilizationdifficulty)) + (bloodhelpdifficulty != -666 && bloodhelpdifficulty < stabilizationdifficulty ? " (" + DataHolder.inst().getSkillTable().get(bloodhelpdifficulty) + " для кровотечения)" : "")));
                        fail.addComponent(failcomp);
                        fail.addComponent(new MessageComponent(" пытается стабилизировать раны " + friendd.getName() + "!"));
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), fail);
                    }
                }

                ClickContainerMessage remove = new ClickContainerMessage("HelpingAction", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/" + NextExecutor.NAME + " combat");
                else {
                    player.performCommand("/toolpanel");
                }
            }
            case "cpr": {
                String bandage = (args.length > 1 ? args[1] : "");
                String oldoldcontainer = (args.length > 2 ? args[2] : "");
                if (bandage.equals("bandage") || bandage.equals("custom")) {
                    if (bandage.equals("bandage")) {
                        ItemStack is2 = subordinate.getItemForHand(0);
                        ItemStack is1 = subordinate.getItemForHand(1);
                        if (!is1.isEmpty() && is1.hasTagCompound() && is1.getTagCompound() != null && is1.getTagCompound().hasKey("medical") &&
                                is1.getTagCompound().getString("medical").equals("bandage")) {
                        } else if (!is2.isEmpty() && is2.hasTagCompound() && is2.getTagCompound() != null && is2.getTagCompound().hasKey("medical") &&
                                is2.getTagCompound().getString("medical").equals("bandage")) {
                        } else {
                            player.sendMessage(new Message("Нет бинтов в руках.", ChatColor.RED));
                            return;
                        }
                    }
                    // CREATE CLICK PANEL
                    Message message = new Message("");
                    MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                    //close.setBold(true);
                    close.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + oldoldcontainer);
                    MessageComponent start = new MessageComponent("Выберите кому зажать раны:\n", ChatColor.COMBAT);
                    message.addComponent(start);
                    List<Player> targets = new ArrayList<>();
                    targets = ServerProxy.getAllFightersInRange(subordinate, 2);
                    boolean found = false;
                    for (Player friend : targets) {
                        Combat combat = DataHolder.inst().getCombatForPlayer(friend.getName());
                        if (!friend.hasStatusEffect(StatusType.BLEEDING) && (combat == null || !combat.hasLethalCompany(friend))) continue;
                        found = true;
                        //if (opponent.getName().equals(sender.getName())) continue;
                        MessageComponent oppComponent = new MessageComponent("[" + friend.getName() + "] ", ChatColor.BLUE);
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/helpingaction cprtarget %s %s %s", bandage, friend.getName(), oldoldcontainer)
                        );
                        message.addComponent(oppComponent);
                    }
                    if (!found) {
                        player.sendMessage(new Message("Нет подходящих целей поблизости.", ChatColor.RED));
                        return;
                    }
                    message.addComponent(close);

                    // SEND PACKET TO CLIENT
                    Helpers.sendClickContainerToClient(message, "HelpingAction", getCommandSenderAsPlayer(sender), subordinate);

                } else {
                    // CREATE CLICK PANEL
                    Message message = new Message("");
                    MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                    close.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + bandage);
                    MessageComponent start = new MessageComponent("Выберите чем зажимать:\n", ChatColor.COMBAT);
                    message.addComponent(start);
                    MessageComponent ban = new MessageComponent("[Бинт] ", ChatColor.COMBAT);
                    ban.setClickCommand("/helpingaction cpr bandage " + bandage);
                    message.addComponent(ban);
                    MessageComponent custom = new MessageComponent("[Кусок ткани] ", ChatColor.RED);
                    custom.setClickCommand("/helpingaction cpr custom " + bandage);
                    custom.setUnderlined(true);
                    custom.setHoverText(new TextComponentString("§4Штраф -1 при зажимании без бинта."));
                    message.addComponent(custom);
                    message.addComponent(close);
                    // SEND PACKET TO CLIENT
                    Helpers.sendClickContainerToClient(message, "HelpingAction", getCommandSenderAsPlayer(sender), subordinate);
                }
                return;
            }
            case "cprtarget": {
                String bandage = (args.length > 1 ? args[1] : "");
                String friendName = (args.length > 2 ? args[2] : "");
                String oldoldcontainer = (args.length > 3 ? args[3] : "");
                Player friend = DataHolder.inst().getPlayer(friendName);
                if (ServerProxy.getRoundedDistanceBetween(subordinate, friend) > 2) {
                    player.sendMessage(new Message("Слишком далеко.", ChatColor.RED));
                    player.performCommand("/helpingaction " + oldoldcontainer);
                    return;
                }
                if (friend.hasStatusEffect(StatusType.BURNING)) {
                    player.sendMessage(new Message("Нельзя зажимать раны горящих, сперва потушите!", ChatColor.RED));
                    player.performCommand("/helpingaction " + oldoldcontainer);
                    return;
                }

                if (friend.getCprFrom() != null) {
                    player.sendMessage(new Message("Игроку уже зажимают раны!", ChatColor.RED));
                    player.performCommand("/helpingaction " + oldoldcontainer);
                    return;
                }

                if (bandage.equals("bandage")) {
                    ItemStack is2 = subordinate.getItemForHand(0);
                    ItemStack is1 = subordinate.getItemForHand(1);
                    if (!is1.isEmpty() && is1.hasTagCompound() && is1.getTagCompound() != null && is1.getTagCompound().hasKey("medical") &&
                            is1.getTagCompound().getString("medical").equals("bandage")) {
                        is1.setCount(is1.getCount() - 1);
                    } else if (!is2.isEmpty() && is2.hasTagCompound() && is2.getTagCompound() != null && is2.getTagCompound().hasKey("medical") &&
                            is2.getTagCompound().getString("medical").equals("bandage")) {
                        is2.setCount(is2.getCount() - 1);
                    } else {
                        player.sendMessage(new Message("Нет бинтов в руках.", ChatColor.RED));
                        return;
                    }
                }
                Player friendd = DataHolder.inst().getPlayer(friendName);
                int stabilizationdifficulty = -666;
                int bloodhelpdifficulty = -666;
                if (friendd.hasStatusEffect(StatusType.BLEEDING)) {
                    if (friendd.getStrongestBleeding() < 5) bloodhelpdifficulty = -2;
                    else if (friendd.getStrongestBleeding() < 10) bloodhelpdifficulty = -1;
                    else if (friendd.getStrongestBleeding() < 20) bloodhelpdifficulty = 0;
                    else if (friendd.getStrongestBleeding() < 40) bloodhelpdifficulty = 1;
                    else bloodhelpdifficulty = 2;
                }
                Combat combat = DataHolder.inst().getCombatForPlayer(friendd.getName());
                if (combat != null && combat.hasLethalCompany(friendd)) {
                    stabilizationdifficulty = 1;
                }

                SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% первая помощь");
                if (!bandage.equals("bandage")) sdm.getDice().addMod(new Modificator(-1, ModificatorType.NOBANDAGE));
                sdm.build();
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), sdm);
                if ((bloodhelpdifficulty == -666 || sdm.getDice().getResult() >= bloodhelpdifficulty) && (stabilizationdifficulty == -666 || sdm.getDice().getResult() >= stabilizationdifficulty)) {
                    Message success = new Message(subordinate.getName() + " ", ChatColor.DARK_GREEN);
                    MessageComponent succcomp;
                    succcomp = new MessageComponent("успешно");
                    succcomp.setUnderlined(true);
                    succcomp.setHoverText(new TextComponentString("§2Сложность была " + DataHolder.inst().getSkillTable().get(Math.max(bloodhelpdifficulty, stabilizationdifficulty))));
                    success.addComponent(succcomp);
                    success.addComponent(new MessageComponent(" зажимает раны " + friendd.getName() + "!"));
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), success);
                    friendd.setCprFrom(subordinate);
                    subordinate.setCprTo(friendd);
                } else {
                    Message fail = new Message(subordinate.getName() + " ", ChatColor.DARK_RED);
                    MessageComponent failcomp;
                    failcomp = new MessageComponent("безуспешно");
                    failcomp.setUnderlined(true);
                    failcomp.setHoverText(new TextComponentString("§4Сложность была " + DataHolder.inst().getSkillTable().get(Math.max(bloodhelpdifficulty, stabilizationdifficulty))));
                    fail.addComponent(failcomp);
                    fail.addComponent(new MessageComponent(" пытается зажать раны " + friendd.getName() + "!"));
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, friend, subordinate.getDefaultRange(), fail);
                }


                ClickContainerMessage remove = new ClickContainerMessage("HelpingAction", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/" + NextExecutor.NAME + " combat");
                else {
                    player.performCommand("/toolpanel");
                }
                return;
            }
            case "allydefense":
                Message messagedef = new Message("Выберите цель защиты:\n", ChatColor.COMBAT);
                String oldoldcontainer = (args.length > 1 ? args[1] : "");
                //EntityLivingBase forgePlayer;
                EntityLivingBase forgeTarget;
                if (!DataHolder.inst().isNpc(subordinate.getName())) {
                    forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
                } else {
                    forgePlayer = DataHolder.inst().getNpcEntity(subordinate.getName());
                }
                List<Player> targets = DataHolder.inst().getCombatForPlayer(subordinate.getName()).getFighters();
                for (Player potentialTarget : targets) {
                    if (potentialTarget == subordinate) continue;
                    if (potentialTarget != null) {
                        if (!DataHolder.inst().isNpc(potentialTarget.getName())) {
                            forgeTarget = ServerProxy.getForgePlayer(potentialTarget.getName());
                        } else {
                            forgeTarget = (EntityLivingBase) DataHolder.inst().getNpcEntity(potentialTarget.getName());
                        }
                    } else {
                        continue;
                    }
                    if (forgeTarget.getDistance(forgePlayer) <= 3) {
                        MessageComponent potTarg = new MessageComponent("[" + potentialTarget.getName() + "] ", ChatColor.BLUE);
                        potTarg.setClickCommand("/helpingaction allydefenseready " + potentialTarget.getName() + " " + oldoldcontainer);
                        messagedef.addComponent(potTarg);

                    }
                }
                MessageComponent back = new MessageComponent("[Назад]", ChatColor.GRAY);
                back.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + " " + oldoldcontainer);
                messagedef.addComponent(back);
                Helpers.sendClickContainerToClient(messagedef, "HelpingAction", (EntityPlayerMP) sender, subordinate);
                return;
            case "allydefenseready":
                ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " решает защищать " + args[1] + "!", ChatColor.DARK_GREEN));
                Player friend = DataHolder.inst().getPlayer(args[1]);
                if (ServerProxy.getRoundedDistanceBetween(subordinate, friend) > 3) {
                    player.sendMessage(new Message("Слишком далеко.", ChatColor.RED));
                    player.performCommand("/helpingaction " + (args.length > 2 ? args[2] : ""));
                    return;
                }
                subordinate.clearDefends();
                subordinate.setDefends(DataHolder.inst().getPlayer(args[1]));
                ClickContainerMessage remove = new ClickContainerMessage("HelpingAction", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT) player.performCommand("/" + NextExecutor.NAME + " combat");
                else {
                    player.performCommand("/toolpanel");
                }
                return;
            case "cleardefends":
                if (subordinate.getDefends() != null) {
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " решает больше не защищать " + subordinate.getDefends().getName() + "!", ChatColor.RED));
                }
                subordinate.clearDefends();
                player.performCommand("/geteffects");
                return;
            default: {
                break;
            }
        }

        ClickContainer.deleteContainer(oldContainer, getCommandSenderAsPlayer(sender));

        Message msg = new Message(String.format(START_MESSAGE, "помощь"), ChatColor.COMBAT);

        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " HelpingAction" + oldContainer);
        MessageComponent action = new MessageComponent("[Потушить] ", ChatColor.BLUE);
        action.setHoverText("Снять до трёх ходов горения.", TextFormatting.GOLD);
        action.setClickCommand("/helpingaction extinguish " + oldContainer);
        msg.addComponent(action);
        action = new MessageComponent("[Поднять] ", ChatColor.BLUE);
        action.setClickCommand("/helpingaction getup " + oldContainer);
        msg.addComponent(action);
        action = new MessageComponent("[Перевязать] ", ChatColor.BLUE);
        action.setClickCommand("/helpingaction bandaid " + oldContainer);
        msg.addComponent(action);
        action = new MessageComponent("[Зажать раны] ", ChatColor.BLUE);
        action.setClickCommand("/helpingaction cpr " + oldContainer);
        msg.addComponent(action);
        action = new MessageComponent("[Защищать] ", ChatColor.BLUE);
        action.setClickCommand("/helpingaction allydefense " + oldContainer);
        msg.addComponent(action);
        msg.addComponent(toRoot);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(msg, "HelpingAction", (EntityPlayerMP) sender, subordinate);
//        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//        ClickContainerMessage result = new ClickContainerMessage(, json);
//        KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);


    }
}
