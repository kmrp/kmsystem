package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.attack.ChooseAttackSkill;
import ru.konungstvo.commands.helpercommands.player.attack.PerformAttackCommand;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MagicExecutor extends CommandBase {
    private static String START_MESSAGE = "Выберите %s!\n";
    public static final String NAME = "domagic";

    @Override
    public String getName() {
        return "domagic";
    }

    @Override
    public List<String> getAliases() {
        List<String> res = new ArrayList();
        res.add("MagicButton");
        return res;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        if(args.length > 0) oldContainer = args[0];
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);

            if (!DataHolder.inst().isNpc(subordinate.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
            } else {
                forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(subordinate.getName());
            }



        System.out.println(String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
        // REMOVE PREVIOUS PANEL
        switch (oldContainer) {
            case "ready": {
                int turns = Integer.parseInt(args[1]);
                int debuff = Integer.parseInt(args[2]);
                String name = String.join(" ", Arrays.copyOfRange(args, 3, args.length));
                ClickContainerMessage remove = new ClickContainerMessage("MagicButton", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                //System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()).getReactionList().getQueue().getNext() == subordinate);
                subordinate.addStatusEffect("подготовка", StatusEnd.TURN_START, turns, StatusType.PREPARING, name, (debuff == 0 ? -666 : debuff),
                        new StatusEffect("подготовлено", StatusEnd.TURN_END, 1, StatusType.PREPARED, name, -666, null, false));
                subordinate.setMovementHistory(MovementHistory.NONE);
                subordinate.setChargeHistory(ChargeHistory.NONE);
                player.performCommand("/" + NextExecutor.NAME + " magic");
                return;
            }
            case "chooseskill": {
                int hand = Integer.parseInt(args[1]);
                int maxtargets = -1;
                int range = -1;
                int radius = -1;
                int chained = -1;
                if (args.length > 2) maxtargets = Integer.parseInt(args[2]);
                if (args.length > 3) range = Integer.parseInt(args[3]);
                if (args.length > 4) {
                    if (args[4].startsWith("0123радиус=")) radius = Integer.parseInt(args[4].split("=")[1]);
                    if (args[4].startsWith("0123цепь=")) chained = Integer.parseInt(args[4].split("=")[1]);
                }
                ClickContainerMessage remove = new ClickContainerMessage("MagicButton", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                SkillSaverHandler skillSaverHandler = null;
//                if (hand == 0) {
//                    skillSaverHandler = new SkillSaverHandler(subordinate.getItemForHand(0));
//                } else {
//                    skillSaverHandler = new SkillSaverHandler(subordinate.getItemForHand(1));
//                }
                // GENERATE CLICK PANEL
                Message message = new Message("Выберите навык:\n", ChatColor.COMBAT);
                List<Skill> attackSkills = subordinate.getAttackSkills();

                attackSkills = subordinate.getSkills();

                for (Skill sk : attackSkills) {
                    String skillStr = Character.toUpperCase(sk.getName().charAt(0)) + sk.getName().substring(1);
                    MessageComponent mes = new MessageComponent("[" + skillStr + "] ", ChatColor.BLUE);
                    if (maxtargets > 1) {
                        if (radius > 0) {
                            mes.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/%s choosetargetmulti %s %s %s %s %s", NAME, hand, sk.getName().replace(" ", "_"), maxtargets, range, "0123радиус=" + radius)
                            );
                        } else if (chained > 0) {
                            mes.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/%s choosetargetmulti %s %s %s %s %s", NAME, hand, sk.getName().replace(" ", "_"), maxtargets, range, "0123цепь=" + chained)
                            );
                        } else {
                            mes.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/%s choosetargetmulti %s %s %s %s", NAME, hand, sk.getName().replace(" ", "_"), maxtargets, range)
                            );
                        }
                    }
                    else mes.setClickCommand(String.format("/%s choosetarget %s %s", NAME, hand, sk.getName().replace(" ", "_")));
                    message.addComponent(mes);
                }

                MessageComponent res = new MessageComponent("[Плохо] ", ChatColor.BLUE);
                if (maxtargets > 1) {
                    if (radius > 0) {
                        res.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s choosetargetmulti %s default %s %s %s", NAME, hand, maxtargets, range, "0123радиус=" + radius)
                        );
                    } else if (chained > 0) {
                        res.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s choosetargetmulti %s default %s %s %s", NAME, hand, maxtargets, range, "0123цепь=" + chained)
                        );
                    } else {
                        res.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s choosetargetmulti %s default %s %s", NAME, hand, maxtargets, range)
                        );
                    }
                }
                else res.setClickCommand(String.format("/%s choosetarget %s default", NAME, hand));
                message.addComponent(res);

                MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
                modifiers.setClickCommand("/modifiersbutton MagicButton");
                message.addComponent(modifiers);

                MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
                //toRoot.setBold(true);
                toRoot.setClickCommand("/toroot ChooseSkill " + subordinate.getName());
                message.addComponent(toRoot);


                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "ChooseSkill", getCommandSenderAsPlayer(sender), subordinate);
                return;
            }
            case "choosetarget": {
                ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
                ClickContainerMessage remove = new ClickContainerMessage("MagicButton", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                int hand = Integer.parseInt(args[1]);
                String skill = args[2];
                int range = 20;
                if (args.length > 3) range = Integer.parseInt(args[3]);

                SkillSaverHandler skillSaver = new SkillSaverHandler(subordinate.getItemForHand(hand));
                skillSaver.setSkillSaver(subordinate.getName(), SkillType.MASTERING.toString(), skill.replace("_", " "));

                // CREATE CLICK PANEL
                Message message = new Message("");
                MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                //close.setBold(true);
                close.setClickCommand("/" + ToRoot.NAME + " SelectOpponent" + " MagicButton");
                MessageComponent start = new MessageComponent("Выберите цель:\n", ChatColor.COMBAT);
                message.addComponent(start);
                List<Player> targets = new ArrayList<>();
                targets = ServerProxy.getAllFightersInRange(subordinate, range);

                targets.add(subordinate);
                for (Player opponent : targets) {
                    //if (opponent.getName().equals(sender.getName())) continue;
                    MessageComponent oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s use %s %s %s", NAME, hand, skill, opponent.getName())
                    );
                    message.addComponent(oppComponent);

                }
                message.addComponent(close);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "SelectOpponent", getCommandSenderAsPlayer(sender), subordinate);
//                String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//                ClickContainerMessage result = new ClickContainerMessage("SelectOpponent", json);
//                KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
                return;
            }
            case "choosetargetmulti": {
                ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
                ClickContainerMessage remove = new ClickContainerMessage("MagicButton", true);
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                int hand = Integer.parseInt(args[1]);
                String skill = args[2];
                int maxtargets = Integer.parseInt(args[3]);
                int range = Integer.parseInt(args[4]);
                int radius = -1;
                int chained = -1;
                if (args.length > 5) {
                    if (args[5].startsWith("0123радиус=")) radius = Integer.parseInt(args[5].split("=")[1]);
                    if (args[5].startsWith("0123цепь=")) chained = Integer.parseInt(args[5].split("=")[1]);
                }
                int n = 5;
                if (chained > -1 || radius > -1) n = 6;


                SkillSaverHandler skillSaver = new SkillSaverHandler(subordinate.getItemForHand(hand));
                skillSaver.setSkillSaver(subordinate.getName(), SkillType.MASTERING.toString(), skill.replace("_", " "));

                // CREATE CLICK PANEL
                Message message = new Message("");
                MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
                close.setClickCommand("/" + ToRoot.NAME + " SelectOpponent" + " MagicButton");

                MessageComponent start = new MessageComponent("Выберите цели:\n", ChatColor.COMBAT);
                message.addComponent(start);

                String[] targets = Arrays.copyOfRange(args, n, args.length);
                String targetsUntil = "";
                String targetsString = String.join(" ", targets);
                System.out.println("targ " + targetsString);
                for (String target : targets) {
                    MessageComponent oppComponent = new MessageComponent("[" + target + "] ", ChatColor.DARK_BLUE);
                    if (radius > 0) {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s choosetargetmulti %s %s %s %s %s %s", NAME, hand, skill, maxtargets, range, "0123радиус=" + radius, targetsString.replaceFirst("(\\s|^)" + target + "(\\s|$)", " ")).replaceAll("\\s+", " ")
                        );
                    } else if (chained > 0) {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s choosetargetmulti %s %s %s %s %s %s", NAME, hand, skill, maxtargets, range, "0123цепь=" + chained, String.join(" ", targetsUntil))
                        );
                    } else {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s choosetargetmulti %s %s %s %s %s", NAME, hand, skill, maxtargets, range, targetsString.replaceFirst("(\\s|^)" + target + "(\\s|$)", " ")).replaceAll("\\s+", " ")
                        );
                    }
                    message.addComponent(oppComponent);
                    if (targetsUntil.isEmpty()) targetsUntil += target;
                    else targetsUntil += (" " + target);
                }
                Player ult = subordinate;
                int lrange = range;
                if (radius > 0 && targets.length > 0) {
                    ult = DataHolder.inst().getPlayer(targets[0]);
                    lrange = radius;
                } else if (chained > 0 && targets.length > 0) {
                    ult = DataHolder.inst().getPlayer(targets[targets.length-1]);
                    lrange = chained;
                }

                if (targets.length < maxtargets) {
                    List<Player> newtargets = new ArrayList<>();
                    newtargets = ServerProxy.getAllFightersInRange(ult, lrange);
                    if (!newtargets.contains(subordinate)) newtargets.add(subordinate);

                    for (Player opponent : newtargets) {
                        if ((" " + targetsString + " ").contains(" " + opponent.getName() + " ")) continue;
                        //if (opponent.getName().equals(sender.getName())) continue;
                        MessageComponent oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
                        if (radius > 0) {
                            oppComponent.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/%s choosetargetmulti %s %s %s %s %s %s", NAME, hand, skill, maxtargets, range, "0123радиус=" + radius, (targetsString + " " + opponent.getName()).trim())
                            );
                        } else if (chained > 0) {
                            oppComponent.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/%s choosetargetmulti %s %s %s %s %s %s", NAME, hand, skill, maxtargets, range, "0123цепь=" + chained, (targetsString + " " + opponent.getName()).trim())
                            );
                        } else {
                            oppComponent.setClickCommand(
                                    //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                    String.format("/%s choosetargetmulti %s %s %s %s %s", NAME, hand, skill, maxtargets, range, (targetsString + " " + opponent.getName()).trim())
                            );
                        }
                        message.addComponent(oppComponent);

                    }
                }

                if (targets.length > 0) {
                    MessageComponent ready = new MessageComponent("[Готово] ", ChatColor.RED);
                    ready.setClickCommand(
                            String.format("/%s usemulti %s %s %s", NAME, hand, skill, targetsString)
                    );
                    message.addComponent(ready);
                }

                message.addComponent(close);

                // SEND PACKET TO CLIENT
                Helpers.sendClickContainerToClient(message, "SelectOpponent", getCommandSenderAsPlayer(sender), subordinate);
//                String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//                ClickContainerMessage result = new ClickContainerMessage("SelectOpponent", json);
//                KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
                return;

            }
            case "usemulti": {
                ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));
                player.sendMessage(String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
                int hand = Integer.parseInt(args[1]);
                String skill = args[2].replace("_", " ");
                String[] targets = Arrays.copyOfRange(args, 3, args.length);

                WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(subordinate.getItemForHand(hand));
                String name = "";
                System.out.println("тест1");
                try {
                    name = weaponTagsHandler.getDefaultWeaponName();
                } catch (Exception ignored) {
                    return;
                }

                if (!weaponTagsHandler.getDefaultWeapon().hasKey("statusEffect")) return;

                NBTTagCompound tag = weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic");
                int turns = 0;
                int cost = 0;
                int diff = 0;
                int cooldown = 0;
                boolean instant = false;
                String defence = "";
                if (tag.hasKey("turns")) turns = tag.getInteger("turns");
                if (tag.hasKey("cost")) cost = tag.getInteger("cost");
                if (tag.hasKey("diff")) diff = tag.getInteger("diff");
                if (tag.hasKey("cooldown")) cooldown = tag.getInteger("cooldown");
                if (tag.hasKey("defence")) defence = tag.getString("defence");
                if (tag.hasKey("instant")) instant = tag.getBoolean("instant");

                for (String targetname : targets) {
                    Player target = DataHolder.inst().getPlayer(targetname);
                    Message tr = new Message(subordinate.getName() + " пытается наложить способность на " + target.getName(), ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), tr);
                    //ServerProxy.informDistantMasters(subordinate, tr, subordinate.getDefaultRange().getDistance());


                    System.out.println("тест2");
                    if (turns != 0 && !subordinate.hasPrepared(name)) {
                        Message clack = new Message("Пуф! Ничего не произошло… (не подготовлено)", ChatColor.RED);
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                        //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                        if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                        return;
                    }
                    if (subordinate.onCooldown(name)) {
                        Message clack = new Message("Пуф! Ничего не произошло… (на восстановлении)", ChatColor.RED);
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                        //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                        if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                        return;
                    }
                    if (!DataHolder.inst().useMana(subordinate.getName(), (targets[0].equals(targetname)) ? cost : (int) Math.ceil(cost / 2))) {
                        Message clack = new Message("Пуф! Ничего не произошло… (недостаточно потенциала)", ChatColor.RED);
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                        //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                        if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                        return;
                    }


                    FudgeDiceMessage fdm = null;
                    FudgeDiceMessage fdm2 = null;

                    System.out.println("тест3");
                    System.out.println(skill);
                    int result = 0;
                    if (diff != -666 || !defence.isEmpty()) {
                        fdm = new SkillDiceMessage(subordinate.getName(), "% " + skill, Range.NORMAL_DIE);
                        fdm.build();
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), fdm);
                        //ServerProxy.informDistantMasters(subordinate, fdm, subordinate.getDefaultRange().getDistance());
                        result = fdm.getDice().getResult();
                    }

                    int defenceInt = -666;
                    if (!defence.isEmpty()) {
                        fdm2 = new SkillDiceMessage(target.getName(), "% " + defence, Range.NORMAL_DIE);
                        fdm2.build();
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), fdm2);
                        //ServerProxy.informDistantMasters(target, fdm2, subordinate.getDefaultRange().getDistance());
                        defenceInt = fdm2.getDice().getResult();
                    }

                    if (result < diff) {
                        Message clack = new Message("Пуф! Ничего не произошло… (не достигнута сложность)", ChatColor.RED);
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                        //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                        if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                        continue;
                    } else if (result < defenceInt) {
                        Message clack = new Message("Пуф! Ничего не произошло… (защита успешна)", ChatColor.RED);
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                        if (cooldown > 0) subordinate.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown, StatusType.COOLDOWN, name, (subordinate.getCombatState() == CombatState.SHOULD_ACT));
                        //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                        if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                        continue;
                    }

                    System.out.println("тест4");


                    NBTTagCompound status = weaponTagsHandler.getDefaultWeapon().getCompoundTag("statusEffect");
                    StatusType statusType = null;
                    for (StatusType statusType1 : StatusType.values()) {
                        if (statusType1.toString().equals(status.getString("statusType"))) {
                            statusType = statusType1;
                            break;
                        }
                    }
                    StatusEnd statusEnd = null;
                    for (StatusEnd statusEnd1 : StatusEnd.values()) {
                        if (statusEnd1.toString().equals(status.getString("statusEnd"))) {
                            statusEnd = statusEnd1;
                            break;
                        }
                    }
                    target.addStatusEffect(status.getString("name"), statusEnd, status.getInteger("turns"),
                            statusType, status.getString("context"), status.getInteger("contextInt"), null, (!instant && target == subordinate && statusEnd == StatusEnd.TURN_END && subordinate.getCombatState() == CombatState.SHOULD_ACT));

                    Message success = new Message("Успех! " + target.getName() + " теперь под воздействием способности " + name + ".", ChatColor.GLOBAL);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), success);
                }
                if (cooldown > 0) subordinate.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown, StatusType.COOLDOWN, name, (subordinate.getCombatState() == CombatState.SHOULD_ACT));
                //ServerProxy.informDistantMasters(subordinate, success, subordinate.getDefaultRange().getDistance());
                if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");

                return;
            }
            case "use": {
                ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));
                player.sendMessage(String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
                int hand = Integer.parseInt(args[1]);
                String skill = args[2].replace("_", " ");
                Player target = DataHolder.inst().getPlayer(args[3]);

                WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(subordinate.getItemForHand(hand));
                String name = "";
                System.out.println("тест1");
                try {
                    name = weaponTagsHandler.getDefaultWeaponName();
                } catch (Exception ignored) {
                    return;
                }

                if (!weaponTagsHandler.getDefaultWeapon().hasKey("statusEffect")) return;

                NBTTagCompound tag = weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic");
                int turns = 0;
                int cost = 0;
                int diff = 0;
                int cooldown = 0;
                boolean instant = false;
                String defence = "";
                if (tag.hasKey("turns")) turns = tag.getInteger("turns");
                if (tag.hasKey("cost")) cost = tag.getInteger("cost");
                if (tag.hasKey("diff")) diff = tag.getInteger("diff");
                if (tag.hasKey("cooldown")) cooldown = tag.getInteger("cooldown");
                if (tag.hasKey("defence")) defence = tag.getString("defence");
                if (tag.hasKey("instant")) instant = tag.getBoolean("instant");
                Message tr = new Message(subordinate.getName() + " пытается наложить способность на " + target.getName(), ChatColor.RED);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), tr);
                //ServerProxy.informDistantMasters(subordinate, tr, subordinate.getDefaultRange().getDistance());


                System.out.println("тест2");
                if (turns != 0 && !subordinate.hasPrepared(name)) {
                    Message clack = new Message("Пуф! Ничего не произошло… (не подготовлено)", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                    //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                    if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                    return;
                }
                if (subordinate.onCooldown(name)) {
                    Message clack = new Message("Пуф! Ничего не произошло… (на восстановлении)", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                    //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                    if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                    return;
                }
                if (!DataHolder.inst().useMana(subordinate.getName(), cost)) {
                    Message clack = new Message("Пуф! Ничего не произошло… (недостаточно потенциала)", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                    //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                    if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                    return;
                }


                FudgeDiceMessage fdm = null;
                FudgeDiceMessage fdm2 = null;

                System.out.println("тест3");
                System.out.println(skill);
                int result = 0;
                if (diff != -666 || !defence.isEmpty()) {
                    fdm = new SkillDiceMessage(subordinate.getName(), "% " + skill, Range.NORMAL_DIE);
                    fdm.build();
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), fdm);
                    //ServerProxy.informDistantMasters(subordinate, fdm, subordinate.getDefaultRange().getDistance());
                    result = fdm.getDice().getResult();
                }

                int defenceInt = -666;
                if (!defence.isEmpty()) {
                    fdm2 = new SkillDiceMessage(target.getName(), "% " + defence, Range.NORMAL_DIE);
                    fdm2.build();
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), fdm2);
                    //ServerProxy.informDistantMasters(target, fdm2, subordinate.getDefaultRange().getDistance());
                    defenceInt = fdm2.getDice().getResult();
                }

                if (result < diff) {
                    Message clack = new Message("Пуф! Ничего не произошло… (не достигнута сложность)", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                    //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                    if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                    return;
                } else if (result < defenceInt) {
                    Message clack = new Message("Пуф! Ничего не произошло… (защита успешна)", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), clack);
                    if (cooldown > 0) subordinate.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown, StatusType.COOLDOWN, name, (subordinate.getCombatState() == CombatState.SHOULD_ACT));
                    //ServerProxy.informDistantMasters(subordinate, clack, subordinate.getDefaultRange().getDistance());
                    if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");
                    return;
                }

                System.out.println("тест4");


                NBTTagCompound status = weaponTagsHandler.getDefaultWeapon().getCompoundTag("statusEffect");
                StatusType statusType = null;
                for (StatusType statusType1 : StatusType.values()) {
                    if (statusType1.toString().equals(status.getString("statusType"))) {
                        statusType = statusType1;
                        break;
                    }
                }
                StatusEnd statusEnd = null;
                for (StatusEnd statusEnd1 : StatusEnd.values()) {
                    if (statusEnd1.toString().equals(status.getString("statusEnd"))) {
                        statusEnd = statusEnd1;
                        break;
                    }
                }
                target.addStatusEffect(status.getString("name"), statusEnd, status.getInteger("turns"),
                        statusType, status.getString("context"), status.getInteger("contextInt"), null, (!instant && target == subordinate && statusEnd == StatusEnd.TURN_END && subordinate.getCombatState() == CombatState.SHOULD_ACT));

                Message success = new Message("Успех! " + target.getName() + " теперь под воздействием способности " + name + ".", ChatColor.GLOBAL);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(subordinate, target, subordinate.getDefaultRange(), success);
                if (cooldown > 0) subordinate.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown, StatusType.COOLDOWN, name, (subordinate.getCombatState() == CombatState.SHOULD_ACT));
                //ServerProxy.informDistantMasters(subordinate, success, subordinate.getDefaultRange().getDistance());
                if (!instant) player.performCommand("/" + NextExecutor.NAME + " resetrecoil");

                return;
            }
            default: {
                ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
                System.out.println("test2");
                KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
                break;
            }
        }



        // CREATE CLICK PANEL
        Message message = new Message(String.format(START_MESSAGE, "магию"), ChatColor.COMBAT);

        WeaponTagsHandler weaponTagsHandler = null;
        SkillSaverHandler skillSaver = null;
        ItemStack activeItem = subordinate.getItemForHand(1);
        //ItemStack activeItemSecond = null;

        weaponTagsHandler = new WeaponTagsHandler(activeItem);
        boolean weapon = true;
        String name = "";
        try {
            name = weaponTagsHandler.getDefaultWeaponName();
        } catch (Exception ignored) {
            weapon = false;
        }
        if (activeItem.isEmpty()) weapon = false;

        WeaponTagsHandler weaponTagsHandler2 = null;
        SkillSaverHandler skillSaver2 = null;
        ItemStack activeItem2 = subordinate.getItemForHand(0);
        //ItemStack activeItemSecond = null;

        weaponTagsHandler2 = new WeaponTagsHandler(activeItem2);
        boolean weapon2 = true;
        String name2 = "";
        try {
            name2 = weaponTagsHandler2.getDefaultWeaponName();
        } catch (Exception ignored) {
            weapon2 = false;
        }
        if (activeItem2.isEmpty()) weapon2 = false;


        if (weapon && (weaponTagsHandler.isStatusEffect() || weaponTagsHandler.isWeapon()) && weaponTagsHandler.getDefaultWeapon().hasKey("magic")) {
            NBTTagCompound tag = weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic");
            int turns = 0;
            int debuff = 0;
            int range = 20;
            int multicast = 0;
            int chain = 0;
            int radius = 0;
            if (tag.hasKey("turns")) turns = tag.getInteger("turns");
            if (tag.hasKey("debuff")) debuff = tag.getInteger("debuff");
            if (tag.hasKey("range")) range = tag.getInteger("range");
            if (tag.hasKey("multicast")) multicast = tag.getInteger("multicast");
            if (tag.hasKey("chain")) chain = tag.getInteger("chain");
            if (tag.hasKey("radius")) radius = tag.getInteger("radius");

            if (subordinate.onCooldown(name)) {
                MessageComponent mes = new MessageComponent("[" + "Восстанавливается " + name + "] ", ChatColor.GRAY);
                message.addComponent(mes);
            } else {

                if (turns <= 0) {

                    MessageComponent mes = new MessageComponent("[" + "Использовать " + name + "] ", ChatColor.GLOBAL);
                    skillSaver = new SkillSaverHandler(subordinate.getItemForHand(1));
                    if (skillSaver.isEmpty() || skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString()).equals("")) {
                        // CHOOSE SKILL
                        if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "1" + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : ""));
                        else mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "1");
                    } else {
                        String skill = skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString());
                        if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " choosetargetmulti " + "1 " + skill.replaceAll(" ", "_") + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : "") );
                        else mes.setClickCommand("/" + MagicExecutor.NAME + " choosetarget " + "1 " + skill.replaceAll(" ", "_") + " " + range);
                    }
                    message.addComponent(mes);

                } else {
                    MessageComponent mes = new MessageComponent("[" + "Подготовить " + name + "] ", ChatColor.BLUE);
                    mes.setClickCommand("/" + MagicExecutor.NAME + " ready " + turns + " " + debuff + " " + name);
                    message.addComponent(mes);
                }
            }
        }

        if (weapon && weaponTagsHandler.isStatusEffect() && subordinate.hasPrepared(name)) {
            NBTTagCompound tag = weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic");
            int turns = 0;
            int debuff = 0;
            int range = 20;
            int multicast = 0;
            int chain = 0;
            int radius = 0;
            if (tag.hasKey("turns")) turns = tag.getInteger("turns");
            if (tag.hasKey("debuff")) debuff = tag.getInteger("debuff");
            if (tag.hasKey("range")) range = tag.getInteger("range");
            if (tag.hasKey("multicast")) multicast = tag.getInteger("multicast");
            if (tag.hasKey("chain")) chain = tag.getInteger("chain");
            if (tag.hasKey("radius")) radius = tag.getInteger("radius");

            if (subordinate.onCooldown(name)) {
                MessageComponent mes = new MessageComponent("[" + "Восстанавливается " + name + "] ", ChatColor.GRAY);
                message.addComponent(mes);
            } else {

                MessageComponent mes = new MessageComponent("[" + "Использовать " + name + "] ", ChatColor.GLOBAL);
                skillSaver = new SkillSaverHandler(subordinate.getItemForHand(1));
                if (skillSaver.isEmpty() || skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString()).equals("")) {
                    // CHOOSE SKILL
                    if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "1" + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : ""));
                    else mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "1");
                } else {
                    String skill = skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString());
                    if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " choosetargetmulti " + "1 " + skill.replaceAll(" ", "_") + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : "") );
                    else mes.setClickCommand("/" + MagicExecutor.NAME + " choosetarget " + "1 " + skill.replaceAll(" ", "_") + " " + range);
                }
                message.addComponent(mes);
            }
        }

        if (weapon2 && (weaponTagsHandler2.isStatusEffect() || weaponTagsHandler2.isWeapon()) && weaponTagsHandler2.getDefaultWeapon().hasKey("magic")) {
            NBTTagCompound tag = weaponTagsHandler2.getDefaultWeapon().getCompoundTag("magic");
            int turns = 0;
            int debuff = 0;
            int range = 20;
            if (tag.hasKey("turns")) turns = tag.getInteger("turns");
            if (tag.hasKey("debuff")) debuff = tag.getInteger("debuff");
            if (tag.hasKey("range")) range = tag.getInteger("range");
            int multicast = 0;
            int chain = 0;
            int radius = 0;
            if (tag.hasKey("multicast")) multicast = tag.getInteger("multicast");
            if (tag.hasKey("chain")) chain = tag.getInteger("chain");
            if (tag.hasKey("radius")) radius = tag.getInteger("radius");
            if (subordinate.onCooldown(name2)) {
                MessageComponent mes = new MessageComponent("[" + "Восстанавливается " + name2 + "] ", ChatColor.GRAY);
                message.addComponent(mes);
            } else {
                if (turns <= 0) {
                    MessageComponent mes = new MessageComponent("[" + "Использовать " + name2 + "] ", ChatColor.GLOBAL);
                    skillSaver = new SkillSaverHandler(subordinate.getItemForHand(0));
                    if (skillSaver.isEmpty() || skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString()).equals("")) {
                        // CHOOSE SKILL
                        if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "0" + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : ""));
                        else mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "0");
                    } else {
                        String skill = skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString());
                        if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " choosetargetmulti " + "0 " + skill.replaceAll(" ", "_") + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : "") );
                        else mes.setClickCommand("/" + MagicExecutor.NAME + " choosetarget " + "0 " + skill.replaceAll(" ", "_") + " " + range);
                    }
                    message.addComponent(mes);
                } else {
                    MessageComponent mes = new MessageComponent("[" + "Подготовить " + name2 + "] ", ChatColor.BLUE);
                    mes.setClickCommand("/" + MagicExecutor.NAME + " ready " + turns + " " + debuff + " " + name2);
                    message.addComponent(mes);
                }
            }
        }

        if (weapon2 && weaponTagsHandler2.isStatusEffect() && subordinate.hasPrepared(name2)) {
            NBTTagCompound tag = weaponTagsHandler2.getDefaultWeapon().getCompoundTag("magic");
            int turns = 0;
            int debuff = 0;
            int range = 20;
            int multicast = 0;
            int chain = 0;
            int radius = 0;
            if (tag.hasKey("turns")) turns = tag.getInteger("turns");
            if (tag.hasKey("debuff")) debuff = tag.getInteger("debuff");
            if (tag.hasKey("range")) range = tag.getInteger("range");
            if (tag.hasKey("multicast")) multicast = tag.getInteger("multicast");
            if (tag.hasKey("chain")) chain = tag.getInteger("chain");
            if (tag.hasKey("radius")) radius = tag.getInteger("radius");

            if (subordinate.onCooldown(name2)) {
                MessageComponent mes = new MessageComponent("[" + "Восстанавливается " + name2 + "] ", ChatColor.GRAY);
                message.addComponent(mes);
            } else {
                MessageComponent mes = new MessageComponent("[" + "Использовать " + name2 + "] ", ChatColor.GLOBAL);
                skillSaver = new SkillSaverHandler(subordinate.getItemForHand(0));
                if (skillSaver.isEmpty() || skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString()).equals("")) {
                    // CHOOSE SKILL
                    if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "0" + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : ""));
                    else mes.setClickCommand("/" + MagicExecutor.NAME + " chooseskill " + "0");
                } else {
                    String skill = skillSaver.getSkillSaverFor(subordinate.getName(), SkillType.MASTERING.toString());
                    if (multicast > 0) mes.setClickCommand("/" + MagicExecutor.NAME + " choosetargetmulti " + "0 " + skill.replaceAll(" ", "_") + " " + multicast + " " + range + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : "") );
                    else mes.setClickCommand("/" + MagicExecutor.NAME + " choosetarget " + "0 " + skill.replaceAll(" ", "_") + " " + range);
                }
                message.addComponent(mes);
            }
        }


        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " MagicButton" + " ModifiersButton " + "ToolPanel");
        message.addComponent(toRoot);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "MagicButton", (EntityPlayerMP) sender, subordinate);
//        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//        ClickContainerMessage result = new ClickContainerMessage(, json);
//        KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);


    }
}
