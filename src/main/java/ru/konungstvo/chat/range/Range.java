package ru.konungstvo.chat.range;

// used to determine how distant the message is and how the range is described in chat
public enum Range {

    HUSHED_STORY(1, "===#",  "~"),
    WHISPER_STORY(3, "==#",  "*"),
    QUIET_STORY(9, "=#",     "**"),
    SCREAM_STORY(80, "!!!#", "******"),
    SHOUT_STORY(60, "!!#",   "*****"),
    LOUD_STORY(36, "!#",     "****"),
    NORMAL_STORY(18, "#",    "***"),

    HUSHED_DIE(1, "===%",  "едва слышно бросает"),
    WHISPER_DIE(3, "==%",  "очень тихо бросает"),
    QUIET_DIE(9, "=%",     "тихо бросает"),
    SCREAM_DIE(80, "!!!%", "СВЕРХГРОМКО ОБРУШИВАЕТ"),
    SHOUT_DIE(60, "!!%",   "очень громко бросает"),
    LOUD_DIE(36, "!%",     "громко бросает"),
    NORMAL_DIE(18, "%",    "бросает"),

    HUSHED(1, "===",  "едва слышно"),
    WHISPER(3, "==",  "шепчет"),
    QUIET(9, "=",     "вполголоса"),
    SCREAM(80, "!!!", "орёт"),
    SHOUT(60, "!!",   "кричит"),
    LOUD(36, "!",     "восклицает"),
    NORANGE(0, "~~~~~~~~~~~~~~~~~~~~~~~",     "невидимо"),
    GM_DIE(0, "-%", "бросает"),
    GM(0, "-", "to GM"),
    CM_DIE(0, ";%", "бросает"),
    CM(0, ";", "to CM"),
 //   RADIO(0, "@", "по рации"),
    NORMAL(18);

    private int distance;
    private String sign;
    private String description;

    Range(int distance) {
        this(distance, "", "");
    }

    Range(int distance, String sign, String description) {
        this.distance = distance;
        this.description = description;
        this.sign = sign;
    }

    public int getNumberOfSymbolsToStrip() {
        if (this.sign.length() == 0) return 0;
        if (this.sign.contains("%")) return this.sign.length()-1;
        return this.sign.length();

    }

    public int getDistance() {
        return distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSign() {
        return sign;
    }
}
