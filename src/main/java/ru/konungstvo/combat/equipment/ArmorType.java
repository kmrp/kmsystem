package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public enum ArmorType implements Serializable {
    LIGHT("легкая", 1),
    MEDIUM("средняя", 2),
    HEAVY("тяжелая", 3)
    ;
    private String str;
    private int defense;


    ArmorType(String str, int defense) {
        this.str = str;
        this.defense = defense;
    }

    public String getStr() {
        return str;
    }

    public int getDefense() {
        return defense;
    }

    public static ArmorType getFromName(String name) {
        for (ArmorType type : ArmorType.values()) {
            if (type.getStr().equals(name)) {
                return type;
            }
        }
        return null;
    }


}
