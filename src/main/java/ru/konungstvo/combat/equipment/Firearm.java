package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;
import org.json.simple.parser.ParseException;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

import java.util.ArrayList;
import java.util.HashMap;

public class Firearm implements WeaponTag {
    private String rangeType;
    private String weaponType;
    private int sort;

    private Module optic;
    private Module barrel;
    private Module underbarrel;
    private Module stock;
    private Module magazine;
    private Module extra;
    private Module extra2;

    private ArrayList<String> proficiencies = new ArrayList<String>();

    private HashMap<String, HashMap<String, Integer>> flamerstats = new HashMap<>();
    private boolean isNade = false;

    private boolean infinite;
    private boolean bulletForShotgun;
    private Projectile loadedProjectile;
    private int homemade = 0;
    private int energy = -666;
    private int energyDamage = -666;
    private EnergyType energyType = EnergyType.NONE;
    private String sound = "";



    protected Firearm(String name, Projectile projectile) {
        //super(name);
        this.loadedProjectile = projectile;
    }

    public boolean isBullet() {
        if (loadedProjectile.getCaliber() == null) return false;
        if (loadedProjectile.getCaliber().equals("мрп")) return true;
        if (loadedProjectile.getCaliber().equals("срп")) return true;
        if (loadedProjectile.getCaliber().equals("крп")) return true;
        return false;
    }

    public boolean isBuck() {
        if (loadedProjectile.getCaliber() == null) return false;
        if (loadedProjectile.getCaliber().equals("мрд")) return true;
        if (loadedProjectile.getCaliber().equals("срд")) return true;
        if (loadedProjectile.getCaliber().equals("крд")) return true;
        return false;
    }

    public Projectile getProjectile() {
        return loadedProjectile;
    }

    public void fillFromNBT(NBTTagCompound nbtTagCompound, ArrayList<String> proficiencies) throws ParseException {
        System.out.println("Creating firearm: " + nbtTagCompound.toString());
        this.rangeType = nbtTagCompound.getString("range");
        this.weaponType = nbtTagCompound.getString("type");
        this.sort = Integer.parseInt(nbtTagCompound.getString("sort"));
        this.proficiencies = proficiencies;
        if (nbtTagCompound.hasKey("homemade")) this.homemade = nbtTagCompound.getInteger("homemade");
        if (nbtTagCompound.hasKey("energy")) this.energy = nbtTagCompound.getInteger("energy");
        if (nbtTagCompound.hasKey("energyType")) {
            if (nbtTagCompound.getString("energyType").equals("plasma")) energyType = EnergyType.PLASMA;
            if (nbtTagCompound.getString("energyType").equals("laser")) energyType = EnergyType.LASER;
            if (nbtTagCompound.hasKey("energyDamage")) energyDamage = nbtTagCompound.getInteger("energyDamage");
            else energyDamage = countEnergyDamage();
        }
        if (nbtTagCompound.hasKey("flamerstats")) {
            NBTTagCompound flamerpound = nbtTagCompound.getCompoundTag("flamerstats");
            for (String name : flamerpound.getKeySet()) {
                HashMap<String, Integer> flamer = new HashMap<>();
                int fuel = flamerpound.getCompoundTag(name).getInteger("fuel");
                int distance = flamerpound.getCompoundTag(name).getInteger("distance");
                int angle = flamerpound.getCompoundTag(name).getInteger("angle");
                int durdamage = flamerpound.getCompoundTag(name).getInteger("durdamage");
                flamer.put("fuel", fuel);
                flamer.put("distance", distance);
                flamer.put("angle", angle);
                flamer.put("durdamage", durdamage);
                flamerstats.put(name, flamer);
            }
        }
        if (nbtTagCompound.hasKey("grenadestats")) {
            isNade = true;
        }
        if (isEnergy()) {
            Projectile proj = new Projectile("энергия");
            proj.setDamage(getEnergyDamage());
            proj.setCaliber("энергия");
            if (isLaser()) proj.setCaliber("лазер");
            if (isPlasma()) proj.setCaliber("плазма");
            proj.setMod("");
            if (isShotgun()) proj.setCaliber("энергодробь");
            setLoadedProjectile(proj);
        }
    }

    public int getModulesMods() {
        return this.optic.getMod() +
                this.stock.getMod() +
                this.barrel.getMod() +
                this.underbarrel.getMod() +
                this.extra.getMod() +
                this.extra2.getMod() +
                this.magazine.getMod();
    }

    public void fillModules(WeaponTagsHandler handler) {
        this.optic = new Module(handler.getOptic(), ModuleType.OPTIC);
        this.stock = new Module(handler.getAccessory(), ModuleType.STOCK);
        this.barrel = new Module(handler.getBarrel(), ModuleType.BARREL);
        this.underbarrel = new Module(handler.getUnderbarrel(), ModuleType.UNDERBARREL);
        this.extra = new Module(handler.getExtra(), ModuleType.EXTRA);
        this.extra2 = new Module(handler.getAmmunition(), ModuleType.EXTRA2);
        this.magazine = new Module(handler.getMagazine().getCompoundTag("module"), ModuleType.MAGAZINE);
    }

    public int getDamage() {
        return loadedProjectile.getDamage();
    }

    @Override
    public double getReach() {
        return 0;
    }

    public Projectile getLoadedProjectile() {
        return loadedProjectile;
    }

    public void setLoadedProjectile(Projectile loadedProjectile) {
        this.loadedProjectile = loadedProjectile;
    }

    public String getRangeType() {
        return rangeType;
    }

    public boolean isBreakable() {
        return !weaponType.equals("пулемёт") || sort != 3;
    }

    public boolean isLMG() {
        return weaponType.equals("пулемёт") && sort == 1;
    }

    public boolean isFlamer() {
        return !flamerstats.isEmpty();
    }

    public boolean isLSMG() {
        return weaponType.equals("пистолет-пулемёт") && sort == 1;
    }
    public boolean isLShotgun() {
        return (weaponType.equals("дробовик") || weaponType.equals("обрез")) && sort == 1;
    }

    public boolean isShotgun() {
        return (weaponType.equals("дробовик") || weaponType.equals("обрез"));
    }


    public boolean isSMG() {
        return weaponType.equals("пистолет-пулемёт");
    }

    public int getRecoil() {
        if (isLaser()) return 0;
        switch (weaponType) {
            case "автоматический пистолет":
                if (sort == 1) return 0;
                if (sort == 2) return -1;
                if (sort == 3) return -2;
            case "пистолет-пулемёт":
                if (sort == 1) return 0;
                if (sort == 2) return 0;
                if (sort == 3) return -1;
            case "штурмовая винтовка":
                if (sort == 1) return 0;
                if (sort == 2) return -1;
                if (sort == 3) return -2;
            case "пулемёт":
                if (sort == 1) return -1;
                if (sort == 2) return -2;
                if (sort == 3) return -1;
            case "дробовик":
            case "обрез":
                if (sort == 1) return 0;
                if (sort == 2) return -1;
                if (sort == 3) return -2;
            case "винтовка":
                if (sort == 3) return -1;
            default:
                return 0;
        }
    }

    public int getStrengthReq() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет-пулемёт":
            case "пистолет":
            case "револьвер":
                if (sort == 3) {
                    return 1;
                }
                return -4;
            case "винтовка":
            case "штурмовая винтовка":
                if (sort == 3) {
                    return 2;
                }
                return -4;
            case "гранатомёт":
            case "дробовик":
                if (sort == 2) {
                    return 2;
                } else if (sort == 3) {
                    return 3;
                }
                return -4;
            case "обрез":
                if (sort == 2) {
                    return 1;
                } else if (sort == 3) {
                    return 2;
                }
                return -4;
            case "пулемёт":
                if (sort == 1) return 1;
                if (sort == 2) return 2;
                if (sort == 3) return 3;
            default:
                return -4;
        }

    }

    public boolean isModern() {
        switch (weaponType) {
            case "устаревшее":
            case "устаревший огнестрел":
            case "другое":
            case "метательное":
            case "способность":
                return false;
            default:
                return true;
        }
    }

    public boolean shouldBoom() {
        return (isModern() || weaponType.equals("устаревший огнестрел"));
    }

    public boolean needStock() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
                return false;
            default:
                return true;
        }
    }

    public boolean isTwoHanded() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
            case "метательное":
            case "способность":
                return false;
            case "устаревший огнестрел":
                return !rangeType.toLowerCase().contains("пистолет");
            case "обрез":
                return sort != 1;
            default:
                return true;
        }
    }

    public boolean isPistol() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
                return true;
            default:
                return false;
        }
    }

    public boolean hasSingleRecoil() {
        switch (weaponType) {
            case "дробовик":
            case "обрез":
                return true;
            case "винтовка":
            case "штурмовая винтовка":
                return sort == 3;
            default:
                return false;
        }
    }

    public boolean isPistolOrSawoff() {
        return (weaponType.equals("пистолет") || weaponType.equals("автоматический пистолет") || weaponType.equals("револьвер") || weaponType.equals("обрез"));
    }

    public String getWeaponType() {return weaponType;}
    public Integer getSort() {return sort;}

    public String getSortString() {
        switch (sort) {
            case 1:
                if (weaponType.equals("винтовка")) return "легкая";
                return "легкий";
            case 2:
                if (weaponType.equals("винтовка")) return "средняя";
                return "средний";
            case 3:
                if (weaponType.equals("винтовка")) return "тяжелая";
                return "тяжелый";
            default:
                return "???";
        }
    }
    public int getHomemade() {
        return homemade;
    }

    public HashMap<String, HashMap<String, Integer>> getFlamerstats() {return flamerstats;}

    @Override
    public ArrayList<String> getProficiencies() {
        return proficiencies;
    }

    @Override
    public String getSound() {
        return "";
    }

    public boolean isEnergy() {
        return energy > -666;
    }

    public int getEnergy() {
        return energy;
    }

    public boolean isLaser() {
        return energyType.equals(EnergyType.LASER);
    }
    public boolean isPlasma() {
        return energyType.equals(EnergyType.PLASMA);
    }

    public int countEnergyDamage() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
            case "пистолет-пулемёт":
                return 3 + sort;
            case "винтовка":
            case "штурмовая винтовка":
            case "пулемёт":
            case "устаревший огнестрел":
                return 4 + sort;
            case "дробовик":
            case "обрез":
                return 5 + sort;
            default:
                return 5;
        }
    }

    public int getEnergyDamage() {
        return energyDamage;
    }

    public boolean isNade() {
        return isNade;
    }
}
