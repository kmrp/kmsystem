package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.chat.range.RangeFactory;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PseudonymExecutor extends CommandBase {


    @Override
    public String getName() {
        return "pseudonym";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList<String>();
        result.add("pseudoname");
        return result;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 1) {
            player.setPseudonym("");
            TextComponentString ok = new TextComponentString("Псевдоним сброшен");
            sender.sendMessage(ok);
            return;
        }
        if (args[0].equals("reset")) {
            player.setPseudonym("");
            TextComponentString ok = new TextComponentString("Псевдоним сброшен");
            sender.sendMessage(ok);
            return;
        }
        String pseudo = String.join(" ", Arrays.copyOfRange(args, 0, args.length));
        player.setPseudonym(pseudo);
        TextComponentString ok = new TextComponentString("Псевдоним установлен на " + pseudo);
        sender.sendMessage(ok);

    }

}
