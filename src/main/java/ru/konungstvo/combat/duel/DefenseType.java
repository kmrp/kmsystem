package ru.konungstvo.combat.duel;

public enum  DefenseType {
    NOTHING,
    BLOCKING,
    PARRY,
    HAND_TO_HAND,
    COUNTER,
    ANTICOUNTER,
    EVADE,
    CONSTITUTION,
    WILLPOWER;


    public String toNameString() {
        switch (this) {
            case EVADE: return "уклонение";
            case PARRY: return "парирование";
            case NOTHING: return "ничего";
            case HAND_TO_HAND: return "рукопашный бой";
            case COUNTER: return "контратака";
            case ANTICOUNTER: return "от контратаки";
            case BLOCKING: return "блокирование";
            case CONSTITUTION: return "группировка";
            case WILLPOWER: return "сила воли";
        }
        return "unidentified";

    }
}
