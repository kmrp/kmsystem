package ru.konungstvo.bridge.discord;

import ru.konungstvo.control.Logger;

/**
 * Handles discord events and commands.
 */
public class MyEvents {
    private Logger logger = new Logger("Discord:" + this.getClass().getSimpleName());

    /*
    @EventSubscriber
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getChannel().getName().equals("ingame")) { return; } // TODO: Hardcoded channel name!
        logger.debug("Channel name: " + event.getChannel().getName());
        String res = "";
        String content = event.getMessage().getContent();
        if (content.startsWith(BotUtils.BOT_PREFIX + "ping")) {

            //BotUtils.sendMessage(event.getChannel(), "pong");

        } else if (content.startsWith(BotUtils.BOT_PREFIX + "exec")) {
            String custom_exec_res = null;
            for (ru.konungstvo.player.Player pl : DataHolder.getInstance().getPlayerList()) {
                if (content.startsWith("!exec " + pl.getName())) {
                    String command = content.substring(6 + pl.getName().length() + 1);
                    logger.debug(pl.getName() + " was made to execute command \"" + command + "\" by " + event.getAuthor().getName());
                    if (pl.forgeExecute(command)) {
                        custom_exec_res = pl.getName() + " выполнил команду \"" + command + '\"';
                    } else {
                        custom_exec_res = "ERROR: " + pl.getName() + " не смог выполнить команду " + command;
                    }
                    BotUtils.sendMessage(event.getChannel(), custom_exec_res);
                    return;
                }

            }

            if (content.startsWith(BotUtils.BOT_PREFIX + "exec msg")) {
                BotUtils.sendMessage(event.getChannel(), "Так не получится. Пользуйтесь !msg {player} {message}");
            } else {
                String mes = "";
                try {
                    content.substring(6);
                } catch (Exception e) {
                    return;
                }
                Bukkit.getServer().dispatchCommand(new DiscordCommandSender(event.getChannel()), event.getMessage().getContent().substring(6));
            }


        } else if (content.startsWith(BotUtils.BOT_PREFIX + "msg")) {
            String message = content.substring(5);
            String receiver = message.substring(0, message.indexOf(" "));
            message = "<§2" + event.getAuthor().getName() + "§f->§a" + receiver + "§f>" + message.substring(receiver.length());
            if (!DataHolder.getInstance().getPlayer(receiver).hasPermission(Permission.GM)) {
                ForgeCore.sendMessageTo(receiver, message);
            }
            ForgeCore.informMasters(message);
            BotUtils.sendMessage(event.getChannel(), message.replaceAll("§.", ""));


        } else if (content.startsWith(BotUtils.BOT_PREFIX + "online") || content.startsWith("!онлайн")) {
            String online = "Текущий онлайн (%s): ";
            int i = 0;
            for (ru.konungstvo.player.Player player : DataHolder.getInstance().getPlayerList()) {
                i++;
                if (player.hasPermission(Permission.GM))
                    online += "**" + player.getName() + "**";
                else
                    online += player.getName();
                if (i == DataHolder.getInstance().getPlayerList().size())
                    online += ".";
                else
                    online += ", ";
            }
            if (i == 0)
                res = "Никого онлайн!";
            else
                res = String.format(online, i);

        } else if (content.startsWith(BotUtils.BOT_PREFIX + "help")) {
            BotUtils.sendMessage(event.getChannel(),
                    "Все сообщения в этот канал транслируются в ГМ-чат игры.\n" +
                            "Чтобы отправить в игру глобальное сообщение, поставьте в начале символ ^.\n\n" +
                            "Все команды начинаются с символа !\n" +
                            "!exec {command} — выполнить команду в игре;\n" +
                            "!online либо !онлайн — показать текущий онлайн\n" +
                            "!msg {ник} {сообщение} — написать игроку в лс\n" +
                            "!help — вывести эту справку.");

        } else {
            if (content.startsWith("^")) {
                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    res = ChatColor.GLOBAL + "<§2" + event.getAuthor().getName() + ChatColor.GLOBAL + "> (( " + event.getMessage().getContent().substring(1) + " ))";
                    player.sendMessage(res);
                }
            } else {
                for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                    if (player.hasPermission(Permission.GM.toString())) {
                        res = "<§2" + event.getAuthor().getName() + "§e to GM> §f" + event.getMessage();
                        player.sendMessage(res);
                    }
                }
            }


        }
       // BotUtils.sendMessage(event.getChannel(), res.replaceAll("§.", ""));

    }
    */

}