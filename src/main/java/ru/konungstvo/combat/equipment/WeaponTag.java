package ru.konungstvo.combat.equipment;

import java.lang.reflect.Array;
import java.util.ArrayList;

public interface WeaponTag {
    int getDamage();

    double getReach();

    ArrayList<String> getProficiencies();

    public String getSound();
}
