package ru.konungstvo.combat;

import java.io.Serializable;

@Deprecated
public class Weapon implements Serializable {
    private String skillName;
    private RangedWeapon rangedWeapon = null;
    private int mod;

    public Weapon(String skillName, int mod) {
        this.skillName = skillName;
        this.mod = mod;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public int getMod() {
        return mod;
    }

    public void setMod(int mod) {
        this.mod = mod;
    }


    @Override
    public String toString() {
        String modStr = String.valueOf(mod);
        String ranged = "";
        if (this.rangedWeapon != null) ranged = " {" + this.rangedWeapon.getName() + "}";
        if (mod >= 0) modStr = "+" + mod;
        return "[" + modStr + "] для навыка " + skillName + ranged;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String) {
            String objStr = (String) obj;
            return this.getSkillName().equals(objStr);
        } else if (obj instanceof Weapon) {
            Weapon weapon = (Weapon) obj;
            return weapon.getSkillName().equals(this.skillName);
        }
        return false;
    }

    public RangedWeapon getRangedWeapon() {
        return rangedWeapon;
    }

    public void setRangedWeapon(String name) {
        System.out.println("Name for ranged: " + name);
        for (RangedWeapon rangedWeapon : RangedWeapon.values()) {
            if (rangedWeapon.getName().equals(name) || rangedWeapon.getAlias().equals(name)) {
                this.rangedWeapon = rangedWeapon;
                return;
            }
        }
    }
}
