package ru.konungstvo.commands.helpercommands.player.defense;

import de.cas_ual_ty.gci.item.ItemGun;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.duel.Attack;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.commands.executor.SurrenderExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class SelectDefenseCommand extends CommandBase {
    public static final String NAME = "selectdefense";
    public static String START_MESSAGE = "Выберите способ защиты:\n";
    public static String END_MESSAGE = "Вы выбрали %s.";

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList();
        result.add("selectdefence");
        return result;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        DataHolder dh = DataHolder.inst();

        Player player = dh.getPlayer(sender.getName());
        Player defender = dh.getPlayer(sender.getName());

        if (args.length > 0 && args[0].equals("stopcpr")) {
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(defender, defender.getCprTo(), defender.getDefaultRange(), new Message(defender.getName() + " прекращает зажимать раны " + defender.getCprTo().getName() + ".", ChatColor.RED));
            defender.removeCprTo();
        }

        // GET GM
        if (defender.getSubordinate() != null) {
            defender = defender.getSubordinate();
        }
        Skill defense = defender.getAutoDefense();

        Duel duel = dh.getDuelForDefender(defender.getName());

        if (args.length > 0 && args[0].equals("cancel")) {
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(defender, duel.getAttacker(), defender.getDefaultRange(), new Message(defender.getName() + " отказывается защищать " + duel.getBeingDefended().getName() + ".", ChatColor.RED));
            defender.clearDefends();
            defender = duel.getBeingDefended();
            duel.setBeingDefended(null);
            Player attacker = duel.getAttacker();
            while (!defender.getDefendedBy().isEmpty()) {
                if (ServerProxy.getRoundedDistanceBetween(defender, defender.getDefendedBy().get(defender.getDefendedBy().size()-1)) > 3) {
                    defender.getDefendedBy().get(defender.getDefendedBy().size()-1).clearDefends();
                    continue;
                }
                duel.setBeingDefended(defender);
                duel.setDefended(true);
                Message def = new Message(attacker.getName() + " пытается атаковать " + defender.getName() + ", но сперва вынужден пройти через защитников.", ChatColor.GRAY);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, attacker.getDefaultRange(), def);
                defender = defender.getDefendedBy().get(defender.getDefendedBy().size()-1);
                break;
            }
            duel.setDefender(defender);
//        System.out.println(opponentName + "'s thorax:");
//        DataHolder.inst().getPlayer(opponentName).getArmor().getArmorPiece(BodyPart.THORAX);
//        ClickContainer.deleteContainer("Modifiers", getCommandSenderAsPlayer(sender));
            duel.initDefense();
            ClickContainer.deleteContainer("SelectDefense", getCommandSenderAsPlayer(sender));
            return;
        }

        if (duel.getAttacker().getModifiersState().isModifier(ModifiersState.capture)) {
            try {
                ItemStack is = duel.getAttacker().getItemForHand(duel.getActualActiveHand());
                ItemGun gun = (ItemGun) is.getItem();
                if (gun.hasStockOptions()) {
                    boolean needStock = duel.getActiveAttack().weapon.getFirearm().needStock();
                    boolean hasStock = gun.hasStock(is);
                    System.out.println("needStock " + needStock + " " + "hasStock " + hasStock);
                    double distance = ServerProxy.getDistanceBetween(duel.getAttacker(), defender);
                    if(hasStock) {
                        if (!needStock) {
                            if (distance <= 5) duel.getAttacker().getPercentDice().setStock(-10);
                            if (distance >= 10) duel.getAttacker().getPercentDice().setStock(10);
                        }
                    } else {
                        if (needStock) {
                            if (distance <= 5) duel.getAttacker().getPercentDice().setStock(10);
                            if (distance >= 10) duel.getAttacker().getPercentDice().setStock(-10);
                        }
                    }
                }
            } catch (Exception ignored) {

            }
            try {
                if (duel.getActiveAttack().getWeapon().getMelee().isTwohanded()) {
                    duel.getAttacker().getPercentDice().checkAndGetAndSetLimbInjurty(2, duel.getAttacker().getName());
                } else {
                    duel.getAttacker().getPercentDice().checkAndGetAndSetLimbInjurty(duel.getActualActiveHand(), duel.getAttacker().getName());
                }
            } catch (Exception ignored) {
                duel.getAttacker().getPercentDice().checkAndGetAndSetLimbInjurty(duel.getActualActiveHand(), duel.getAttacker().getName());
            }
            duel.getActiveAttack().getDiceMessage().getDice().cast();
//            duel.getActiveAttack().getDiceMessage().build();
            FudgeDiceMessage fdm = duel.getActiveAttack().getDiceMessage();
            Message capture = new Message(duel.getAttacker().getName() + " пытается принудить " + defender.getName() + " к сдаче с броском ", ChatColor.RED);
            MessageComponent c2 = new MessageComponent(fdm.getDice().getResultAsString(), ChatColor.DICE);
            c2.setUnderlined(true);
            c2.setHoverText(new TextComponentString("§e(( " + duel.getAttacker().getName() + " бросает 4df " + fdm.getDice().getInfo() + " от " +
                    fdm.getDice().getInitial() + " [" + fdm.getDice().getSkillName() + "]. Результат: " + fdm.getDice().getResultAsString() + " ))" +
                    "\n§cБросок совершен без учета модификаторов."));
            capture.addComponent(c2);
            capture.addComponent(new MessageComponent(".", ChatColor.RED));
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(duel.getAttacker(), defender, duel.getAttacker().getDefaultRange(), capture);
            //ServerProxy.informDistantMasters(duel.getAttacker(), capture, duel.getAttacker().getDefaultRange().getDistance());
        } else {
            Message info = new Message(duel.getAttacker().getName() + " атакует " + (duel.isOpportunity() ? "по возможности " : "") + defender.getName() + "!", ChatColor.RED);
            Message masterInfo = new Message(duel.getAttacker().getName() + " атакует " + (duel.isOpportunity() ? "по возможности " : ""), ChatColor.RED);
            MessageComponent oppComponent = new MessageComponent(defender.getName(), ChatColor.RED);
            if (DataHolder.inst().isNpc(defender.getName())) {
                oppComponent.setUnderlined(true);
                oppComponent.setClickCommand("/sub " + defender.getName());
            }
            masterInfo.addComponent(oppComponent);
            masterInfo.addComponent(new MessageComponent("!", ChatColor.RED));
            ServerProxy.sendMessageFromTwoSourcesAndInformMastersWithUniqueMessage(duel.getAttacker(), defender, duel.getAttacker().getDefaultRange(), info, masterInfo);
            //ServerProxy.informDistantMasters(duel.getAttacker(), info, duel.getAttacker().getDefaultRange().getDistance());
        }

        if (defense != null && !duel.getAttacker().getModifiersState().isModifier(ModifiersState.capture)) {
            switch (defense.getSkillType()) {
                case EVADE:
                    player.performCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.EVADE.toString());
                    return;
                case PARRY:
                    player.performCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.PARRY.toString());
                    return;
                case BLOCK:
                    player.performCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.BLOCKING.toString());
                    return;
                case CONSTITUTION:
                    player.performCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.CONSTITUTION.toString());
                    return;
            }

        }

        // CREATE CLICK PANEL
        Message message = new Message(START_MESSAGE, ChatColor.COMBAT);

        if (duel.getAttacker().getModifiersState().isModifier("suppressing")) {
            message = new Message("Вас пытаются подавить огнём!\n", ChatColor.RED);
            MessageComponent oppComponent = new MessageComponent("[Держаться] ", ChatColor.DARK_AQUA);
            oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.WILLPOWER.toString());
            oppComponent.setHoverText("Защищаться силой воли от подавления", TextFormatting.AQUA);
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Укрыться] ", ChatColor.GLOBAL);
            oppComponent.setClickCommand("/" + SurrenderExecutor.NAME + " " + "cover");
            oppComponent.setHoverText("Укрыться и пропустить следующий ход", TextFormatting.WHITE);
            message.addComponent(oppComponent);

        } else {

            if (defender.getCprTo() != null) {
                MessageComponent oppComponent = new MessageComponent("[Прервать зажимание] ", ChatColor.RED);
                oppComponent.setClickCommand("/selectdefence stopcpr");
                message.addComponent(oppComponent);
            }

            MessageComponent oppComponent;

            if (duel.getBeingDefended() == null) {
                oppComponent = new MessageComponent("[Уклонение] ", ChatColor.BLUE);
                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.EVADE.toString());
                message.addComponent(oppComponent);
            } else {
                oppComponent = new MessageComponent("[Уклонение] ", ChatColor.GRAY);
                oppComponent.setHoverText("Нельзя защищать других уклонением.", TextFormatting.BLUE);
                message.addComponent(oppComponent);
            }


            Attack activeAttack = duel.getActiveAttack();
            if (activeAttack.getDiceMessage().getDice().getSkillName().toLowerCase()
                    .equals(DefenseType.HAND_TO_HAND.toString().toLowerCase())
                    || activeAttack.getWeapon().isRanged()
                    && ServerProxy.getRoundedDistanceBetween(defender, duel.getAttacker()) < 2) {

                oppComponent = new MessageComponent("[Рукопашный бой] ", ChatColor.BLUE);
                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.HAND_TO_HAND.toString());
                message.addComponent(oppComponent);
            }

            oppComponent = new MessageComponent("[Парирование] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.PARRY.toString());
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Блокирование] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.BLOCKING.toString());
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Группировка] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.CONSTITUTION.toString());
            message.addComponent(oppComponent);

            if (DataHolder.inst().eligibleForCounter(defender) && !duel.isUncommon()) {
                oppComponent = new MessageComponent("[Контратака] ", ChatColor.BLUE);
                oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.COUNTER.toString());
                message.addComponent(oppComponent);
            } else if (duel.getAttacks().size() <= 1 && defender.getCombatState() == CombatState.SHOULD_WAIT && !duel.isUncommon() && (!defender.hasStatusEffect(StatusType.STUNNED) || defender.cantBeStunned())) {
                oppComponent = new MessageComponent("[Контратака] ", ChatColor.GRAY);
                oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.COUNTER.toString());
                oppComponent.setHoverText("Контратака недоступна, но если длина вашего оружия превышает длину оружия противника, то вы можете использовать контратаку.", TextFormatting.GRAY);
                message.addComponent(oppComponent);
            }

            if (duel.getAttacker().getModifiersState().isModifier(ModifiersState.capture)) {
                oppComponent = new MessageComponent("[Сдаться] ", ChatColor.DEFAULT);
                oppComponent.setClickCommand("/" + SurrenderExecutor.NAME);
                message.addComponent(oppComponent);
            }

            if (duel.getBeingDefended() == null) {
                oppComponent = new MessageComponent("[Ничего] ", ChatColor.BLUE);
                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.NOTHING.toString());
                message.addComponent(oppComponent);
            } else {
                oppComponent = new MessageComponent("[Закрыть собой] ", ChatColor.BLUE);
                oppComponent.setHoverText("Вы гарантированно защитите союзника от атаки, но получите урон целиком.", TextFormatting.RED);
                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.NOTHING.toString());
                message.addComponent(oppComponent);
                oppComponent = new MessageComponent("[Не защищать] ", ChatColor.RED);
                oppComponent.setHoverText("Отказаться защищать союзника.", TextFormatting.RED);
                oppComponent.setClickCommand("/" + SelectDefenseCommand.NAME + " cancel");
                message.addComponent(oppComponent);
            }
        }

        MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
        modifiers.setClickCommand("/modifiersbutton");
        message.addComponent(modifiers);


        // SEND PACKET TO CLIENT
        EntityPlayerMP client = getCommandSenderAsPlayer(sender);
        //if (dh.isNpc(sender.getName())) {
        //    client = ServerProxy.getForgePlayer(mastermind.getName());
        //}
        Helpers.sendClickContainerToClient(message, "SelectDefense", client, defender);
        /*
        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
        ClickContainerMessage result = new ClickContainerMessage("SelectDefense", json);
        KMPacketHandler.INSTANCE.sendTo(result, getCommandSenderAsPlayer(sender));
         */
    }
}
