package ru.konungstvo.chat.message;

import net.minecraft.util.text.TextComponentString;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.Language;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.chat.range.RangeFactory;
import ru.konungstvo.control.Logger;
import ru.konungstvo.player.Permission;

import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LanguageMessage extends RoleplayMessage {
    private List<MessageComponent> components;
    private String playerName;
    private String content;
    private Language language;
    protected Range range;
    Logger logger = new Logger("Message");
    private HashMap<String, String> inputs = new HashMap<>();
    private boolean built;
    private Permission needslanguage;
    private RoleplayMessage messageIfUnderstands;
    private RoleplayMessage messageIfDoesntUnderstand;

    static public final List<String> hinte = Arrays.asList("ichi", "hito", "nana", "yatsu", "ju", "iru", "ni", "mi", "ue", "shimo", "o", "onna", "man", "tsuchi", "yama", "sen", "kawa", "ko", "sho", "kuchi",
            "te", "go", "hi", "roku", "bu", "naka", "en", "tsuki", "hiru", "ima", "ki", "tete", "moku", "ho", "ten", "tomo", "mizu", "sho", "hidari", "yotsu", "de", "rittoru", "kita", "hon", "haha",
            "nakaba", "migi", "shiroi", "umu", "furu");
    static public final List<String> thal = Arrays.asList("minä", "talo", "ssa", "mme", "nie", "mi", "vaa", "ra", "jo", "lä", "o", "sä", "kään", "del", "anen", "mäl", "jär", "inen", "myy", "pä",
            "per", "kele", "mikä", "ketto", "kar", "tuu", "maa", "kolme", "klu", "hää", "linen", "minen", "hei", "ho", "mö", "nöö", "lukke", "söö", "töön", "teå", "ån", "emen", "ainen", "tål", "rainen",
            "yksi", "tyt", "yhde", "toista", "neljä");
    static public final List<String> robo = Arrays.asList("g","u","k","ó","ÿà","ò","é","i","ë","ïx","ö","l","ê","äa","æ","å","èõ","ç","ì","s","n","ü","to","ùá","ã","â","ñ","ð","þ","w","í","h",
            "ø","ôm","cv","ý","f","r","û","p","e","d","î");

    static private final String alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    private static final Pattern pattern = Pattern.compile("([а-яё]{1,3})");

    public LanguageMessage(String playerName, String content, Range range, Language language) {
        super(playerName, content, range);
        this.components = null;
        this.playerName = playerName;
        this.content = content.replaceAll("&([a-z0-9])", "§$1");
        this.range = range;
        this.language = language;
        this.built = false;

        //logger.debug("Created message with content \"" + content + "\" and range " + range + " and template " + template);
    }

    private void generateTemplate() {
        if (this.components != null) return;
        List<MessageComponent> template = new LinkedList<>();
        setComponents(buildTemplate(template));
    }

    // This is the method that should be overriden by subclasses to generate different template
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {

        StringBuilder contentNew = new StringBuilder();
        String contentTemp = content.replaceAll("§([a-z0-9])", " §$1 ");
        contentTemp = contentTemp.replaceAll("([\\.,!?—])", " $1 ");
        contentTemp = contentTemp.replaceAll(">>>", " >>> ");
        contentTemp = contentTemp.replaceAll("\\*", " * ");
        String[] words = contentTemp.split(" ");
        System.out.println(content);
        boolean translate = true;
        boolean action = false;
        for (String word : words) {
            if(word.matches("§([a-z0-9])") || word.matches("([\\.,!?\\-—])")) {
                System.out.println(word);
                contentNew.append(word);
            } else if (word.equals(">>>")) {
                System.out.println(word);
                translate = !translate;
            } else if (word.equals("*")) {
                System.out.println(word);
                action = !action;
                contentNew.append(action ? " " : "").append("* ");
                continue;
            } else if (!translate || action) {
                System.out.println(word);
                contentNew.append(word);
            } else if (word.matches("[a-zA-Z]+")) {
                System.out.println(word);
                contentNew.append(word);
            } else if (word.matches("[а-яёА-ЯЁ\\-]+")) {
                System.out.println(word);
                System.out.println(word + " must be translated");
                word = word.replaceAll("\\-", "");
                contentNew.append(translateWord(word));
            } else {
                System.out.println(word);
                contentNew.append(word);
            }
            contentNew.append(" ");
        }
        String contentFinal = contentNew.toString().trim();
        System.out.println(contentFinal);
        contentFinal = contentFinal.replaceAll("\\s§([a-z0-9])\\s", "§$1");
        contentFinal = contentFinal.replaceAll("\\s([\\.,!?\\-—])\\s", "$1");
        contentFinal = contentFinal.replaceAll("\\s([\\.,!?\\-])", "$1");
        contentFinal = contentFinal.replaceAll("\\s\\*\\s", "*");
        contentFinal = contentFinal.replaceAll("\\s\\*$", "*");
        contentFinal = contentFinal.replaceAll("\\s\\s", " ");
        contentFinal = contentFinal.replaceAll("\\s\\s", " ");
        messageIfDoesntUnderstand = new RoleplayMessage(playerName, contentFinal, range);
        messageIfDoesntUnderstand.build();
        StringBuilder stringBuilder = new StringBuilder();
        String[] test2 = content.split(">>>");
        boolean hinteb = true;
        boolean skip = false;
        if (content.startsWith(">>>")) {
            skip = true;
        }
        for (String s : test2) {
            if (skip) skip = false;
            else if (hinteb) stringBuilder.append(" ").append(language.getSign()).append(" ");
            else stringBuilder.append(" §6§lα§r ");
            stringBuilder.append(s);
            hinteb = !hinteb;
        }
        content = stringBuilder.toString().trim().replaceAll("\\s\\s", " ");
        MessageComponent test = new MessageComponent("[" + language.getName() + "]", ChatColor.DARK_GRAY);
        test.setHoverText(new TextComponentString("§a" + playerName + (range == Range.NORMAL ? "" : "§f (" + range.getDescription() + ")") + "§f: " + contentFinal));
        MessageComponent gmes = new MessageComponent("{playerName} ", ChatColor.NICK);
        if (range == Range.NORMAL) {
            template.add(gmes);
            template.add(test);
            template.add(new MessageComponent(": {content}"));
            //template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + ": {content}";
        } else {
            template.add(gmes);
            template.add(test);
            template.add(new MessageComponent(" ({rangeDescription}): {content}"));
            //template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + " ({rangeDescription}): {content}";
        }
        return template;
    }

    public String getContent() {
        return this.content;
    }


    public String translateWord(String word) {
        StringBuilder result = new StringBuilder();
        List<String> bits = language.getBits();
        System.out.println(word);
        Matcher matcher = pattern.matcher(word.toLowerCase());
        while (matcher.find()) {
            int num = 1;
            String test = matcher.group();
            for (char ch : test.toCharArray()) {
                num += alphabet.indexOf(String.valueOf(ch));
            }
            result.append(bits.get(num % bits.size()));
            System.out.println(bits.get(num % bits.size()));
        }
        String resStr = result.toString();
        if (Character.isUpperCase(word.charAt(0))) resStr = resStr.substring(0, 1).toUpperCase() + resStr.substring(1);
        return resStr;
//        char[] chars = word.toLowerCase().toCharArray();
//        StringBuilder result = new StringBuilder();
//        System.out.println(chars);
//        for (int i = 0; i < chars.length; i++) {
//            int num = 1;
//            num += alphabet.indexOf(chars[i]);
//            if (chars.length >= i + 1) num += alphabet.indexOf(chars[i+1]);
//            if (chars.length >= i + 2) num += alphabet.indexOf(chars[i+2]);
//            result.append(hinte.get(num % hinte.size() - 1));
//            System.out.println(hinte.get(num % hinte.size() - 1));
//            i += 3;
//        }
//        return result.toString();
    }

    // replace {variables} in template with according values with the help of MessageBuilder
    public void build() {
        if (isBuilt()) return;
        generateTemplate();
        inputs.put("content", content);
        inputs.put("playerName", playerName);
        inputs.put("rangeDescription", range.getDescription());

        for (MessageComponent component : components) {
           for (Map.Entry<String, String> input : inputs.entrySet()) {
               String variable = '{' + input.getKey() + '}';
               if (component.getText().contains(variable)) {
                   // put actual content in templates
                   component.setText(component.getText().replace(variable, input.getValue()));
               }
           }
        }
        built = true;
        try {
            //DataHolder.inst().getPlayer(playerName).getPercentDice().setSkill(0); // reset percent dice for skill //TODO sus
            //DataHolder.inst().getPlayer(playerName).getPercentDice().setCustom(0);
        } catch (Exception ignored) {}
        /*
        inputs.put("content", content);
        inputs.put("playerName", playerName);
        inputs.put("rangeDescription", range.getDescription());
        try {
            String templateStr = template.toString();
            result = MessageBuilder.build(templateStr, inputs);
        } catch (MissingFormatArgumentException e) {
            this.result = new MessageComponent(e.getCause() + " " + e.getMessage(), ChatColor.RED);
            //return new GameMessage(e.getCause() + " " + e.getMessage(), ChatColor.RED);
        }

         */
    }

    public boolean isBuilt() {
        return built;
    }

    public void unbuild() {
        this.built = false;
    }

    private void defineRange() {
        range = RangeFactory.build(content);
        content = content.substring(range.getSign().length());
    }

    @Deprecated
    public String getResult() {
        return "";
    }

    public String getPlayerName() {
        return playerName;
    }

    public Range getRange() {
        return this.range;
    }

    public void setComponents(List<MessageComponent> components) {
        this.components = components;
    }

    public List<MessageComponent> getComponents() {
        return components;
    }

    public int getRangeAsInt() {
        return this.range.getDistance();
    }

    protected void addInput(String var, String input) {
        inputs.put(var, input);
    }

    protected String getInput(String input) {
        return inputs.get(input);
    }

    protected void setContent(String content) {
        this.content = content;
    }

    public void setRange(Range range) {
        this.range = range;
//        generateTemplate();
    }

    public RoleplayMessage getMessageIfDoesntUnderstand() {
        return messageIfDoesntUnderstand;
    }

    public Language getLanguage() {return language;}
}
