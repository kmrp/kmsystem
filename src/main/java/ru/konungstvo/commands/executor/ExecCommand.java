package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.GenericException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.util.Arrays;

public class ExecCommand extends CommandBase {

    @Override
    public String getName() {
        return "exec";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender sender, String[] args) throws CommandException {

        if (args.length == 0) {
            TextComponentString tcs = new TextComponentString("Пример использовани: /exec Volpe say Я лох");
            tcs.getStyle().setColor(TextFormatting.YELLOW);
            sender.sendMessage(tcs);
            return;
        }

        String name = args[0];
        Player player = DataHolder.inst().getPlayer(name);

        if (player == null) {
            TextComponentString tcs = new TextComponentString("Игрок " + name + " не найден.");
            tcs.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(tcs);
            return;
        }

        String command = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
        player.performCommand(command);
        TextComponentString tcs = new TextComponentString(name + " исполнил команду \"" + command);
        tcs.getStyle().setColor(TextFormatting.YELLOW);
        sender.sendMessage(tcs);
        return;

    }
}
