package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.control.network.ModifierMessage;
import ru.konungstvo.control.network.ModifierSyncMessage;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

public class SyncModifiers extends CommandBase {
    public static final String NAME = "syncmodifiers";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String modifierN = args[0];
        boolean purge = false;
        if (modifierN.equals("purge")) {
            purge = true;
        }
//        System.out.println(NAME + " is being executed. Type: " + modifierType + ", mod: " + modifier);

        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (player.getSubordinate() != null) {
            player = player.getSubordinate();
        }
//        System.out.println("modifiersState for " + player.getName() + ": " + player.getModifiersState());

        /*
        Duel duel = DataHolder.getInstance().getDuelForPlayer(sender.getName());
        if (duel != null) {
            duel.setCustomModifierFor(sender.getName(), modifierType, modifier);
        }
         */
        int dice = -666;
        int damage = -666;
        int bodypart = -666;
        int percent = -666;

        if (!purge) {
            dice = Integer.parseInt(args[0]);
            damage = Integer.parseInt(args[1]);
            percent = Integer.parseInt(args[2]);
            bodypart = Integer.parseInt(args[3]);

        }

        // REMOVE PREVIOUS PANEL
//        ClickContainerMessage remove = new ClickContainerMessage("ChooseModifier", true);
//        KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));

        // SEND PACKET TO CLIENT
        boolean serial = false;
        boolean stationary = false;
        boolean cover = false;
        boolean optic = false;
        boolean bipod = false;
        boolean stunning = false;
        boolean capture = false;
        boolean noriposte = false;
        boolean shieldwall = false;
        boolean suppressing = false;


        if (!purge) {
            if (args[4].equals("1")) serial = true;
            if (args[5].equals("1")) stationary = true;
            if (args[6].equals("1")) cover = true;
            if (args[7].equals("1")) optic = true;
            if (args[8].equals("1")) bipod = true;
            if (args[9].equals("1")) stunning = true;
            if (args[10].equals("1")) capture = true;
            if (args[11].equals("1")) noriposte = true;
            if (args[12].equals("1")) shieldwall = true;
            if (args[13].equals("1")) suppressing = true;
            player.getModifiersState().setModifier("dice", dice);
            player.getModifiersState().setModifier("damage", damage);
            player.getModifiersState().setModifier("bodypart", bodypart);
            player.getModifiersState().setModifier("serial", Integer.parseInt(args[3]));
            player.getModifiersState().setModifier("percent", percent);
            player.getModifiersState().setModifier("stationary", Integer.parseInt(args[5]));
            player.getModifiersState().setModifier("cover", Integer.parseInt(args[6]));
            player.getModifiersState().setModifier("optic", Integer.parseInt(args[7]));
            player.getModifiersState().setModifier("bipod", Integer.parseInt(args[8]));
            player.getModifiersState().setModifier("stunning", Integer.parseInt(args[9]));
            player.getModifiersState().setModifier("capture", Integer.parseInt(args[10]));
            player.getModifiersState().setModifier("noriposte", Integer.parseInt(args[11]));
            player.getModifiersState().setModifier("shieldwall", Integer.parseInt(args[12]));
            player.getModifiersState().setModifier("suppressing", Integer.parseInt(args[13]));
        } else {
            player.getModifiersState().setModifier("dice", -666);
            player.getModifiersState().setModifier("damage", -666);
            player.getModifiersState().setModifier("bodypart", -666);
            player.getModifiersState().setModifier("serial", -666);
            player.getModifiersState().setModifier("percent", -666);
            player.getModifiersState().setModifier("stationary", -666);
            player.getModifiersState().setModifier("cover", -666);
            player.getModifiersState().setModifier("optic", -666);
            player.getModifiersState().setModifier("bipod", -666);
            player.getModifiersState().setModifier("stunning", -666);
            player.getModifiersState().setModifier("capture", -666);
            player.getModifiersState().setModifier("noriposte", -666);
            player.getModifiersState().setModifier("shieldwall", -666);
            player.getModifiersState().setModifier("suppressing", -666);
        }

//        if (modifiedDamage) System.out.println("Modified Damage True");
//        if (bodyPart) System.out.println("Body Part True");
//        if (serial) System.out.println("Serial True");

        ModifierSyncMessage result = new ModifierSyncMessage(dice, damage, bodypart, serial, percent, stationary, cover, optic, bipod, stunning, capture, noriposte, shieldwall, suppressing);
        KMPacketHandler.INSTANCE.sendTo(result, getCommandSenderAsPlayer(sender));

//        System.out.println("Set " + modifierType + " to " + modifier + " for " + player.getName());
//
//        TextComponentString endResult = new TextComponentString("Модификаторы обновлены.");
//        endResult.getStyle().setColor(TextFormatting.GRAY);
////        sender.sendMessage(endResult);
//
//        ClickContainer.deleteContainer("ModifiersButton", (EntityPlayerMP) sender);
//        System.out.println(toRoot);
//        if(!toRoot.equals("")) DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + toRoot + " " + oldContainer);
    }
}
