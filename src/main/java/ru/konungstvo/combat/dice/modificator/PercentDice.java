package ru.konungstvo.combat.dice.modificator;

import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.control.DataHolder;

public class PercentDice {
    private int custom;
    private int wound;
    private int previousDefenses;
    private int skill;
    private int modules;
    private int bipod;
    private int bloodloss;
    private int limbInjury;
    private int ammo;
    private int mentalDebuff;
    private int weight;
    private int stock;
    private int highground;
    private int fatigue;
    private int weakened;
    private int buff;
    private int ripost;

    private int eqBuff;

    private int psychWeight;

    private int backstab;

    private int proficiency;

    private int hunger;

    private int thirst;

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }


    public PercentDice() {
        this.custom = 0;
        this.wound = 0;
        this.previousDefenses = 0;
        this.skill = 0;
        this.modules = 0;
        this.bipod = 0;
        this.bloodloss = 0;
        this.limbInjury = 0;
        this.ammo = 0;
        this.mentalDebuff = 0;
        this.weight = 0;
        this.stock = 0;
        this.highground = 0;
        this.fatigue = 0;
        this.weakened = 0;
        this.buff = 0;
        this.ripost = 0;
        this.eqBuff = 0;
        this.psychWeight = 0;
        this.backstab = 0;
        this.proficiency = 0;
        this.hunger = 0;
        this.thirst = 0;
    }

    public int get() {
        return custom + wound + previousDefenses + skill + modules + bipod + bloodloss + limbInjury + ammo + mentalDebuff + weight + stock + highground + fatigue + weakened + buff + ripost + eqBuff + psychWeight + backstab + proficiency + hunger + thirst;
    }

    public String getPercent() {
        int percentDice = get();
        if (percentDice >= 100) percentDice = percentDice % 100;
        if (percentDice <= -100) percentDice = percentDice % 100;
        if (percentDice == 0) return "§7";
        String dbuff = "§aБафф: ";
        if (percentDice < 0 ) dbuff = "§cДебафф: ";
        return "\n" + dbuff + percentDice + "%";
    }

    public int getCustom() {
        return custom;
    }

    public void addToCustom(int add) {
        this.custom += add;
    }

    public void setPreviousDefenses(int previousDefenses) {
        this.previousDefenses = previousDefenses;
    }

    public void setCustom(int custom) {
        this.custom = custom;
    }

    public int getWound() {
        return wound;
    }

    public void setWound(int wound) {
        this.wound = wound;
    }

    public int getEqBuff() {
        return eqBuff;
    }

    public void setEqBuff(int eqBuff) {
        this.eqBuff = eqBuff;
    }

    public int getBackStab() {
        return backstab;
    }

    public void setBackStab(int backstab) {
        System.out.println(backstab + " backstab");
        this.backstab = backstab;
    }

    public int getPsychWeight() {
        return psychWeight;
    }

    public void setPsychWeight(int psychWeight) {
        this.psychWeight = psychWeight;
    }


    public void addToWound(int add) {
        this.wound += add;
    }

    @Override
    public String toString() {
        String result = "";
        if (custom != 0) {
            result += "\n" + (custom < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Кастом: " + custom + "%";
        }
        if (wound != 0) {
            result += "\n"  + (wound < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) +  "Раны: " + wound + "%";
        }
        if (previousDefenses != 0) {
            result += "\n" + (previousDefenses < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Пред. защиты: " + previousDefenses + "%";
        }
        if (skill != 0) {
            result += "\n" + (skill < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Навык: " + skill + "%";
        }
        if (modules != 0) {
            result += "\n" + (modules < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Модули: " + modules + "%";
        }
        if (bipod != 0) {
            result += "\n" + (bipod < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Сошки: " + bipod + "%";
        }
        if (bloodloss != 0) {
            result += "\n" + (bloodloss < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Кровопотеря: " + bloodloss + "%";
        }
        if (limbInjury != 0) {
            result += "\n" + (limbInjury < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Повреждения конечностей: " + limbInjury + "%";
        }
        if (ammo != 0) {
            result += "\n" + (ammo < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Боеприпас: " + ammo + "%";
        }
        if (mentalDebuff != 0) {
            result += "\n" + (mentalDebuff < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Доп. штраф от ментальных ран: " + mentalDebuff + "%";
        }
        if (weight != 0) {
            result += "\n" + (weight < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Нагрузка: " + weight + "%";
        }
        if (psychWeight != 0) {
            result += "\n" + (psychWeight < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Психическая нагрузка: " + psychWeight + "%";
        }
        if (stock != 0) {
            result += "\n" + (stock < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Приклад: " + stock + "%";
        }

        if (highground != 0) {
            result += "\n" + (highground < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Высота: " + highground + "%";
        }

        if (fatigue != 0) {
            result += "\n" + (fatigue < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Усталость: " + fatigue + "%";
        }

        if (weakened != 0) {
            result += "\n" + (weakened < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Слабость: " + weakened + "%";
        }

        if (buff != 0) {
            if (buff > 0) result += "\n" + (buff < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Усиление: " + buff + "%";
            if (buff < 0) result += "\n" + (buff < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Ослабление: " + buff + "%";
        }

        if (ripost != 0) {
            result += "\n" + (ripost < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Рипост: " + ripost + "%";
        }

        if (eqBuff != 0) {
            result += "\n" + (eqBuff < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "От снаряжения: " + eqBuff + "%";
        }

        if (backstab != 0) {
            result += "\n" + (backstab < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "В спину: " + backstab + "%";
        }

        if (proficiency != 0) {
            result += "\n" + (proficiency < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Умение: " + proficiency + "%";
        }

        if (hunger != 0) {
            result += "\n" + (hunger < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Голод: " + hunger + "%";
        }

        if (thirst != 0) {
            result += "\n" + (thirst < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) + "Жажда: " + thirst + "%";
        }

        return result;

    }

    public int getModules() {
        return modules;
    }

    public void setModules(int modules) {
        this.modules = modules;
    }

    public int getBipod() {
        return bipod;
    }

    public void setBipod(int bipod) {
        this.bipod = bipod;
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public int getBloodloss() {
        return bloodloss;
    }

    public void setBloodloss(int bloodloss) {
        this.bloodloss = bloodloss;
    }

    public int getLimbInjury() {
        return limbInjury;
    }

    public void setLimbInjury(int limbInjury) {
        this.limbInjury = limbInjury;
    }

    public int getMentalDebuff() {
        return mentalDebuff;
    }

    public void setMentalDebuff(int mentalDebuff) {
        this.mentalDebuff = mentalDebuff;
    }

    public int getWeightDebuff() {
        return weight;
    }

    public void setWeightDebuff(int weight) {
        this.weight = weight;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getHighground() {
        return highground;
    }

    public void setHighground(int highground) {
        this.highground = highground;
    }


    public int getFatigue() {
        return fatigue;
    }

    public void setFatigue(int fatigue) {
        this.fatigue = fatigue;
    }

    public int getWeakened() {
        return weakened;
    }

    public void setWeakened (int weakened) {
        this.weakened = weakened;
    }

    public void setBuff (int buff, int level) {
        if (buff > 0) {
            buff = Math.max(0, buff - (level - 2) * 10);
        }
        this.buff = buff;
    }

    public int getBuff() {return buff;}

    public int getRipost() {
        return ripost;
    }

    public void setRipost(int ripost) {
        this.ripost = ripost;
    }

    public int getHunger() {
        return hunger;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public int getThirst() {
        return thirst;
    }

    public void setThirst(int thirst) {
        this.thirst = thirst;
    }

    public void checkAndGetAndSetLimbInjurty(int limb, String name) {
        int debuff = 0;
        switch (limb) {
            case 0:
                debuff = DataHolder.inst().getModifiers().getLeftarmInjuriesDebuff(name);
                break;
            case 1:
                debuff = DataHolder.inst().getModifiers().getRightarmInjuriesDebuff(name);
                break;
            case 2:
                debuff = DataHolder.inst().getModifiers().getLeftarmInjuriesDebuff(name) + DataHolder.inst().getModifiers().getRightarmInjuriesDebuff(name);
                break;
            case 3:
                debuff = DataHolder.inst().getModifiers().getLegsInjuriesDebuff(name);
                break;
        }
        try {
            if (DataHolder.inst().getPlayer(name).cantGetLimbInjury()) debuff = 0;
        } catch (Exception ignored) {

        }
        this.limbInjury = -debuff;
    }

    public void setProficiency(int proficiency) {
        this.proficiency = proficiency;
    }

    public int getProficiency() {
        return proficiency;
    }
}
