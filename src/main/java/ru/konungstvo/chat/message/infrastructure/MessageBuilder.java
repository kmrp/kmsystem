package ru.konungstvo.chat.message.infrastructure;

import java.util.HashMap;
import java.util.Map;
import java.util.MissingFormatArgumentException;

// replace {variables} with given inputs
// an example of template: "{playerName} (to GM): {content}"

@Deprecated // Replaced with MessageGenerator
public  class MessageBuilder {
    public static String build(String template, HashMap<String, String> inputs) {
        for (Map.Entry<String, String> input : inputs.entrySet()) {
            String variable = '{' + input.getKey() + '}';
            if (template.contains(variable)) {
                template = template.replace(variable, input.getValue());
            }
        }

        if (template.contains("{") || template.contains("}")) {
            //throw new MissingFormatArgumentException("A variable in template found that has no real value! " + template);
        }
        return template;
    }

}
