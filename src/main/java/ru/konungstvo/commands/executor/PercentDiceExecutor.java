package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class PercentDiceExecutor extends CommandBase {
    public static final String NAME = "percent";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        int percent;
        String maybePlayer;
        Player player = DataHolder.inst().getPlayer(sender.getName());

        percent = Integer.parseInt(args[0]);
        if (args.length > 1 && player.hasPermission(Permission.GM)) {
            maybePlayer = args[1];
            player = DataHolder.inst().getPlayer(maybePlayer);
        }


        //player.getPercentDice().setCustom(percent);
        player.getModifiersState().setModifier("percent", percent);
        sender.sendMessage(new TextComponentString("Percent dice for " + player.getName() + " set to " + percent));
        return;

    }
}
