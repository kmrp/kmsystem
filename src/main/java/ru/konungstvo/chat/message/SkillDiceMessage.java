package ru.konungstvo.chat.message;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;
import noppes.mpm.Server;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.combat.dice.SkillDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;
import toughasnails.api.TANCapabilities;
import toughasnails.thirst.ThirstHandler;

import java.io.Serializable;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.konungstvo.bridge.ServerProxy.getForgePlayer;

public class SkillDiceMessage
        extends FudgeDiceMessage
        implements Comparable, Serializable {

    private SkillDice skillDice;
    private String comment = "";
    private Logger logger = new Logger(this.getClass().getSimpleName());

    public SkillDiceMessage(String playerName, String content, SkillDice skillDice) {
        this(playerName, content, Range.NORMAL, skillDice);
    }

    public SkillDiceMessage(String playerName, String content) {
        this(playerName, content, Range.NORMAL_DIE);
    }

    public SkillDiceMessage(String playerName, String content, Range range) {
        this(playerName, content, range, null, 0);
    }

    public SkillDiceMessage(String playerName, String content, Range range, SkillDice skilldice) {
        this(playerName, content, range, skilldice, 0);
    }

    public SkillDiceMessage(String playerName, String message, Range range, int previousDefenses) {
        this(playerName, message, range, null, previousDefenses);
    }

    public SkillDiceMessage(String playerName, String content, Range range, SkillDice skillDice, int previousDefenses) {
        super(playerName, content, range);

//        logger.debug("Creating SDM: " + content);
        String comment = "";
        if (skillDice == null) {
            // parse skillName and skillLevel from content, using player's skillset
            Player player = DataHolder.inst().getPlayer(playerName);
            if (player == null) throw new DataException("1", "Игрок " + playerName + " не найден!");

            String skill = player.getSkillNameFromContext(content);

            if (skill == null) {
                throw new DataException("1", "Неверный ввод. Попробуйте: % название_навыка, либо % уровень_навыка.");
            }

            boolean ignorewounds = false;
            if (content.contains("IGNOREWOUNDS")) {
                ignorewounds = true;
                content = content.replaceFirst("IGNOREWOUNDS", "").trim();
            }
            // consider situation where we used alias for skill
            if (!content.substring(2).toLowerCase().startsWith(skill)) {
                content = content.replaceFirst(player.getAliasFor(skill), skill);
            }

            player.getPercentDice().setCustom(0);
            if (player.getModifiersState().getModifier("percent") != null) {
                player.getPercentDice().setCustom(player.getModifiersState().getModifier("percent"));
                System.out.println("Custom percent set to " + player.getPercentDice().getCustom());
            }

            String initial = player.getSkillLevel(skill);

            //parse message to extract needed values
            Pattern pattern = Pattern.compile(SkillDice.regex);

//            logger.debug("Trying to trim " + skill + " from " + content);
            Matcher matcher = pattern.matcher(content.replaceFirst("%", "").trim().substring(skill.length()));
            if (!matcher.find()) throw new IllegalStateException("Неверно введённый 4dF дайс!");

            if (skill.toLowerCase().equals("выносливость")) {
                player.resetPercentDice();
            }

            int mod = 0;
            if (matcher.group(1) != null && !matcher.group(1).equals("")) {
                //System.out.println("Matcher" + (matcher.group(1)));
                String modifier = matcher.group(1);
                //System.out.println("MOD" + modifier);
                if(modifier.contains("%")) {
                    System.out.println("TRUE");
                    int percentMod = Integer.parseInt(modifier.replace("%","").trim());
                    player.getPercentDice().setCustom(percentMod);
                } else {
                    mod = Integer.parseInt(modifier.substring(1));
                    if(mod < -9) mod = -9;
                    if(mod > 9) mod = 9;
                }
            }
            //System.out.println("MOD" + mod);
            if (matcher.group(2) != null && !matcher.group(2).equals("")) {
                comment = matcher.group(2).substring(1).replaceAll("§.", "").replaceAll("&([a-z0-9])", "§$1");
            }
            this.comment = comment;

            int armorMod = 0;
//            if (SkillsHelper.isArmorDependent(skill)) {
//                armorMod = player.getArmorMod();
//            }

            int shieldMod = 0;
//            if (SkillsHelper.isArmorDependent(skill)) {
//                shieldMod = player.getShieldMod();
//            }

            int woundsPercentMod = player.getWoundsMod();
            if (player.isHoldingOnWill()) woundsPercentMod -= 200;
            System.out.println("before ins" + woundsPercentMod);
            if (player.isInsensitive()) woundsPercentMod = Math.max(0, (woundsPercentMod - 50) / 2);
            System.out.println("after ins" + woundsPercentMod);

//            System.out.println("WoundsPercentMod is " + woundsPercentMod);
            int woundsMod = 0;
            int cutPart = woundsPercentMod % 100;
//            System.out.println("cutPart is " + -cutPart);
            player.getPercentDice().setWound(-cutPart);
            if (woundsPercentMod >= 100) {
                woundsMod = -(woundsPercentMod / 100);
            }

            if (!DataHolder.inst().isNpc(playerName)) {
                int foodLevel = player.getFoodLevel();
                if (foodLevel < 5) {
                    player.getPercentDice().setHunger(-(50 - foodLevel * 10));
                } else {
                    player.getPercentDice().setHunger(0);
                }
                int thirst = player.getThirstLevel();
                if (thirst < 5) {
                    player.getPercentDice().setThirst(-(50 - thirst * 10));
                } else {
                    player.getPercentDice().setThirst(0);
                }

            }

            int bloodloss = player.getBloodloss();
            player.getPercentDice().setBloodloss(-bloodloss);
            System.out.println("FudgeDiceMessage: " + "bloodloss=" + -bloodloss);

            int mentalDebuff = player.getMentalDebuff();
            player.getPercentDice().setMentalDebuff(-mentalDebuff);
            System.out.println("FudgeDiceMessage: " + "mentalDebuff=" + -mentalDebuff);

            player.getPercentDice().setFatigue(player.getFatigue());
            player.getPercentDice().setWeakened(player.getWeakened());
            if (player.getSkill(skill.toLowerCase()) != null) {
                String sk = skill.toLowerCase();
                if (sk.startsWith("владение") || sk.startsWith("метание")) sk = "владение";
                if (sk.startsWith("парирование")) sk = "парирование";
                player.getPercentDice().setBuff(player.getBuff(sk), player.getSkill(skill.toLowerCase()).getLevel());
            } else {
                player.getPercentDice().setBuff(0, 0);
            }

            if (skill.toLowerCase().equals("выносливость") || ignorewounds) {
                woundsMod = 0;
                player.getPercentDice().setWound(0);
                player.getPercentDice().setBloodloss(0);
                player.getPercentDice().setMentalDebuff(0);
                player.getPercentDice().setFatigue(0);
                player.getPercentDice().setWeakened(0);
                player.getPercentDice().setHunger(0);
                player.getPercentDice().setThirst(0);
            }

            //конечности
            if (skill.toLowerCase().equals("уклонение") || skill.toLowerCase().equals("передвижение")) {
                player.getPercentDice().checkAndGetAndSetLimbInjurty(3, playerName);
            }
            //

            //конечности
            if (skill.toLowerCase().equals("рукопашный бой")) {
                player.getPercentDice().checkAndGetAndSetLimbInjurty(2, playerName);
            }
            //

            //вес
            int weightMod = 0;
            int weightPerc = 0;
            int psyWeightMod = 0;
            int weight = player.countWeight(); //TODO Оптимизировать?
            int optimalWeight = player.getOptimalWeight();
            int maxWeight = player.getMaxWeight();
            System.out.println("FudgeDiceMessage: " + "weight=" + weight);
            System.out.println("FudgeDiceMessage: " + "optimalWeight=" + optimalWeight);
            System.out.println("FudgeDiceMessage: " + "maxWeight=" + maxWeight);

            if (weight > optimalWeight) {
                if ((skill.toLowerCase().matches("реакция|передвижение|ловкость|скрытность|уклонение"))) {
                    if (weight > optimalWeight * 2) {
                        weightPerc += optimalWeight * 5;
                    } else {
                        weightPerc += ((weight - optimalWeight) * 5);
                    }
                } else {
                    player.getPercentDice().setWeightDebuff(0);
                }
                if (weight > maxWeight && !skill.toLowerCase().equals("выносливость")) {
                    if (weight > maxWeight * 2) {
                        weightPerc += maxWeight * 5;
                    } else {
                        weightPerc += (weight - maxWeight) * 5;
                    }
                }
            }
            int weightCutPart = weightPerc % 100;
            player.getPercentDice().setWeightDebuff(-weightCutPart);
            if (weightPerc >= 100) {
                weightMod = -(weightPerc / 100);
            }



            if (player.getPsychweight() > player.getMaxPsychweight()) {
                if ((skill.toLowerCase().matches("психическая защита|опознание|пси-скрытность|реакция|внимательность") || true)) {
                    int w = player.getPsychweight();
                    while (w > player.getMaxPsychweight()) {
                        psyWeightMod -= 5;
                        w -= 1;
                    }

                }
            }

            if (skill.equalsIgnoreCase("пылеморфизм")) {
                weightMod = 0;
                weightPerc = 0;
                psyWeightMod = 0;
                player.getPercentDice().setWeightDebuff(0);
                player.getPercentDice().setMentalDebuff(0);
                player.getPercentDice().setFatigue(0);
                player.getPercentDice().setWeakened(0);
                player.getPercentDice().setHunger(0);
                player.getPercentDice().setThirst(0);

            }

            int psychCut = psyWeightMod % 100;
            player.getPercentDice().setPsychWeight(psychCut);
            int psyActualWeightMod = 0;
            if (psyWeightMod <= -100) {
                psyActualWeightMod = (psyWeightMod / 100);
            }


            int eqBuff = player.getBuffsFromEquipment(skill);
            int eqActualBuff = 0;
            int eqCut = eqBuff % 100;
            player.getPercentDice().setEqBuff(eqCut);
            if (eqBuff >= 100) {
                eqActualBuff = (eqBuff / 100);
            }
            if (eqBuff <= -100) {
                eqActualBuff = (eqBuff / 100);
            }


            int coverMod = 0;

            int concentratedMod = 0;
            if (player.hasConcentration(skill)) {
                System.out.println("!!!!!!!!!!!!found concentration on " + skill);
                concentratedMod = 1;
                player.removeConcentration(skill);
            }

            player.getPercentDice().setSkill(player.getSkill(skill).getPercent());
            skillDice = new SkillDice(initial, skill, mod, woundsMod, armorMod, coverMod, shieldMod, weightMod, psyActualWeightMod, eqActualBuff, concentratedMod, player.getPercentDice());
            if (player.getCustomAdvantage() != 0) {
                if (player.getCustomAdvantage() > 0) skillDice.addAdvantage("Кастом");
                else skillDice.addDisadvantage("Кастом");
            }

        } else {
            System.out.println("skilldice already set");
            skillDice.getPercentDice().setSkill(DataHolder.inst().getPlayer(playerName).getSkill(skillDice.getSkillName()).getPercent());
        }

        skillDice.considerDefenseMod(previousDefenses);


        this.skillDice = skillDice;
    }

    @Override
    public void build() {
        if (isBuilt()) return;

        skillDice.cast();

        if (comment!= null && !comment.equals("")) comment = " (" + comment.replaceAll("&([a-z0-9])" + "", "§$1") + "§e)";
        this.addInput("info", skillDice.getInfo());
        this.addInput("baseLevel", skillDice.getBaseAsString());
        this.addInput("skillInfo", skillDice.getSkillInfo());
        this.addInput("result", skillDice.getResultAsString());
        this.addInput("comment", comment);
        //if (getDice().wasChangedByPercentDice()) {
            int percentDice = getDice().getPercentDice().get();
            String percentDiceStr = String.valueOf(percentDice);
            percentDiceStr += "%";
            String dbuff = "Бафф: ";
            if (percentDice < 0 ) dbuff = "Дебафф: ";
            String debug = "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
                    " " + (Math.abs(percentDice) >= getDice().getPersentResult() && percentDice != 0 ? (percentDice < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) : "") + "Бросок: " + getDice().getPersentResult() + "/100" + TextFormatting.GRAY;
        debug += "\n" + getDice().getPercentDice().toString();
        this.addInput("debug", debug);

       // }
        super.build();
    }




    public void considerDefenseMod(int previousDefenses) {
        this.skillDice.considerDefenseMod(previousDefenses);
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        template.add(new MessageComponent("(( ", ChatColor.DICE));
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));
        if (range.toString().contains("GM")) {
            template.add(new MessageComponent(" (to GM)"));
            //template.add(new MessageComponent(" бросает 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" бросает 4dF ", ChatColor.DICE));
        } else if (range.toString().contains("CM")) {
            template.add(new MessageComponent(" (to CM)", ChatColor.CMCHAT));
            //template.add(new MessageComponent(" бросает 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" бросает 4dF ", ChatColor.DICE));
        } else {
            //template.add(new MessageComponent(" {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" {rangeDescription} 4dF ", ChatColor.DICE));
        }
        MessageComponent percentage = new MessageComponent("{info}", ChatColor.DICE);
        percentage.setHoverText((getPercentInfo() + "\n" + getDice().getAdvDisadvString()).trim(), TextFormatting.GOLD);
        template.add(percentage);
        //template.add(new MessageComponent(" от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))", ChatColor.DICE));

        template.add(new MessageComponent(" от {baseLevel}", ChatColor.DICE));
        MessageComponent modInfo = new MessageComponent(" [{skillInfo}]", ChatColor.DICE);
        String modString = skillDice.getModAsString();
        if (!modString.isEmpty()) modInfo.setHoverText(modString, TextFormatting.GRAY);
        template.add(modInfo);
//        template.add()"{modInfo}";
        template.add(new MessageComponent("{comment}", ChatColor.DICE));
        template.add(new MessageComponent(". Результат: {result} ))", ChatColor.DICE));
        return template;
    }

    /*
    @Override
    protected void generateTemplate() {
        String template;
        if (range.toString().contains("GM")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + " §f(to GM)" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))";
        } else if (range.toString().contains("BD")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + ChatColor.BDCHAT.get() + " (to BD)" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))";
        } else {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))";
        }
        //this.setTemplate(template);
    }
     */




    @Override
    public FudgeDice getDice() {
        return this.skillDice;
    }

    @Override
    public int compareTo(Object o) {
        SkillDiceMessage other = (SkillDiceMessage) o;
        int leftDie = this.getDice().getResult();
        int rightDie = other.getDice().getResult();
        if (leftDie == rightDie) {
            return 0;
        } else if (leftDie < rightDie) {
            return -1;
        }
        return 1;

    }

    public int compareToButBetter(Object o) {
        SkillDiceMessage other = (SkillDiceMessage) o;
        int leftDie = this.getDice().getFirstResult();
        int rightDie = other.getDice().getFirstResult();
        System.out.println(this.getPlayerName() + " first result " + leftDie + "|" + other.getPlayerName() + " first result " + rightDie);
        if (leftDie == rightDie) {
            System.out.println("EQUAL");
//            System.out.println(this.getPlayerName() + " is rerolled " + this.getDice().isRerolled() + "|" + other.getPlayerName() + " is rerolled " + other.getDice().isRerolled());
//            if (this.getDice().isRerolled() && other.getDice().isRerolled()) { //Probably should comment this part but who actually cares?
//                System.out.println(this.getPlayerName() + " last result " + this.getDice().getResult() + "|" + other.getPlayerName() + " last result " + other.getDice().getResult());
//                if (this.getDice().getResult() > other.getDice().getResult()) {
//                    System.out.println("1");
//                    return 1;
//                } else if (this.getDice().getResult() < other.getDice().getResult()) {
//                    System.out.println("-1");
//                    return -1;
//                }
//            }
            for (int i = 0; i < 15; i++) {
                System.out.println("RE-ROLL TIME " + i);
                this.reroll();
                other.reroll();
                System.out.println(this.getPlayerName() + " last result " + this.getDice().getResult() + "|" + other.getPlayerName() + " last result " + other.getDice().getResult());
                if (this.getDice().getResult() > other.getDice().getResult()) {
                    System.out.println("1");
                    return 1;
                } else if (this.getDice().getResult() < other.getDice().getResult()) {
                    System.out.println("-1");
                    return -1;
                }
            }
            System.out.println(this.getPlayerName() + " base " + this.getDice().getBase() + "|" + other.getPlayerName() + " base " + other.getDice().getBase());
            if (this.getDice().getBase() > other.getDice().getBase()) {
                return 1;
            } else if (this.getDice().getBase() < other.getDice().getBase()) {
                return -1;
            }
            Random rand = new Random();
            System.out.println("EPIC FAIL - FLIP A COIN");
            if (rand.nextInt(2) == 0) {
                System.out.println("1");
                return 1;
            } else {
                System.out.println("-1");
                return -1;
            }
            //return 0;
        } else if (leftDie < rightDie) {
            System.out.println("-1");
            return -1;
        }
        System.out.println("1");
        return 1;

    }

    public void reroll() {
        String snapshot = this.getResult();
        String prevResult = DataHolder.inst().getSkillTable().get(this.getDice().getResult());
        this.skillDice.reroll();
        this.addInput("result", skillDice.getResultAsString());
        this.build();
        String debug = "Rerolled \"" + snapshot + "\": " + prevResult + " --> " +
                DataHolder.inst().getSkillTable().get(this.getDice().getResult()
                );
        logger.info(debug);
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }
}
