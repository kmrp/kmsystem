package ru.konungstvo.control.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class ModifierMessage implements IMessage {
    public int modifier = 0;
    public boolean modifiedDamage = false;
    public boolean bodyPart = false;
    public boolean serial = false;
    public boolean percent = false;
    public boolean stationary = false;
    public boolean cover = false;
    public boolean optic = false;
    public boolean bipod = false;
    public boolean stunning = false;
    public boolean capture = false;
    public boolean noriposte = false;
    public boolean shieldwall = false;
    public boolean suppressing = false;

    public ModifierMessage() {
    }


    public ModifierMessage(boolean modifiedDamage, int modifier) {
        this(modifiedDamage, modifier, false, false, false, false, false, false, false, false, false, false, false, false);
    }

    //public ModifierMessage(boolean modifiedDamage, int modifier, boolean bodyPart) {
    //    this(modifiedDamage, modifier, bodyPart);
    //}
    public ModifierMessage(boolean modifiedDamage, int modifier, boolean bodyPart, boolean serial, boolean percent, boolean stationary,
                           boolean cover, boolean optic, boolean bipod, boolean stunning, boolean capture, boolean noriposte, boolean shieldwall, boolean suppressing) {
        this.modifier = modifier;
        this.modifiedDamage = modifiedDamage;
        this.bodyPart = bodyPart;
        this.serial = serial;
        this.percent = percent;
        this.stationary = stationary;
        this.cover = cover;
        this.optic = optic;
        this.bipod = bipod;
        this.stunning = stunning;
        this.capture = capture;
        this.noriposte = noriposte;
        this.shieldwall = shieldwall;
        this.suppressing = suppressing;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        modifier = buf.readInt();
        modifiedDamage = buf.readBoolean();
        bodyPart = buf.readBoolean();
        serial = buf.readBoolean();
        percent = buf.readBoolean();
        stationary = buf.readBoolean();
        cover = buf.readBoolean();
        optic = buf.readBoolean();
        bipod = buf.readBoolean();
        stunning = buf.readBoolean();
        capture = buf.readBoolean();
        noriposte = buf.readBoolean();
        shieldwall = buf.readBoolean();
        suppressing = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(modifier);
        buf.writeBoolean(modifiedDamage);
        buf.writeBoolean(bodyPart);
        buf.writeBoolean(serial);
        buf.writeBoolean(percent);
        buf.writeBoolean(stationary);
        buf.writeBoolean(cover);
        buf.writeBoolean(optic);
        buf.writeBoolean(bipod);
        buf.writeBoolean(stunning);
        buf.writeBoolean(capture);
        buf.writeBoolean(noriposte);
        buf.writeBoolean(shieldwall);
        buf.writeBoolean(suppressing);
    }
}
